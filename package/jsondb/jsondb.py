# import json
import re
import uuid
import json
import types
import urllib
import requests
from datetime import datetime

def connect(url):
    match = re.compile('^jsondb://([^:]*):([0-9]+)([^:]+):(.*)$').match(url)
    exec(urllib.request.urlopen(f'http://{match[1]}:{match[2]}{match[3]}/api.py').read().decode('utf-8')) in locals()
    return locals()["Database"](f'http://{match[1]}:{match[2]}{match[3]}', match[4])


