# Abstract

`jsondb` is a high-speed, in-memory object database component of [metacg](https://bitbucket.org/iapy/metacg)  that offers a `REST` API. It relies on [metacg](https://bitbucket.org/iapy/metacg)  and [weblib](https://bitbucket.org/iapy/weblib) for its `REST` API functionality.

- The models are `boost::hana` structures, meaning the model is precompiled and the schema is created at compile-time.
- All objects that are instantiated are stored in memory, saved to disk as `JSON` files upon *Create*, *Update*, and *Delete* (CUD) operations, and loaded when the application starts.
- `jsondb` handles memory management, which includes the allocation and deallocation of objects. Even though user code can access raw pointers, they are not intended to be removed using the `delete`operator.
- `jsondb` supports `nullable` relationships between objects, which are automatically set to `nullptr`when the related objects are deleted.

# Defining model

Let’s begin by creating a model to house our music library:

```c++
namespace music {

struct Country {
    BOOST_HANA_DEFINE_STRUCT(Country,
        (std::string, name)
    );
};

struct Artist
{
    BOOST_HANA_DEFINE_STRUCT(Artist,
        (std::string, name),
        (jsondb::Pointer<Country>, country)
    );
};

struct Album
{
    BOOST_HANA_DEFINE_STRUCT(Album,
        (int, year),
        (std::string, name),
        (jsondb::Pointer<Artist>, artist)
    );
};

} // namespace music
```

As stated, the models are simply `boost::hana` structures. The smart pointer `jsondb::pointer<>` is utilised to establish references between these structures.

## RESTful server

`jsondb` support `RESTful API` for accessing its data:

```c++
int main(int argc, char **argv)
{
    using namespace music;
    
    // Enumerate all persistent types
    using Database = jsondb::Server
    <    
        Album,
        Artist,
        Country
    >;

    using Graph = cg::Graph
    <
        // Eable multithreading, required
        cg::Connect<Database, msgbus::Msgbus>,
        // Enable RESTful API, optional
        cg::Connect<weblib::Server, Database, weblib::tag::Handler>
    >;

    return msgbus::Service<Graph>(
        cg::Args<weblib::Server>(2403), // port for RESTful API
        cg::Args<Database>(
            argv[1], // path to where objects are stored
            “/db"    // http://127.0.0.1:2403/db/
        )
    );
}
```

Compile and run.

```
$ ./demo ~/data
(2021-12-10 10:48:55) [INFO ] Crow/master server is running at http://0.0.0.0:2403 using 8 threads
(2021-12-10 10:48:55) [INFO ] Call `app.loglevel(crow::LogLevel::Warning)` to hide Info level logs. 
```

```
$ tree ~/data
/Users/mika/data
└── music 
    ├── Album 
    │   └── tmp 
    ├── Artist
    │   └── tmp 
    └── Country 
        └── tmp
7 directories, 0 files
```
Every persistent type is stored in a separate directory. The parent directory `music` corresponds to the `namespace music`.

The subsequent bash script is a basic example demonstrating how to interact with the database server:

```bash
# Save object
$ echo '{"name": "USA"}' | curl -X POST http://127.0.0.1:2403/db/music/Country --data-binary @- --silent | jq
{
  "_id": "04e891be-1108-4398-a264-dc355ae795b8",
  "name": "USA"
}

# List objects
$ curl http://127.0.0.1:2403/db/music/Country --silent  | jq
[
  {
    "_id": "04e891be-1108-4398-a264-dc355ae795b8",
    "name": "USA"
  }
]

# Bulk create (Note that the country is omitted for the second object and is null in the output)
$ echo '[{"name": "Dream Theater", "country": "04e891be-1108-4398-a264-dc355ae795b8"}, {"name": "Pain of Salvation"}]' | curl -X POST http://127.0.0.1:2403/db/music/Artist --data-binary @- --silent | jq
[
  {
    "_id": "2eb953b8-6676-415e-a4b7-c5893826a47a",
    "country": "04e891be-1108-4398-a264-dc355ae795b8",
    "name": "Dream Theater"
  },
  {
    "_id": "45fe49ad-4924-42d7-bd07-3e493ba5d5cc",
    "country": null,
    "name": "Pain of Salvation"
  }
]

# Removing object (will make all references null)
$ curl -X DELETE http://127.0.0.1:2403/db/music/Country/04e891be-1108-4398-a264-dc355ae795b8 --silent
$ curl http://127.0.0.1:2403/db/music/Artist
[
  {
    "_id": "45fe49ad-4924-42d7-bd07-3e493ba5d5cc",
    "country": null,
    "name": "Pain of Salvation"
  },
  {
    "_id": "2eb953b8-6676-415e-a4b7-c5893826a47a",
    "country": null,
    "name": "Dream Theater"
  }
]

$ echo '[{"name": "USA"},{"name": "Sweden"}]' | curl -X POST http://127.0.0.1:2403/db/music/Country --data-binary @- --silent | jq
[
  {
    "_id": "f9202da9-3559-4f93-aef1-e7b34924e72c",
    "name": "USA"
  },
  {
    "_id": "4ff0fe22-a37e-430c-935f-8a79c3f4831d",
    "name": "Sweden"
  }
]

# Bulk editing objects
$ echo '[{"_id": "2eb953b8-6676-415e-a4b7-c5893826a47a", "country": "f9202da9-3559-4f93-aef1-e7b34924e72c"},{"_id": "45fe49ad-4924-42d7-bd07-3e493ba5d5cc", "country": "4ff0fe22-a37e-430c-935f-8a79c3f4831d"}]' | curl -X PUT http://127.0.0.1:2403/db/music/Artist --data-binary @- --silent | jq
[
  {
    "_id": "2eb953b8-6676-415e-a4b7-c5893826a47a",
    "country": "f9202da9-3559-4f93-aef1-e7b34924e72c",
    "name": "Dream Theater"
  },
  {
    "_id": "45fe49ad-4924-42d7-bd07-3e493ba5d5cc",
    "country": "4ff0fe22-a37e-430c-935f-8a79c3f4831d",
    "name": “Pain Of Salvation"
  }
]

# Searching
# curl http://127.0.0.1:2403/db/music/Artist/country\?eq\=4ff0fe22-a37e-430c-935f-8a79c3f4831d --silent | jq
[
  {
    "_id": "2eb953b8-6676-415e-a4b7-c5893826a47a",
    "country": "f9202da9-3559-4f93-aef1-e7b34924e72c",
    "name": "Dream Theater"
  }
]
```

## Adding indexes

Let’s adjust our model to facilitate the search for `Artist`s.

```c++
namespace music {
// ...
struct Artist
{
    BOOST_HANA_DEFINE_STRUCT(Artist,
        (std::string, name),
        (jsondb::Pointer<Country>, country)
    );

    // Adding index for search
    using Index = jsondb::Index<&Artist::name>;
};
// ...
} // namespace music
```

Recompile and restart, then use the following `bash` script to test:

```
$ curl http://127.0.0.1:2403/db/music/Artist/name\?eq\=Dream%20Theater --silent | jq
[
  {
    "_id": "2eb953b8-6676-415e-a4b7-c5893826a47a",
    "country": "f9202da9-3559-4f93-aef1-e7b34924e72c",
    "name": "Dream Theater"
  }
]
```


## Creating simple c++ client

```c++
struct Client : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                auto db = this->remote(jsondb::tag::Client{});
                // here goes all the client code
                db.stop();
                return 0;
            }
        };
    };

    using Services = ct::tuple<Impl>;
};

// We will need to reconnect components
int main(int argc, char **argv)
{
    using namespace music;
    using Database = jsondb::Server<
        Album, Artist, Country
    >;

    using Graph = cg::Graph<
        // This is to enable multithreading, required
        cg::Connect<Database, msgbus::Msgbus>,
        // This is to connect client to database
        cg::Connect<Client, Database, jsondb::tag::Client>
    >;

    return msgbus::Service<Graph>(
        cg::Args<Database>(
            argv[1], // path to where objects are stored
            "/db"    // http:///127.0.0.1:2403/db/
        )
    );
}
```

## Query language

The subsequent code can be situated within the `int run()` method of the previously defined client.

Initially, we should initiate a transaction. Transactions can be readonly:

```c++
db.transaction([](jsondb::Lock<Artist const> lock){

});
```

or can mutate database:

```c++
db.transaction([](jsondb::Lock<Artist> lock){

});
```

Readonly transactions maintain readonly locks for objects listed in the `jsondb::Lock` arguments, as well as for all objects that these have references to. On the other hand, mutating transactions hold read-write locks for objects listed in the `jsondb::Lock` arguments, and readonly locks for all objects that these have references to.

In the above example, `jsondb::Lock<Artist const>` maintains readonly locks for both `Artist`and `Country`, while `jsondb::Lock<Artist>` holds a read-write lock for `Artist` and a readonly lock for `Country`.

If the transaction were to alter both countries and artists, a `jsondb::Lock<Artist, Country>` should be requested:

```c++
db.transaction([](jsondb::Lock<Artist, Country> lock){

});
```

### Querying data

```c++
db.transaction([](jsondb::Lock<Artist const> lock){
    lock.each([](Artist const *artist){
        std::cout << artist->name;
        if(artist->country)
        {
            std::cout << " is from " << artist->country->name;
        }
        std::cout << '\n';
    });

    lock.each([](Country const *country){
        std::cout << country->name << '\n';
    });
});
```

The code above bears some resemblance to the following `SQL` queries:

```sql
SELECT * FROM artists, countries WHERE artist.country = country.id;
SELECT * FROM countries;
```

Any of the subsequent actions would result in a compile-time error because `jsondb::Lock<Artist const>` is a read-only lock:

```c++
lock.each([](Artist *artist){});
lock.each([](Country *country){});
```

Any of the subsequent actions would result in a compile-time error because `jsondb::Lock<Artist const>` is not holding lock for `Album`s:

```c++
lock.each([](Album *album){});
lock.each([](Album const *album){});
```

### Selecting data by UUID

All objects in the database are identified by `UUID`. It is possible to find a unique object by its uuid:

```c++
boost::uuids::uuid a;
if(lock.get(a, [](Album const *album){
	// process found object here
}))
{
    std::cout << "Found\n";
}
else
{
    std::cout << "Not found\n";
}
```

### Selecting data with index

```c++
db.transaction([](jsondb::Lock<Artist const> lock){
    lock.each(
	    jsondb::Search<&Artist::name>{
	        .eq = "Dream Theater"
		},
	    [](auto const *artist){
	        std::cout << artist->name << '\n';
	    }
	);
});
```

In the lambda function provided as the second argument to `lock.each`, there’s no requirement to explicitly declare the types being iterated over. This can be inferred from the `jsondb::Search<>` parameters, hence `[](auto const *artist)`.

The aforementioned code is analogous to the following `SQL` query:

```SQL
SELECT * FROM artists WHERE artist.name = 'Dream Theater';
```

`jsondb::Search` supports `<`, `>`, `<=`, `>=`, `!=` operators in addition to `=`:

```c++
 // artist.name > 'C'
sondb::Search<&Artist::name>{ .gt = "C" }

 // artist.name < 'F'
sondb::Search<&Artist::name>{ .lt = "F" }

 // artist.name > 'C' and artist.name < 'F'
sondb::Search<&Artist::name>{ .gt = "C", .lt = "F" }

 // artist.name >= 'C' and artist.name < 'F'
sondb::Search<&Artist::name>{ .eq = "C", .gt = "C", .lt = "F" }

 // artist.name != 'C'
sondb::Search<&Artist::name>{ .ne = "C"}
```

Currently, conducting searches on multiple indexes is not supported. If necessary, this operation should be performed manually within the `each(Search<>, [auto *]{})` callback.

All the references are indexed implicitly, so it is always possible to query by referenced objects:

```c++
boost::uuids::uuid c;
db.transaction([&](jsondb::Lock<Artist const> lock){
    if(!lock.get(c, [](Country const *country){
	    lock.each(
		    jsondb::Search<&Artist::country>{
		        .eq = country
			},
		    [](auto const *artist){
		        std::cout << artist->name << ' '
		           << artist->country->name << '\n';
		    }
		);
    }))
    {
	    lock.each(
		    jsondb::Search<&Artist::country>{
		        .eq = nullptr
			},
		    [](auto const *artist){
		        std::cout << artist->name << ' nullptr\n';
		    }
		);
    }
});
```

## Creating objects

We need to employ a mutable lock in order to create new objects:

```c++
db.transaction([](jsondb::Lock<Artist, Country> lock){
    auto *country = lock.make(Country{
        .name = "USA"
    });
    lock.make(Artist{
        .name = "Dream Theater",
        .country = country
    });
});
```

The code above bears some resemblance to the following `SQL` queries:

```SQL
@country = GENERATE_UUID();
INSERT INTO countries (_id, name) 
       VALUES (@country, 'USA');

@artist = GENERATE_UUID();
INSERT INTO artists (_id, name, country) 
       VALUES (@artist, 'Dream Theater', @country);
```

It is always possible to obtain generated UUID of the created object:

```c++
// method 1
boost::uuids::uuid artist_id;
auto artist = lock.make(Artist{
	.name = "Dream Theater",
	.country = country
}, artist_id);

// method 2
std::optional<boost::uuids::uuid> artist_id_2 = lock.uuidof(lock(o1));
```

## Modifying objects

Likewise, we need to employ a mutable lock in order to modify objects:

```c++
db.transaction([](jsondb::Lock<Artist, Country> lock){
    auto *country = lock.make(Country{
        .name = "USA"
    });
    lock.each(
	    jsondb::Search<&Artist::name>{
	        .eq = "Dream Theater"
		},
	    [&country](auto const *artist){
	        artist->country = country
	        artist->name = "Keith Jarrett"
	    }
	);
});
```

The code above bears some resemblance to the following `SQL` queries:

```SQL
@country = GENERATE_UUID();
INSERT INTO countries (_id, name) 
       VALUES (@country, 'USA');

UPDATE artists
       SET country = @country
       SET name = 'Keith Jarrett'
       WHERE name = 'Dream Theater';
```

### Deleting objects

To delete objects, mutable lock again:

```c++
db.transaction([](jsondb::Lock<Artist> lock){
    lock.each(
	    jsondb::Search<&Artist::name>{
	        .eq = "Dream Theater"
		},
	    [&lock](auto const *artist){
	        lock.remove(artist);
	    }
	);
});
```

It is always possible to remove object by reference:

```c++
db.transaction([](jsondb::Lock<Artist> lock){
    lock.each(
	    jsondb::Search<&Artist::name>{
	        .eq = "Dream Theater"
		},
	    [&lock](auto const *artist){
	        if(artist->country)
		        lock.remove(artist->country);
	    }
	);
});
```

When an object is deleted, all references to that object automatically become `nullptr`:
- In the first scenario, all `Albums` by _Dream Theater_ would have their `Artist` reference set to `nullptr`
- In the second scenario, all `Artist`s from the same country as _Dream Theater_ would have their `Country`reference set to `nullptr`.

## Unique indexes

The `jsondb` has the capability to support unique indexes. This means that it can maintain a special kind of index, known as a unique index, which ensures that the index’s uniqueness constraint is enforced. In other words, no two separate records in the database will have the same value for the attributes that make up the unique index.

To create a unique index for `Country::name`, we need the following:

```c++
struct Country {
    BOOST_HANA_DEFINE_STRUCT(Country,
        (std::string, name),
        (std::uint64_t, population)
    );

    using Unique = jsondb::Index<&Country::name>;
};
```

Having unique index modifies the way `jsondb` operates on objects:

```c++
lock.make(Country{
	.name = "UK",
	.population = 60'000'000
});

lock.make(Country{
	.name = "UK",
	.population = 67'000'000
});
```

The `lock.unique` method, when used to query unique objects, will execute the lambda function provided as the second argument. This lambda function will be invoked with an existing object (if an object with a uniquely indexed field value is already present) or it will result in the creation of a new object.

```c++
lock.unique("UK", [&](Country *config) {

});
```

Querying unique objects with `lock.unique` method will invoke the lambda passed as second argument with either existing object (if object with uniquely indexed field value already exists) or create a new one.
## Field types

`jsondb` supports the following field types:

- All the primitive types (`int8_t`, `uint8_t`, ... `float`, `double`) which are indexable.
- `std::string` which is indexable.
- `nlohmann::json` which is indexable to store arbitrary `JSON` data
- `jsondb::Pointer<>` which is indexable and indexes for them are created automatically.
	- `jsondb::Search` on `jsondb::Pointer` has one field `.eq`
- `std::vector` which is indexable
	- `jsondb::Search` on `std::vector` has one field `.contains`
- `std::variant`
	- should contain either all of model types or all of non-model types
	- if contains all model types (i.e. `std::variant<Artist, Album>`) then it's indexable
- `std::map<any, any>` which is indexable
	- `jsondb::Search` on `std::map` has one field `.contains` (for the key)
- Any other user-defined `JSON`-serialisable type
