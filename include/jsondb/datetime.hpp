#pragma once
#include <chrono>
#include <jsonio/serializer.hpp>

namespace jsondb {
using DateTime = std::chrono::time_point<std::chrono::system_clock>;
} // namespace jsondb

namespace jsonio {
template<>
struct Serializer<jsondb::DateTime, void>
{
    static constexpr char Format[] = "%Y-%m-%dT%H:%M:%S";

    template<typename P>
    static void save(nlohmann::json &json, jsondb::DateTime const &value, P* = nullptr)
    {
        std::time_t now = std::chrono::system_clock::to_time_t(value);
        std::string repr(30, '\0');
        std::strftime(&repr[0], repr.size(), Format, std::gmtime(&now));
        json = repr.c_str();
    }

    template<typename P>
    static void load(nlohmann::json const &json, jsondb::DateTime &value, P* = nullptr)
    {
        std::tm tm = {};
        auto const &repr = json.get<std::string>();
        strptime(repr.c_str(), Format, &tm);
        value = std::chrono::system_clock::from_time_t(std::mktime(&tm));
    }

    static jsondb::DateTime parse(std::string_view const &value)
    {
        std::tm tm = {};
        strptime(value.data(), Format, &tm);
        return std::chrono::system_clock::from_time_t(std::mktime(&tm));
    }

    static std::string write(jsondb::DateTime const &value)
    {
        std::time_t now = std::chrono::system_clock::to_time_t(value);
        std::string repr(30, '\0');
        std::strftime(&repr[0], repr.size(), Format, std::gmtime(&now));
        return repr.c_str();
    }
};

} // namespace jsonio
