#pragma once

#include <jsondb/detail/meta.hpp>
#include <jsondb/detail/tables.hpp>
#include <jsondb/search.hpp>

#include <shared_mutex>
#include <mutex>
#include <bitset>

namespace jsondb {
namespace detail {

template<size_t S>
struct LockGuard
{
    std::function<void(std::bitset<S> const &)> destroy;
    std::bitset<S> dirty = 0;

    ~LockGuard()
    {
        if(dirty.any())
        {
            destroy(dirty);
        }
    }
};

template<typename T, template <typename> typename L>
class Lock
{
protected:
    ct::get_t<ct::index<0>, typename table_for<T>::type> &table;

private:
    L<std::shared_mutex> lock_;

    using Self  = Lock<T, L>;
    using Table = std::remove_cv_t<std::remove_reference_t<decltype(table)>>;

public:
    using Type  = std::remove_cv_t<T>;
    Lock(Lock &&) = default;

    template<typename ...Ls>
    Lock(std::tuple<Ls...> &locks)
    : Lock(std::move(std::get<Self>(locks))) {}

    template<typename ...Ts>
    Lock(Tables<Ts...> &tables)
    : table(std::get<Table>(tables.tables))
    , lock_(*table.mutex) {}
};

template<typename T>
struct lock_for
{
    using type = ct::tuple<Lock<T, std::unique_lock>>;
};

template<typename T>
struct lock_for<T const>
{
    using type = ct::tuple<Lock<T const, std::shared_lock>>;
};

template<typename T>
using lock_for_t = ct::get_t<ct::index<0>, typename lock_for<T>::type>;

template<typename ...Ts>
struct LockTraits
{
    using Decay = ct::transform_t<decay, ct::tuple<Ts...>>;
    using Nodes = typename bfs<Decay, ct::tuple<>>::type;

    using Types = ct::join_t
    <
        ct::tuple<Ts...>,
        ct::transform_t
        <
            add_const,
            ct::filter_t
            <
                ct::detail::not_in_tuple<Decay>::template filter,
                Nodes
            >
        >
    >;

    using Locks = ct::transform_t<lock_for, Types>;
};

template<typename T>
struct DirectLockTraits
{
    using Types = ct::join_t
    <
        ct::tuple<T>,
        ct::transform_t
        <
            add_const,
            ct::filter_t
            <
                ct::detail::not_in_tuple<ct::tuple<T>>::template filter,
                ct::transform_t<ct::detail::get_second, typename detail::make_edges<T>::type>
            >
        >
    >;

    using Locks = ct::transform_t<lock_for, Types>;
};

template<typename T, typename ...Ts>
struct InvertLockTraits
{
    using Types = ct::unique_t<ct::join_t<ct::tuple<T>, typename edges_to<T, Ts...>::type>>;
    using Locks = ct::transform_t<lock_for, Types>;
};

STORAGE_DEFINE_2ND_METAFUNCTION(make_locks, U, {
    using type = std::conditional_t<
        0 == ct::count_v<U, typename T::Types>,
        std::conditional_t<
            0 == ct::count_v<U const, typename T::Types>,
            ct::tuple<>,
            ct::tuple<detail::lock_for_t<U const>>
        >,
        ct::tuple<detail::lock_for_t<U>>
    >;
});

CT_TYPE(Lock_);

template<typename ...Ts>
class Lock_<ct::tuple<Ts...>> : protected Ts...
{
protected:
    template<typename ...Us>
    Lock_(std::tuple<Us...> &&locks) : Ts{locks}...  {}

    template<typename Functor>
    void for_each(Functor &&functor)
    {
        for_each_<Functor, Ts...>(std::forward<Functor>(functor));
    }

private:
    template<typename Functor, typename U, typename ...Us>
    void for_each_(Functor &&functor)
    {
        functor(U::table);
        if constexpr (sizeof ...(Us) > 0)
            for_each_<Functor, Us...>(std::forward<Functor>(functor));
    }
};

template<typename T, typename Traits>
class InternalLock : protected Lock_<typename Traits::Locks>
{
protected:
    using Base = Lock_<typename Traits::Locks>;

public:
    template<typename ...Us>
    InternalLock(detail::Tables<Us...> &tables) : Base{
        std::move(detail::instantiate<
            typename ct::transform_t<make_locks<Traits>::template f, ct::tuple<Us...>>::std_type
        >(tables))
    } {}
};

template<typename T>
using DirectLock = InternalLock<T, DirectLockTraits<T>>;

template<typename T, typename ...Ts>
class InvertLock : private InternalLock<T, InvertLockTraits<T, Ts...>>
{
public:
    template<typename Tables>
    InvertLock(Tables &tables) : InternalLock<T, InvertLockTraits<T, Ts...>>(tables) {}

    void collect_garbage(Table<T> &table, std::bitset<sizeof ...(Ts)> &collected)
    {
        for(auto it = table.garbage.begin(); it != table.garbage.end();)
        {
            InternalLock<T, InvertLockTraits<T, Ts...>>::Base::for_each([&it, &collected](auto &referencing){
                using F = typename table_of<std::decay_t<decltype(referencing)>>::type;
                if(referencing.clean(it->second.get()))
                {
                    collected[ct::index_of_v<F, ct::tuple<Ts...>>] = true;
                }
            });
            it = table.garbage.erase(it);
        }
    }
};

} // namespace detail

template<typename ...Ts>
class Lock : private detail::Lock_<typename detail::LockTraits<Ts...>::Locks>
{
private:
    using Traits = detail::LockTraits<Ts...>;
    using Base = detail::Lock_<typename Traits::Locks>;

    template<typename T>
    static constexpr bool Writable = (0 != ct::count_v<detail::lock_for_t<T>, typename Traits::Locks>);

public:
    template<typename ...Us>
    Lock(detail::Tables<Us...> &tables, detail::LockGuard<sizeof ...(Us)> &guard) : Base{
        std::move(detail::instantiate<
            typename ct::transform_t<detail::make_locks<Traits>::template f, ct::tuple<Us...>>::std_type
        >(tables))
    }
    {
        detail::template apply<0>((typename Traits::Locks*)nullptr, [&guard](auto *p){
            using T = typename std::remove_pointer_t<decltype(p)>::Type;
            if constexpr (Writable<T>)
            {
                guard.dirty[ct::index_of_v<T, ct::tuple<Us...>>] = true;
            }
        });
    }

    template<typename T>
    auto uuidof(T const *value) const
    {
        if constexpr (Writable<T>)
            return detail::lock_for_t<T>::table.uuidof(value);
        else
            return detail::lock_for_t<T const>::table.uuidof(value);
    }

    template<typename T>
    auto uuidof(Pointer<T> const &value) const
    {
        if constexpr (Writable<T>)
            return detail::lock_for_t<T>::table.uuidof(value);
        else
            return detail::lock_for_t<T const>::table.uuidof(value);
    }

    template<typename T>
    auto make(T &&value)
    {
        return detail::lock_for_t<T>::table.make(std::forward<T>(value));
    }

    template<typename V, typename Functor>
    std::enable_if_t<detail::TableIndexes<std::remove_cv_t<std::remove_pointer_t<detail::first_argument_t<Functor>>>>::HasUnique>
    unique(V &&value, Functor &&functor)
    {
        using T = std::remove_pointer_t<detail::first_argument_t<Functor>>;
        if constexpr (Writable<T>)
            detail::lock_for_t<T>::table.unique(std::forward<V>(value), std::move(functor));
        else
            detail::lock_for_t<T const>::table.unique(std::forward<V>(value), std::move(functor));
    }

    template<typename T, typename V>
    std::enable_if_t<detail::TableIndexes<T>::HasUnique, bool>
    contains(V &&value)
    {
        if constexpr (Writable<T>)
            return detail::lock_for_t<T>::table.contains(std::forward<V>(value));
        else
            return detail::lock_for_t<T const>::table.contains(std::forward<V>(value));
    }

    template<typename T>
    auto make(T &&value, boost::uuids::uuid &id)
    {
        return detail::lock_for_t<T>::table.make(std::forward<T>(value), id);
    }

    template<typename Functor>
    void each(Functor &&functor)
    {
        using T = std::remove_pointer_t<detail::first_argument_t<Functor>>;
        detail::lock_for_t<T>::table.each(std::move(functor));
    }

    template<typename T>
    void remove(Opaque<T> opaque)
    {
        detail::lock_for_t<T>::table.remove(opaque);
    }

    template<typename T>
    void remove(Pointer<T> &pointer)
    {
        detail::lock_for_t<T>::table.remove(pointer);
    }

    template<typename T>
    void remove(T* &pointer)
    {
        detail::lock_for_t<T>::table.remove(pointer);
    }

    template<typename T>
    auto operator () (Opaque<T> opaque)
    {
        if constexpr (Writable<T>)
            return detail::lock_for_t<T>::table.load(opaque);
        else
            return detail::lock_for_t<T const>::table.load(opaque);
    }

    template<typename T, typename Functor>
    bool operator () (Pointer<T> const &pointer, Functor &&functor)
    {
        return detail::lock_for_t<T>::table.load(pointer, functor);
    }

    template<auto U, typename Functor>
    auto each(Search<U> &&search, Functor &&functor)
    {
        using T = typename detail::MemberTraits<U>::Owner;
        if constexpr (Writable<T>)
            detail::lock_for_t<T>::table.each(std::forward<Search<U>>(search), std::forward<Functor>(functor));
        else
            detail::lock_for_t<T const>::table.each(std::forward<Search<U>>(search), std::forward<Functor>(functor));
    }

    template<auto U>
    auto count(Search<U> &&search)
    {
        using T = typename detail::MemberTraits<U>::Owner;
        if constexpr (Writable<T>)
            return detail::lock_for_t<T>::table.count(std::forward<Search<U>>(search));
        else
            return detail::lock_for_t<T const>::table.count(std::forward<Search<U>>(search));
    }

    template<auto U, typename Functor>
    auto count(Search<U> &&search, Functor &&functor)
    {
        using T = typename detail::MemberTraits<U>::Owner;
        if constexpr (Writable<T>)
            return detail::lock_for_t<T>::table.count(std::forward<Search<U>>(search), std::forward<Functor>(functor));
        else
            return detail::lock_for_t<T const>::table.count(std::forward<Search<U>>(search), std::forward<Functor>(functor));
    }

    template<typename Functor>
    bool get(boost::uuids::uuid const &uuid, Functor &&functor)
    {
        using T = std::remove_pointer_t<detail::first_argument_t<Functor>>;
        if constexpr (Writable<std::remove_cv_t<T>>)
            return detail::lock_for_t<std::remove_cv_t<T>>::table.get(uuid, std::forward<Functor>(functor));
        else
            return detail::lock_for_t<T>::table.get(uuid, std::forward<Functor>(functor));
    }

    template<typename T>
    void serialize(T const *p, nlohmann::json &json)
    {
        jsonio::to(json, *p, this);
    }

    template<typename T>
    void deserialize(T *p, nlohmann::json const &json)
    {
        jsonio::from(json, *p, this);
    }
};

} // namespace jsondb
