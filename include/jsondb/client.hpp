#pragma once
#include <jsondb/tags.hpp>
#include <jsondb/session.hpp>
#include <jsonio/struct.hpp>

#include <cg/component.hpp>
#include <cg/config.hpp>

#include <boost/core/demangle.hpp>
#include <boost/lexical_cast.hpp>

namespace jsondb {

struct Client : cg::Component
{
    template<typename>
    struct Types
    {
        JSON_STRUCT(Server,
        (
            (std::uint16_t, port),
            (std::string, prefix)
        ));
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            template<typename Functor>
            auto transaction(typename Base::Server const &server, Functor &&functor)
            {
                if constexpr (std::is_same_v<decltype(functor({this->state.client, server.prefix, server.port})), void>)
                    functor({this->state.client, server.prefix, server.port});
                else
                    return functor({this->state.client, server.prefix, server.port});
            }

            bool ping(typename Base::Server const &server)
            {
                try {
                    return 200 == this->state.client(weblib::method::get, weblib::http(server.port), server.prefix + "/api.js").result_int();
                } catch(...) {
                    return false;
                }
            }
        };
    };

    template<typename Types>
    struct State
    {
        weblib::Client client{"127.0.0.1"};
    };

    using Ports = ct::map<ct::pair<tag::Remote, Impl>>;
};

}
