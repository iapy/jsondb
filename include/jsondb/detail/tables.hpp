#pragma once

#include <jsonio/deserialize.hpp>
#include <jsondb/detail/meta.hpp>
#include <jsondb/detail/table.hpp>

#include <ct/tuple.hpp>
#include <ct/transform.hpp>

#include <boost/lexical_cast.hpp>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include <array>
#include <atomic>
#include <filesystem>

namespace jsondb {
namespace detail {

template<typename, template <typename> typename> class Lock;

template<typename>
class Loader;

template<>
class Loader<ct::tuple<>>
{
public:
    template<typename T> void load(T &) {}
    template<typename T> void ptrs(T &) {}
};

template<typename ...Ts>
class Loader<ct::tuple<Ts...>> : public std::unordered_map<boost::uuids::uuid, std::vector<Pointer<Ts>*>, boost::hash<boost::uuids::uuid>>...
{
public:
    template<typename Table>
    void load(Table &table)
    {
        using T = typename table_of<Table>::type;
        if constexpr (is_persistent_v<T>)
        {
            for(const auto p : std::filesystem::directory_iterator(table.storage))
            {
                if(p.is_directory()) continue;
                auto uuid = boost::lexical_cast<boost::uuids::uuid>(p.path().stem().string());

                nlohmann::json json;
                {
                    std::ifstream stream{p.path()};
                    stream >> json;
                }

                std::unique_ptr<T> uptr = std::make_unique<T>();
                jsonio::from(json, *uptr, this);

                T* obj = (table.objects[uuid] = std::move(uptr)).get();
                table.inverted[obj] = uuid;
            }
        }
    }

    template<typename T>
    void ptrs(T &tables)
    {
        pointers<T, Ts...>(tables);
        index<T, Ts...>(tables);
    }

private:
    template<typename T, typename U, typename ...Us>
    void pointers(T &tables)
    {
        using M = std::unordered_map<boost::uuids::uuid, std::vector<Pointer<U>*>, boost::hash<boost::uuids::uuid>>;
        for(auto it = this->M::begin(); it != this->M::end(); ++it)
        {
            for(std::size_t i = 0; i < it->second.size(); ++i)
            {
                (*it->second[i]) = std::get<ct::get_t<ct::index<0>, typename table_for<U>::type>>(tables).ptrof(it->first);
            }
        }

        if constexpr (0 != sizeof ...(Us))
        {
            pointers<T, Us...>(tables);
        }
    }

    template<typename T, typename U, typename ...Us>
    void index(T &tables)
    {
        auto &table = std::get<ct::get_t<ct::index<0>, typename table_for<U>::type>>(tables);
        for(auto &[_, object] : table.objects)
        {
            table.index.insert(object.get());
        }
        if constexpr (0 != sizeof ...(Us))
        {
            index<T, Us...>(tables);
        }
    }
};

template<typename ...Ts>
class Tables
{
    using Tuple = ct::tuple<Ts...>;
public:
    explicit Tables(std::filesystem::path const &root)
    : tables{instantiate<decltype(tables)>(root)}
    {
        Loader<ct::filter_t<is_persistent, Tuple>> loader;
        detail::template apply<0>(tables, [this, &loader](auto, auto &table) {
            loader.load(table);
        });
        loader.ptrs(tables);
    }

    Tables(Tables &&) = default;
    Tables(Tables const &) = delete;

    void sync(Batch &batch)
    {
        detail::template apply<0>(tables, [&, this](auto, auto &table) {
            table.sync(*this, batch);
        });
    }

    template<typename Functor>
    void each(Functor &&functor)
    {
        detail::template apply<0>(tables, std::move(functor));
    }

    template<typename T>
    void sync(Table<T> &table, Batch &batch)
    {
        table.sync(*this, batch);
    }

    template<typename T>
    void serialize(T const *p, nlohmann::json &json) const
    {
        jsonio::to(json, *p, &tables);
    }

private:
    typename ct::transform_t<detail::table_for, Tuple>::std_type tables;
    template<typename, template <typename> typename> friend class Lock;
};

} // namespace detail
} // namespace jsondb
