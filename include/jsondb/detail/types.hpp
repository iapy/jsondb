#pragma once

#include <boost/uuid/uuid.hpp>
#include <boost/core/demangle.hpp>
#include <boost/container_hash/hash.hpp>

#include <memory>
#include <unordered_map>

namespace jsondb::detail {

template<typename T>
using InternalStorage = std::unordered_map<
    boost::uuids::uuid, std::unique_ptr<T>, boost::hash<boost::uuids::uuid>>;

template<typename T>
using InvertedStorage = std::unordered_map<
    T const *, boost::uuids::uuid>;

} // namespace jsondb::detail
