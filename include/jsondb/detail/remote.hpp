#pragma once

#include <jsondb/detail/types.hpp>
#include <jsondb/detail/index.hpp>

#include <jsondb/datetime.hpp>
#include <jsondb/pointer.hpp>
#include <jsondb/search.hpp>

#include <weblib/client.hpp>

#include <jsonio/serialize.hpp>
#include <jsonio/deserialize.hpp>

#include <regex>

namespace jsondb {
namespace detail {

template<typename S>
class Cache
{
public:
    Cache(S *session) : session{session}
    {
        session->cache = true;
    }
    ~Cache()
    {
        session->cache = false;
    }
private:
    S *session;
};

STORAGE_DEFINE_2ND_METAFUNCTION(descriptor_equal, I, {
    static constexpr bool value = std::is_same_v<ct::get_t<ct::index<1>, I>, T>;
});

template<typename T, typename MemberPtr>
struct find_descriptor
{
    using type = ct::get_t<ct::index<0>, ct::filter_t<descriptor_equal<MemberPtr>::template f, typename make_indexes<T>::indexes>>;
};

template<typename T>
class Remote
{
protected:
    template<typename Session, typename Functor>
    bool each(Session *session, Functor &&functor)
    {
        Cache<Session> guard{session};
        if(!maybe_cache(session))
        {
            return false;
        }
        for(auto &[uuid, object] : this->objects)
        {
            if constexpr (detail::is_invocable_with_v<Functor, T const *>)
            {
                functor(object.get());
            }
            else if constexpr (detail::is_invocable_with_v<Functor, T const *, boost::uuids::uuid const &>)
            {
                functor(object.get(), uuid);
            }
            else
            {
                static_assert(detail::is_invocable_with_v<Functor, T *> || detail::is_invocable_with_v<Functor, T *, boost::uuids::uuid const &>);
                maybe_changed(session, uuid, object.get(), std::forward<Functor>(functor));
            }
        }
        return true;
    }

    template<typename Session, typename F, F T::*MemPtr, typename Functor>
    bool each(Session *session, Search<MemPtr> &&search, Functor &&functor)
    {
        using Indx = typename find_descriptor<T, boost::hana::struct_detail::member_ptr<decltype(MemPtr), MemPtr>>::type;
        using Name = ct::get_t<ct::index<0>, Indx>;

        std::ostringstream p;
        
        using namespace boost::hana;
        for_each(accessors<Search<MemPtr>>(), [&](auto key) {
            if constexpr (detail::is_vector<F>::value || detail::is_set<F>::value) {
                write_query(session, second(key)(search), (p << (p.tellp() ? '&' : '?') << first(key).c_str() << '='));
            } else {
                if(auto const &v = second(key)(search); v)
                {
                    write_query(session, *v, (p << (p.tellp() ? '&' : '?') << first(key).c_str() << '='));
                }
            }
        });

        if(auto response = +session->client(weblib::method::get, weblib::http(session->port), session->prefix + path + '/' + Name::c_str() + p.str()); 200 == response.code)
        {
            for(auto &j : response.template json<>())
            {
                auto uuid = boost::lexical_cast<boost::uuids::uuid>(j["_id"].template get<std::string>());
                if(auto it = this->objects.find(uuid); it != this->objects.end())
                {
                    jsonio::from(j, *(it->second.get()), session);
                    if constexpr (detail::has_additional_deserializer_v<T>)
                    {
                        (*(it->second.get()))(const_cast<nlohmann::json const &>(j));
                    }

                    if constexpr (detail::is_invocable_with_v<Functor, T const *>)
                    {
                        functor(it->second.get());
                    }
                    else if constexpr (detail::is_invocable_with_v<Functor, T const *, boost::uuids::uuid const &>)
                    {
                        functor(it->second.get(), uuid);
                    }
                    else
                    {
                        static_assert(detail::is_invocable_with_v<Functor, T *> || detail::is_invocable_with_v<Functor, T *, boost::uuids::uuid const &>);
                        maybe_changed(session, uuid, it->second.get(), functor);
                    }
                }
                else
                {
                    T *saved = from_json(session, uuid, j);
                    if constexpr (detail::is_invocable_with_v<Functor, T const *>)
                    {
                        functor(saved);
                    }
                    else if constexpr (detail::is_invocable_with_v<Functor, T const *, boost::uuids::uuid const &>)
                    {
                        functor(saved, uuid);
                    }
                    else
                    {
                        static_assert(detail::is_invocable_with_v<Functor, T *> || detail::is_invocable_with_v<Functor, T *, boost::uuids::uuid const &>);
                        maybe_changed(session, uuid, saved, functor);
                    }
                }
            }
            return true;
        }
        return false;
    }

    template<typename Session, typename Functor>
    bool get(Session *session, boost::uuids::uuid const &uuid, Functor &&functor, bool cache)
    {
        if(cache && session->cache && !maybe_cache(session))
        {
            return false;
        }

        if(auto it = this->objects.find(uuid); it != this->objects.end())
        {
            if constexpr (detail::is_invocable_with_v<Functor, T const *>)
            {
                functor(it->second.get());
            }
            else
            {
                static_assert(detail::is_invocable_with_v<Functor, T *>);
                maybe_changed(session, uuid, it->second.get(), std::forward<Functor>(functor));
            }
            return true;
        }
        else if(!cached)
        {
            if(auto response = +session->client(weblib::method::get, weblib::http(session->port), session->prefix + path + '/' + boost::uuids::to_string(uuid)); 200 == response.code)
            {
                auto json = response.template json<>();
                auto const uuid = boost::lexical_cast<boost::uuids::uuid>(json["_id"].template get<std::string>());

                T *saved = from_json(session, uuid, json);
                if constexpr (detail::is_invocable_with_v<Functor, T const *>)
                {
                    functor(saved);
                }
                else
                {
                    static_assert(detail::is_invocable_with_v<Functor, T *>);
                    maybe_changed(session, uuid, saved, std::forward<Functor>(functor), &json);
                }
                return true;
            }
        }
        return false;
    }

    template<typename Session>
    T const * make(Session *session, T &&source, boost::uuids::uuid *u = nullptr)
    {
        nlohmann::json json;
        jsonio::to(json, source, session);
        if constexpr (detail::has_additional_serializer_v<T>)
        {
            (const_cast<T const &>(source))(json);
        }
        if(auto response = +session->client(weblib::method::post, weblib::http(session->port), session->prefix + path, std::move(json)); 200 == response.code)
        {
            json = response.template json<>();
            auto const uuid = boost::lexical_cast<boost::uuids::uuid>(json["_id"].template get<std::string>());

            if(u)
            {
                *u = uuid;
            }

            return from_json(session, uuid, json);
        }
        return nullptr;
    }

    template<typename Session, typename U, typename Functor>
    bool unique(Session *session, U &&value, Functor &&functor)
    {
        using Indx = ct::get_t<ct::index<0>, typename make_indexes<T>::uniques>;
        using Name = ct::get_t<ct::index<0>, Indx>;
        using Func = ct::get_t<ct::index<1>, Indx>;

        std::ostringstream p;
        write_query(session, value, (p << "?eq="));

        if(auto response = +session->client(weblib::method::get, weblib::http(session->port), session->prefix + path + '/' + Name::c_str() + p.str()); 200 == response.code)
        {
            if(auto j = response.template json<>(); j.size() == 0)
            {
                T object;
                Func{}(object) = value;

                boost::uuids::uuid uuid;
                T *saved = const_cast<T *>(make(session, std::move(object), &uuid));

                if constexpr (detail::is_invocable_with_v<Functor, T const *>)
                {
                    functor(saved);
                }
                else if constexpr (detail::is_invocable_with_v<Functor, T const *, boost::uuids::uuid const &>)
                {
                    functor(saved, uuid);
                }
                else
                {
                    static_assert(detail::is_invocable_with_v<Functor, T *> || detail::is_invocable_with_v<Functor, T *, boost::uuids::uuid const &>);
                    maybe_changed(session, uuid, saved, functor);
                }
                return true;
            }
            else if(j.size() == 1)
            {
                auto uuid = boost::lexical_cast<boost::uuids::uuid>(j[0]["_id"].template get<std::string>());
                if(auto it = this->objects.find(uuid); it != this->objects.end())
                {
                    jsonio::from(j[0], *(it->second.get()), session);
                    if constexpr (detail::has_additional_deserializer_v<T>)
                    {
                        (*(it->second.get()))(const_cast<nlohmann::json const &>(j));
                    }

                    if constexpr (detail::is_invocable_with_v<Functor, T const *>)
                    {
                        functor(it->second.get());
                    }
                    else if constexpr (detail::is_invocable_with_v<Functor, T const *, boost::uuids::uuid const &>)
                    {
                        functor(it->second.get(), uuid);
                    }
                    else
                    {
                        static_assert(detail::is_invocable_with_v<Functor, T *> || detail::is_invocable_with_v<Functor, T *, boost::uuids::uuid const &>);
                        maybe_changed(session, uuid, it->second.get(), functor);
                    }
                }
                else
                {
                    T *saved = from_json(session, uuid, j[0]);
                    if constexpr (detail::is_invocable_with_v<Functor, T const *>)
                    {
                        functor(saved);
                    }
                    else if constexpr (detail::is_invocable_with_v<Functor, T const *, boost::uuids::uuid const &>)
                    {
                        functor(saved, uuid);
                    }
                    else
                    {
                        static_assert(detail::is_invocable_with_v<Functor, T *> || detail::is_invocable_with_v<Functor, T *, boost::uuids::uuid const &>);
                        maybe_changed(session, uuid, saved, functor);
                    }
                }
                return true;
            }
        }
        return false;
    }

    template<typename Session>
    bool remove(Session *session, T const *object)
    {
        if(auto it = inverted.find(object); it != inverted.end())
        {
            return 200 == session->client(weblib::method::del, weblib::http(session->port), session->prefix + path, std::move(boost::uuids::to_string(it->second))).result_int();
        }
        return false;
    }

    std::optional<boost::uuids::uuid> uuidof(T const *object) const
    {
        if(auto it = inverted.find(object); it != inverted.end())
        {
            return it->second;
        }
        return std::nullopt;
    }

    std::optional<boost::uuids::uuid> uuidof(Pointer<T> const &object) const
    {
        if(auto it = inverted.find(object.pointer); it != inverted.end())
        {
            return it->second;
        }
        return std::nullopt;
    }

private:
    template<typename Session, typename F>
    void write_query(Session *session, F const &v, std::ostream &p)
    {
        if constexpr (std::is_pointer_v<F>) {
            if(auto u = session->uuidof(v); u)
            {
                p << boost::uuids::to_string(*u);
            }
            else
            {
                p << "null";
            }
        }
        else if constexpr (std::is_fundamental_v<F>) {
            p << v;
        }
        else if constexpr (std::is_same_v<F, bool>) {
            p << static_cast<int>(v);
        }
        else if constexpr (std::is_enum_v<F>) {
            p << static_cast<std::underlying_type_t<F>>(v);
        }
        else if constexpr (std::is_same_v<F, DateTime>) {
            p << jsonio::Serializer<DateTime>::write(v);
        } 
        else {
            p << weblib::escape(v.begin(), v.end());
        }
    }

    template<typename Session>
    T* from_json(Session *session, boost::uuids::uuid const &uuid, nlohmann::json const &j)
    {
        std::unique_ptr<T> object{new T};
        jsonio::from(j, *object, session);
        if constexpr (detail::has_additional_deserializer_v<T>)
        {
            (*object)(const_cast<nlohmann::json const &>(j));
        }

        T *saved = (objects[uuid] = std::move(object)).get();
        inverted[saved] = uuid;
        return saved;
    }

    template<typename Session>
    bool maybe_cache(Session *session)
    {
        if(!cached)
        {
            if(auto response = +session->client(weblib::method::get, weblib::http(session->port), session->prefix + path); cached = (response.code == 200))
            {
                for(auto &j : response.template json<>())
                {
                    auto uuid = boost::lexical_cast<boost::uuids::uuid>(j["_id"].template get<std::string>());
                    if(auto it = this->objects.find(uuid); it != this->objects.end())
                    {
                        jsonio::from(j, *(it->second.get()), session);
                        if constexpr (detail::has_additional_deserializer_v<T>)
                        {
                            (*(it->second.get()))(const_cast<nlohmann::json const &>(j));
                        }
                    }
                    else
                    {
                        (void)from_json(session, uuid, j);
                    }
                }
            }
        }
        return cached;
    }

    template<typename Session, typename Functor>
    void maybe_changed(Session *session, boost::uuids::uuid const &uuid, T *object, Functor &&functor, nlohmann::json *json = nullptr)
    {
        T const copy = *object;

        if constexpr (detail::is_invocable_with_v<Functor, T *, boost::uuids::uuid const &>)
        {
            functor(object, uuid);
        }
        else
        {
            functor(object);
        }

        bool changed = detail::has_additional_deserializer_v<T>;
        if constexpr (!detail::has_additional_deserializer_v<T>)
        {
            boost::hana::for_each(boost::hana::accessors<T>(), [&](auto key) {
                if(boost::hana::second(key)(copy) != boost::hana::second(key)(*object))
                {
                    changed = true;
                }
            });
        }
        if(changed)
        {
            auto commit = [&, this]() {
                jsonio::to(*json, *object, session);
                if constexpr (detail::has_additional_serializer_v<T>)
                {
                    (const_cast<T const &>(*object))(*json);
                }
                session->client(weblib::method::put, weblib::http(session->port), session->prefix + path, std::move(*json));
            };

            if(json)
            {
                commit();
            }
            else
            {
                nlohmann::json j = {
                    {"_id", boost::uuids::to_string(uuid)} 
                };
                json = &j;
                commit();
            }
        }
    }

private:
    bool cached{false};
    std::string const path{"/" + std::regex_replace(boost::core::demangle(typeid(T).name()), std::regex{"::"}, ":")};

    InternalStorage<T> objects;
    InvertedStorage<T> inverted;
};

} // namespace detail
} // namespace jsondb
