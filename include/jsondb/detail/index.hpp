#pragma once
#include <jsondb/index.hpp>
#include <jsondb/search.hpp>
#include <jsondb/detail/meta.hpp>
#include <jsonio/deserialize.hpp>
#include <jsonio/struct.hpp>

#include <map>
#include <unordered_map>
#include <unordered_set>
#include <iostream>

namespace jsondb {
template<typename> class Pointer;
template<typename> class Indexed;

namespace detail {

template<typename T>
auto has_index(T*) -> decltype(
    static_cast<typename T::Index *>(nullptr), std::true_type{}
);

template<typename T>
std::false_type has_index(...);

template<typename T>
constexpr bool has_index_v = decltype(has_index<T>(0))::value;

template<typename T>
auto has_unique(T*) -> decltype(
    static_cast<typename T::Unique *>(nullptr), std::true_type{}
);

template<typename T>
std::false_type has_unique(...);

template<typename T>
constexpr bool has_unique_v = decltype(has_unique<T>(0))::value;

template<typename T, typename, bool = has_index_v<T>>
struct is_indexed;

template<typename T, typename MemPtr, MemPtr ptr>
struct is_indexed<T, boost::hana::struct_detail::member_ptr<MemPtr, ptr>, false>
{
    static constexpr bool value = false;
};

template<typename T, typename MemPtr, MemPtr ptr>
struct is_indexed<T, boost::hana::struct_detail::member_ptr<MemPtr, ptr>, true>
{
    static constexpr bool value = (0 < ct::count_v<
        MemberTraits<ptr>, typename T::Index::type
    >);
};

template<typename T, typename, bool = has_unique_v<T>>
struct is_unique;

template<typename T, typename MemPtr, MemPtr ptr>
struct is_unique<T, boost::hana::struct_detail::member_ptr<MemPtr, ptr>, false>
{
    static constexpr bool value = false;
};

template<typename T, typename MemPtr, MemPtr ptr>
struct is_unique<T, boost::hana::struct_detail::member_ptr<MemPtr, ptr>, true>
{
    static constexpr bool value = (0 < ct::count_v<MemberTraits<ptr>, typename T::Unique::type>);
};

STORAGE_DEFINE_2ND_METAFUNCTION(indexed_fields, Field, {
    using HanaMemberPtr = ct::get_t<ct::index<1>, Field>;
    static constexpr bool value = 
        is_pointer<std::decay_t<decltype(std::declval<HanaMemberPtr>()(std::declval<T>()))>>::value || is_indexed<T, HanaMemberPtr>::value;
});

STORAGE_DEFINE_2ND_METAFUNCTION(unique_fields, Field, {
    using HanaMemberPtr = ct::get_t<ct::index<1>, Field>;
    static constexpr bool value = is_unique<T, HanaMemberPtr>::value;
});

template<typename T>
struct make_indexes
{
    using indexes = ct::filter_t<indexed_fields<T>::template f, ct::transform_t<
        tuple_from, typename tuple_from<decltype(boost::hana::accessors<T>())>::t
    >>;

    using uniques = ct::filter_t<unique_fields<T>::template f, ct::transform_t<
        tuple_from, typename tuple_from<decltype(boost::hana::accessors<T>())>::t
    >>;
};

template<typename T, typename Underlying>
struct index_accepts : std::is_same<T, Underlying> {};

template<typename T, typename ...Ts>
struct index_accepts<T, ct::tuple<Ts...>> : std::disjunction<index_accepts<T, Ts>...> {};

template<typename T, typename Underlying>
static constexpr bool index_accepts_v = index_accepts<T, Underlying>::value;

template<typename T, typename F, typename MemberFunction>
struct VariantMemberFunction : private MemberFunction
{
    F& operator () (T& value)
    {
        return std::get<F>(MemberFunction::operator ()(value));
    }

    F const & operator () (T const &value)
    {
        return std::get<F>(MemberFunction::operator ()(value));
    }
};

template<typename T, typename F, typename MemberFunction>
class TableIndex : private MemberFunction
{
public:
    using UnderlyingType = F;

protected:
    void insert(T const *value)
    {
        index[MemberFunction::operator ()(*value)].insert(value);
    }

    void remove(T const *value)
    {
        if(auto it = index.find(MemberFunction::operator ()(*value)); it != index.end())
        {
            it->second.erase(value);
            if(it->second.empty())
            {
                index.erase(it);
            }
        }
    }

    template<typename Search, typename Functor>
    void search(Search &&search, Functor &&functor) const
    {
        if(search.eq)
        {
            if constexpr(!std::is_enum_v<UnderlyingType>)
            {
                if((search.gt && *search.eq <= *search.gt) || (search.lt && *search.lt <= *search.eq)) return;
            }
            if(auto it = index.find(*search.eq); it != index.end())
            {
                for(T const *p : it->second) functor(p);
            }
        }
        else
        {
            auto b = index.begin();
            auto e = index.end();

            if constexpr(!std::is_enum_v<UnderlyingType>)
            {
                if(search.gt)
                {
                    if(search.lt)
                    {
                        if(*search.lt <= *search.gt) return;
                        b = index.upper_bound(*search.gt);
                        e = index.lower_bound(*search.lt);
                    }
                    else
                    {
                        b = index.upper_bound(*search.gt);
                    }
                }
                else if(search.lt)
                {
                    e = index.lower_bound(*search.lt);
                }
            }

            for(auto it = b; it != e; ++it)
            {
                for(T const *p : it->second) functor(p);
            }
        }
    }

    template<typename Search>
    std::size_t count(Search &&search) const
    {
        if(search.eq)
        {
            if constexpr(!std::is_enum_v<UnderlyingType>)
            {
                if((search.gt && *search.eq <= *search.gt) || (search.lt && *search.lt <= *search.eq))
                {
                    return 0;
                }
            }
            if(auto it = index.find(*search.eq); it != index.end())
            {
                return it->second.size();
            }
            else
            {
                return 0;
            }
        }

        auto b = index.begin();
        auto e = index.end();

        if constexpr(!std::is_enum_v<UnderlyingType>)
        {
            if(search.gt)
            {
                if(search.lt)
                {
                    if(*search.lt <= *search.gt)
                    {
                        return 0;
                    }
                    b = index.upper_bound(*search.gt);
                    e = index.lower_bound(*search.lt);
                }
                else
                {
                    b = index.upper_bound(*search.gt);
                }
            }
            else if(search.lt)
            {
                e = index.lower_bound(*search.lt);
            }
        }

        std::size_t result{0};
        for(auto it = b; it != e; ++it)
        {
            result += it->second.size();
        }
        return result;
    }

    template<typename Search, typename Functor>
    std::size_t count(Search &&search, Functor &&functor) const
    {
        if(search.eq)
        {
            if constexpr(!std::is_enum_v<UnderlyingType>)
            {
                if((search.gt && *search.eq <= *search.gt) || (search.lt && *search.lt <= *search.eq))
                {
                    return 0;
                }
            }
            if(auto it = index.find(*search.eq); it != index.end())
            {
                std::size_t result = 0;
                for(T const *p : it->second) result += functor(p);
                return result;
            }
            else
            {
                return 0;
            }
        }

        auto b = index.begin();
        auto e = index.end();

        if constexpr(!std::is_enum_v<UnderlyingType>)
        {
            if(search.gt)
            {
                if(search.lt)
                {
                    if(*search.lt <= *search.gt)
                    {
                        return 0;
                    }
                    b = index.upper_bound(*search.gt);
                    e = index.lower_bound(*search.lt);
                }
                else
                {
                    b = index.upper_bound(*search.gt);
                }
            }
            else if(search.lt)
            {
                e = index.lower_bound(*search.lt);
            }
        }

        std::size_t result{0};
        for(auto it = b; it != e; ++it)
        {
            for(T const *p : it->second) result += functor(p);
        }
        return result;
    }

    void modify(T const &oldValue, T const *value)
    {
        if(auto it = index.find(MemberFunction::operator ()(oldValue)); it != index.end())
        {
            it->second.erase(value);
            if(it->second.empty())
            {
                index.erase(it);
            }
        }
        insert(value);
    }

private:
    std::map<UnderlyingType, std::unordered_set<T const *>> index;
};

template<typename T, typename MemberFunction>
class TableIndex<T, bool, MemberFunction> : private MemberFunction
{
public:
    using UnderlyingType = bool;

protected:
    void insert(T const *value)
    {
        index[MemberFunction::operator ()(*value)].insert(value);
    }

    void remove(T const *value)
    {
        index[MemberFunction::operator ()(*value)].erase(value);
    }

    template<typename Search, typename Functor>
    void search(Search &&search, Functor &&functor) const
    {
        if(search.eq)
        {
            for(T const *p : index[*search.eq]) functor(p);
        }
        else
        {
            for(T const *p : index[0]) functor(p);
            for(T const *p : index[1]) functor(p);
        }
    }

    template<typename Search>
    std::size_t count(Search &&search) const
    {
        if(search.eq)
        {
            return index[*search.eq].size();
        }
        else
        {
            return index[0].size() + index[1].size();
        }
    }

    template<typename Search, typename Functor>
    std::size_t count(Search &&search, Functor &&functor) const
    {
        if(search.eq)
        {
            std::size_t result{0};
            for(T const *p : index[*search.eq]) result += functor(p);
            return result;
        }
        else
        {
            std::size_t result{0};
            for(T const *p : index[0]) result += functor(p);
            for(T const *p : index[1]) result += functor(p);
            return result;
        }
    }

    void modify(T const &oldValue, T const *value)
    {
        index[MemberFunction::operator ()(oldValue)].erase(value);
        insert(value);
    }

private:
    std::array<std::unordered_set<T const *>, 2> index;
};

template<typename T, typename F, typename MemberFunction>
class TableIndex<T, Pointer<F>, MemberFunction> : private MemberFunction
{
public:
    using UnderlyingType = F const *;

    void set(T const *object, std::nullptr_t)
    {
        MemberFunction::operator ()(const_cast<T&>(*object)) = nullptr;
        index[nullptr].insert(object);
    }

protected:
    void insert(T const *value)
    {
        index[static_cast<UnderlyingType>(MemberFunction::operator ()(*value).pointer)].insert(value);
    }

    void remove(T const *value)
    {
        if(auto it = index.find(MemberFunction::operator ()(*value).pointer); it != index.end())
        {
            it->second.erase(value);
            if(it->second.empty())
            {
                index.erase(it);
            }
        }
    }

    void modify(T const &oldValue, T const *value)
    {
        remove(oldValue, value);
        insert(value);
    }

    template<typename Functor>
    void for_each(UnderlyingType value, Functor &&functor)
    {
        if(auto it = index.find(value); it != index.end())
        {
            functor(this, &it->second);
            if(it->second.empty())
            {
                index.erase(it);
            }
        }
    }

    template<typename Search, typename Functor>
    void search(Search &&search, Functor &&functor) const
    {
        if(search.eq)
        {
            if(auto it = index.find(*search.eq); it != index.end())
            {
                for(T const *p : it->second) functor(p);
            }
        }
        else
        {
            for(auto it = index.begin(); it != index.end(); ++it)
            {
                for(T const *p : it->second) functor(p);
            }
        }
    }

    template<typename Search>
    std::size_t count(Search &&search) const
    {
        if(search.eq)
        {
            if(auto it = index.find(*search.eq); it != index.end())
            {
                return it->second.size();
            }
            return 0;
        }

        std::size_t result{0};
        for(auto it = index.begin(); it != index.end(); ++it)
        {
            result += it->second.size();
        }
        return result;
    }

    template<typename Search, typename Functor>
    std::size_t count(Search &&search, Functor &&functor) const
    {
        if(search.eq)
        {
            if(auto it = index.find(*search.eq); it != index.end())
            {
                std::size_t result{0};
                for(T const *p : it->second) result += functor(p);
                return result;
            }
            return 0;
        }

        std::size_t result{0};
        for(auto it = index.begin(); it != index.end(); ++it)
        {
            for(T const *p : it->second) result += functor(p);
        }
        return result;
    }

protected:
    void remove(T const &oldValue, T const *value)
    {
        if(auto it = index.find(MemberFunction::operator ()(oldValue).pointer); it != index.end())
        {
            it->second.erase(value);
            if(it->second.empty())
            {
                index.erase(it);
            }
        }
    }
private:
    std::unordered_map<UnderlyingType, std::unordered_set<T const *>> index;
};

template<typename T, typename Container, typename MemberFunction>
class ContainerIndex : private MemberFunction
{
public:
    using UnderlyingType = typename Container::value_type;
    static constexpr bool Hashable = detail::is_std_hashable_v<UnderlyingType>;

protected:
    void insert(T const *value)
    {
        for(auto const &v : MemberFunction::operator ()(*value))
        {
            index[v].insert(value);
        }
    }

    void remove(T const *value)
    {
        for(auto const &v : MemberFunction::operator ()(*value))
        {
            if(auto it = index.find(v); it != index.end())
            {
                it->second.erase(value);
                if(it->second.empty())
                {
                    index.erase(it);
                }
            }
        }
    }

    template<typename Search, typename Functor>
    void search(Search &&search, Functor &&functor) const
    {
        if(auto it = index.find(search.contains); it != index.end())
        {
            for(T const *p : it->second) functor(p);
        }
    }

    template<typename Search>
    std::size_t count(Search &&search) const
    {
        std::size_t result{0};
        if(auto it = index.find(search.contains); it != index.end())
        {
            result += it->second.size();
        }
        return result;
    }

    template<typename Search, typename Functor>
    std::size_t count(Search &&search, Functor &&functor) const
    {
        std::size_t result{0};
        if(auto it = index.find(search.contains); it != index.end())
        {
            for(T const *p : it->second) result += functor(p);
        }
        return result;
    }

    void modify(T const &oldValue, T const *value)
    {
        std::conditional_t<
            !Hashable,
            std::set<UnderlyingType>,
            std::unordered_set<UnderlyingType>
        > s{
            MemberFunction::operator ()(*value).begin(),
            MemberFunction::operator ()(*value).end()
        };
        for(auto v : MemberFunction::operator ()(oldValue))
        {
            if(s.find(v) != s.end())
            {
                continue;
            }
            if(auto it = index.find(v); it != index.end())
            {
                it->second.erase(value);
                if(it->second.empty())
                {
                    index.erase(it);
                }
            }
        }
        for(auto const &v : s)
        {
            index[v].insert(value);
        }
    }

private:
    std::conditional_t<
        !Hashable,
        std::map<UnderlyingType, std::unordered_set<T const *>>,
        std::unordered_map<UnderlyingType, std::unordered_set<T const *>>
    > index;
};

template<typename T, typename F, typename A, typename MemberFunction>
class TableIndex<T, std::vector<F, A>, MemberFunction> : public ContainerIndex<T, std::vector<F, A>, MemberFunction> {};

template<typename T, typename F, typename C, typename A, typename MemberFunction>
class TableIndex<T, std::set<F, C, A>, MemberFunction> : public ContainerIndex<T, std::set<F, C, A>, MemberFunction> {};

template<typename T, typename F, typename H, typename E, typename A, typename MemberFunction>
class TableIndex<T, std::unordered_set<F, H, E, A>, MemberFunction> : public ContainerIndex<T, std::unordered_set<F, H, E, A>, MemberFunction> {};

template<typename T, typename Container, typename MemberFunction>
class MapIndex : private MemberFunction
{
public:
    using UnderlyingType = std::remove_cv_t<typename Container::value_type::first_type>;
    static constexpr bool Hashable = detail::is_std_hashable_v<UnderlyingType>;

protected:
    void insert(T const *value)
    {
        for(auto const &v : MemberFunction::operator ()(*value))
        {
            index[v.first].insert(value);
        }
    }

    void remove(T const *value)
    {
        for(auto const &v : MemberFunction::operator ()(*value))
        {
            if(auto it = index.find(v.first); it != index.end())
            {
                it->second.erase(value);
                if(it->second.empty())
                {
                    index.erase(it);
                }
            }
        }
    }

    template<typename Search, typename Functor>
    void search(Search &&search, Functor &&functor) const
    {
        if(auto it = index.find(search.contains); it != index.end())
        {
            for(T const *p : it->second) functor(p);
        }
    }

    template<typename Search>
    std::size_t count(Search &&search) const
    {
        std::size_t result{0};
        if(auto it = index.find(search.contains); it != index.end())
        {
            result += it->second.size();
        }
        return result;
    }

    template<typename Search, typename Functor>
    std::size_t count(Search &&search, Functor &&functor) const
    {
        std::size_t result{0};
        if(auto it = index.find(search.contains); it != index.end())
        {
            for(T const *p : it->second) result += functor(p);
        }
        return result;
    }

    void modify(T const &oldValue, T const *value)
    {
        std::conditional_t<
            !Hashable,
            std::set<UnderlyingType>,
            std::unordered_set<UnderlyingType>
        > s;

        for(auto const &v : MemberFunction::operator ()(*value))
        {
            s.insert(v.first);
        }

        for(auto const &v : MemberFunction::operator ()(oldValue))
        {
            if(s.find(v.first) != s.end())
            {
                continue;
            }
            if(auto it = index.find(v.first); it != index.end())
            {
                it->second.erase(value);
                if(it->second.empty())
                {
                    index.erase(it);
                }
            }
        }
        for(auto const &v : s)
        {
            index[v].insert(value);
        }
    }

private:
    std::conditional_t<
        !Hashable,
        std::map<UnderlyingType, std::unordered_set<T const *>>,
        std::unordered_map<UnderlyingType, std::unordered_set<T const *>>
    > index;
};

template<typename T, typename K, typename V, typename C, typename A, typename MemberFunction>
class TableIndex<T, std::map<K, V, C, A>, MemberFunction> : public MapIndex<T, std::map<K, V, C, A>, MemberFunction> {};

template<typename T, typename K, typename V, typename H, typename E, typename A, typename MemberFunction>
class TableIndex<T, std::unordered_map<K, V, H, E, A>, MemberFunction> : public MapIndex<T, std::unordered_map<K, V, H, E, A>, MemberFunction> {};

template<typename T, typename MemberFunction, typename ...Ts>
class TableIndex<T, std::variant<Ts...>, MemberFunction> : private TableIndex<T, Ts, VariantMemberFunction<T, Ts, MemberFunction>>...
{
public:
    using UnderlyingType = ct::tuple<typename TableIndex<T, Ts, VariantMemberFunction<T, Ts, MemberFunction>>::UnderlyingType...>;

public:
    void insert(T const *value)
    {
        std::visit([&, this](auto const &v){
            using V = std::decay_t<decltype(v)>;
            using I = TableIndex<T, V, VariantMemberFunction<T, V, MemberFunction>>;
            this->I::insert(value);
        }, memfun(*value));
    }

    void remove(T const *value)
    {
        std::visit([&, this](auto const &v){
            using V = std::decay_t<decltype(v)>;
            using I = TableIndex<T, V, VariantMemberFunction<T, V, MemberFunction>>;
            this->I::remove(value);
        }, memfun(*value));
    }

    void modify(T const &oldValue, T const *value)
    {
        std::visit([&, this](auto const &v) {
            using V = std::decay_t<decltype(v)>;
            using I = TableIndex<T, V, VariantMemberFunction<T, V, MemberFunction>>;
            this->I::remove(oldValue, value);
        }, memfun(oldValue));
        insert(value);
    }

    template<typename F, typename Functor>
    void for_each(F const value, Functor &&functor)
    {
        using V = Pointer<std::remove_cv_t<std::remove_pointer_t<std::remove_cv_t<decltype(value)>>>>;
        using I = TableIndex<T, V, VariantMemberFunction<T, V, MemberFunction>>;
        I::for_each(value, std::move(functor));
    }

    template<typename Search, typename Functor>
    void search(Search &&search, Functor &&functor) const
    {
        std::visit([functor=std::move(functor), this](auto const &v){
            using V = std::decay_t<decltype(v)>;
            if constexpr (std::is_same_v<std::monostate, V>)
            {
                this->template search_<Search, Functor, Ts...>(functor, std::nullopt);
            }
            else 
            {
                if(v == nullptr)
                {
                    this->template search_<Search, Functor, Ts...>(functor, nullptr);
                }
                else
                {
                    using P = Pointer<std::remove_cv_t<std::remove_pointer_t<V>>>;
                    this->TableIndex<T, P, VariantMemberFunction<T, P, MemberFunction>>::search(typename Search::template Rebind<V>{v}, functor);
                }
            }
        }, search.eq.value);
    }

    template<typename Search>
    std::size_t count(Search &&search) const
    {
        return std::visit([this](auto const &v){
            using V = std::decay_t<decltype(v)>;
            if constexpr (std::is_same_v<std::monostate, V>)
            {
                return this->template count_<Search, Ts...>(std::nullopt);
            }
            else
            {
                if(v == nullptr)
                {
                    return this->template count_<Search, Ts...>(nullptr);
                }
                else
                {
                    using P = Pointer<std::remove_cv_t<std::remove_pointer_t<V>>>;
                    return this->TableIndex<T, P, VariantMemberFunction<T, P, MemberFunction>>::count(typename Search::template Rebind<V>{v});
                }
            }
        }, search.eq.value);
    }

    template<typename Search, typename Functor>
    std::size_t count(Search &&search, Functor &&functor) const
    {
        return std::visit([functor=std::forward<Functor>(functor), this](auto const &v){
            using V = std::decay_t<decltype(v)>;
            if constexpr (std::is_same_v<std::monostate, V>)
            {
                return this->template count_<Search, Ts...>(std::nullopt, functor);
            }
            else
            {
                if(v == nullptr)
                {
                    return this->template count_<Search, Ts...>(nullptr, functor);
                }
                else
                {
                    using P = Pointer<std::remove_cv_t<std::remove_pointer_t<V>>>;
                    return this->TableIndex<T, P, VariantMemberFunction<T, P, MemberFunction>>::count(typename Search::template Rebind<V>{v}, functor);
                }
            }
        }, search.eq.value);
    }

private:
    template<typename Search, typename Functor, typename U, typename ...Us, typename ...Args>
    void search_(Functor const &functor, Args&&... args) const
    {
        this->TableIndex<T, U, VariantMemberFunction<T, U, MemberFunction>>::search(
            typename Search::template Rebind<typename U::UnderlyingType>{args...}, functor
        );
        if constexpr (sizeof ...(Us) != 0)
        {
            search_<Search, Functor, Us...>(functor, std::forward<Args>(args)...);
        }
    }

    template<typename Search, typename U, typename ...Us, typename Arg, typename ...Args>
    std::size_t count_(Arg &&arg, Args&&... args) const
    {
        std::size_t result = this->TableIndex<T, U, VariantMemberFunction<T, U, MemberFunction>>::count(
            typename Search::template Rebind<typename U::UnderlyingType>{arg}, args...
        );
        if constexpr (sizeof ...(Us) != 0)
        {
            return result + count_<Search, Us...>(std::forward<Arg>(arg), std::forward<Args>(args)...);
        }
        else
        {
            return result;
        }
    }

private:
    MemberFunction memfun;
    static_assert(std::conjunction_v<is_pointer<Ts>...>);
};


struct JsonFilter
{
    BOOST_HANA_DEFINE_STRUCT(JsonFilter,
        (std::optional<nlohmann::json>, le),
        (std::optional<nlohmann::json>, lt),
        (std::optional<nlohmann::json>, gt),
        (std::optional<nlohmann::json>, ge)
    );
    JSON_RENAME_FIELDS((le, .le),(lt, .lt),(gt, .gt),(ge, .ge));

    operator bool () const
    {
        return !!le || !!lt || !!gt || !!ge;
    }

    std::optional<nlohmann::json::value_t> type() const
    {
        using namespace boost::hana;
        std::set<nlohmann::json::value_t> types;
        for_each(accessors<JsonFilter>(), [this, &types](auto &&accessor){
            if(second(accessor)(*this)) types.insert(second(accessor)(*this)->type());
        });
        if(1 == types.size()) return *(types.begin());
        return std::nullopt;
    }
};

template<typename O>
class JsonIndex
{
private:
    struct Inserter
    {
        std::set<O const *> * const s;
        void operator () (O const *value)
        {
            s->insert(value);
        }
    };

public:
    bool empty() const
    {
        return all.empty();
    }

    void insert(nlohmann::json const &value, O const *o)
    {
        all.insert(o);
        switch(value.type())
        {
        case nlohmann::json::value_t::null:
            nulls.insert(o);
            break;
        case nlohmann::json::value_t::boolean:
            booleans[value.template get<bool>()].insert(o);
            break;
        case nlohmann::json::value_t::string:
            strings[value.template get<std::string>()].insert(o);
            break;
        case nlohmann::json::value_t::number_integer:
        case nlohmann::json::value_t::number_unsigned:
            integers[value.template get<std::int64_t>()].insert(o);
            break;
        case nlohmann::json::value_t::object:
            for(auto const &[k, v] : value.items())
            {
                if(k.empty() || k[0] == '_') continue;
                if(!keys.count(k)) keys.insert(std::make_pair(k, std::make_unique<JsonIndex<O>>()));
                keys.find(k)->second->insert(v, o);
            }
            break;
        }
    }

    void remove(nlohmann::json const &value, O const *o)
    {
        all.erase(o);
        switch(value.type())
        {
        case nlohmann::json::value_t::null:
            nulls.erase(o);
            break;
        case nlohmann::json::value_t::boolean:
            booleans[value.template get<bool>()].erase(o);
            break;
        case nlohmann::json::value_t::string:
            {
                auto it = strings.find(value.template get<std::string>());
                if(it->second.erase(o), it->second.empty()) strings.erase(it);
            }
            break;
        case nlohmann::json::value_t::number_integer:
        case nlohmann::json::value_t::number_unsigned:
            {
                auto it = integers.find(value.template get<std::int64_t>());
                if(it->second.erase(o), it->second.empty()) integers.erase(it);
            }
            break;
        case nlohmann::json::value_t::object:
            for(auto const &[k, v] : value.items())
            {
                if(k.empty() || k[0] == '_') continue;
                auto it = keys.find(k);
                if(it->second->remove(v, o), it->second->empty()) keys.erase(it);
            }
            break;
        }
    }

    template<typename Functor>
    void search(std::optional<nlohmann::json> const &query, Functor &&functor) const
    {
        if(!query)
        {
            for(O const *ptr : all) functor(ptr);
        }
        else switch(query->type())
        {
        case nlohmann::json::value_t::null:
            for(O const *ptr : nulls) functor(ptr);
            break;
        case nlohmann::json::value_t::boolean:
            for(O const *ptr : booleans[query->template get<bool>()]) functor(ptr);
            break;
        case nlohmann::json::value_t::string:
            if(auto it = strings.find(query->template get<std::string>()); it != strings.end())
                for(O const *ptr : it->second) functor(ptr);
            break;
        case nlohmann::json::value_t::number_integer:
        case nlohmann::json::value_t::number_unsigned:
            if(auto it = integers.find(query->template get<std::int64_t>()); it != integers.end())
                for(O const *ptr : it->second) functor(ptr);
            break;
        case nlohmann::json::value_t::object:
            if(JsonFilter filter = jsonio::from<JsonFilter>(*query); filter)
            {
                if(auto t = filter.type(); t)
                {
                    switch (*t)
                    {
                        case nlohmann::json::value_t::boolean:
                            search(filter, booleans, std::forward<Functor>(functor));
                            break;
                        case nlohmann::json::value_t::string:
                            search(filter, strings, std::forward<Functor>(functor));
                            break;
                        case nlohmann::json::value_t::number_integer:
                        case nlohmann::json::value_t::number_unsigned:
                            search(filter, integers, std::forward<Functor>(functor));
                            break;
                    }
                }
            }
            else
            {
                std::set<O const *> objects{all};
                for(auto const &[k, v] : query->items())
                {
                    if(auto it = keys.find(k); it != keys.end())
                    {
                        std::set<O const *> local;
                        it->second->search(v, Inserter{&local});
                        for(auto jt = objects.begin(); jt != objects.end();)
                        {
                            if(local.find(*jt) == local.end()) jt = objects.erase(jt);
                            else ++jt;
                        }
                    }
                    else
                    {
                        objects.clear();
                        break;
                    }
                }
                for(O const *ptr : objects) functor(ptr);
            }
            break;
        }
    }

    std::size_t count(std::optional<nlohmann::json> const &query) const
    {
        if(!query) return all.size();
        switch(query->type())
        {
        case nlohmann::json::value_t::null:
            return nulls.size();
        case nlohmann::json::value_t::boolean:
            return booleans[query->template get<bool>()].size();
        case nlohmann::json::value_t::string:
            if(auto it = strings.find(query->template get<std::string>()); it != strings.end())
                return it->second.size();
            return 0;
        case nlohmann::json::value_t::number_integer:
        case nlohmann::json::value_t::number_unsigned:
            if(auto it = integers.find(query->template get<std::int64_t>()); it != integers.end())
                return it->second.size();
            return 0;
        case nlohmann::json::value_t::object:
            if(JsonFilter filter = jsonio::from<JsonFilter>(*query); filter)
            {
                if(auto t = filter.type(); t)
                {
                    switch (*t)
                    {
                        case nlohmann::json::value_t::boolean:
                            return count(filter, booleans);
                            break;
                        case nlohmann::json::value_t::string:
                            return count(filter, strings);
                            break;
                        case nlohmann::json::value_t::number_integer:
                        case nlohmann::json::value_t::number_unsigned:
                            return count(filter, integers);
                            break;
                    }
                }
                return 0;
            }
            else
            {
                std::set<O const *> objects{all};
                for(auto const &[k, v] : query->items())
                {
                    if(auto it = keys.find(k); it != keys.end())
                    {
                        std::set<O const *> local;
                        it->second->search(v, Inserter{&local});
                        for(auto jt = objects.begin(); jt != objects.end();)
                        {
                            if(local.find(*jt) == local.end()) jt = objects.erase(jt);
                            else ++jt;
                        }
                    }
                    else
                    {
                        objects.clear();
                        break;
                    }
                }
                return objects.size();
            }
        }
        return 0;
    }
private:
    template<typename Index, typename Functor>
    void search(JsonFilter const &f, Index const &index, Functor &&functor) const
    {
        if constexpr (std::is_same_v<Index, std::array<std::unordered_set<O const *>, 2>>)
        {
            std::size_t b = 0;
            std::size_t e = 2;
            if(f.ge) b = static_cast<std::size_t>(f.ge->template get<bool>());
            if(f.gt) b = 1 + static_cast<std::size_t>(f.gt->template get<bool>());
            if(f.le) e = 2 - static_cast<std::size_t>(!f.le->template get<bool>());
            if(f.lt) e = 1 - static_cast<std::size_t>(!f.lt->template get<bool>());
            for(; b < e; ++b)
            {
                for(O const *ptr : index[b]) functor(ptr);
            }
        }
        else
        {
            auto [b, e] = range(f, index);
            if(e == std::end(index) || b->first < e->first)
            {
                for(; b != e; ++b)
                {
                    for(O const *ptr : b->second) functor(ptr);
                }
            }
        } 
    }

    template<typename Index>
    std::size_t count(JsonFilter const &f, Index const &index) const
    {
        std::size_t result{0};
        if constexpr (std::is_same_v<Index, std::array<std::unordered_set<O const *>, 2>>)
        {
            std::size_t b = 0;
            std::size_t e = 2;
            if(f.ge) b = static_cast<std::size_t>(f.ge->template get<bool>());
            if(f.gt) b = 1 + static_cast<std::size_t>(f.gt->template get<bool>());
            if(f.le) e = 2 - static_cast<std::size_t>(!f.le->template get<bool>());
            if(f.lt) e = 1 - static_cast<std::size_t>(!f.lt->template get<bool>());
            for(; b < e; ++b)
            {
                result += index[b].size();
            }
        }
        else
        {
            auto [b, e] = range(f, index);
            if(e == std::end(index) || b->first < e->first)
            {
                for(; b != e; ++b)
                {
                    result += b->second.size();
                }
            }
        } 
        return result;
    }

    template<typename Index>
    std::pair<typename Index::const_iterator, typename Index::const_iterator> range(JsonFilter const &f, Index const &index) const
    {
        auto b = std::begin(index);
        auto e = std::end(index);
        if(f.gt && f.ge)
        {
            if(f.gt->template get<typename Index::key_type>() > f.ge->template get<typename Index::key_type>())
            {
                b = index.upper_bound(f.gt->template get<typename Index::key_type>());
                if(b == index.end()) return std::make_pair(e, e);
            }
            else
            {
                b = index.lower_bound(f.ge->template get<typename Index::key_type>());
                if(b == index.end()) return std::make_pair(e, e);
            }
        }
        else if(f.gt)
        {
            b = index.upper_bound(f.gt->template get<typename Index::key_type>());
            if(b == index.end()) return std::make_pair(e, e);
        }
        else if(f.ge)
        {
            b = index.lower_bound(f.ge->template get<typename Index::key_type>());
            if(b == index.end()) return std::make_pair(e, e);
        }

        if(f.lt && f.le)
        {
            if(f.lt->template get<typename Index::key_type>() < f.le->template get<typename Index::key_type>())
            {
                e = index.lower_bound(f.lt->template get<typename Index::key_type>());
            }
            else
            {
                e = index.upper_bound(f.le->template get<typename Index::key_type>());
            }
        }
        else if(f.lt)
        {
            e = index.lower_bound(f.lt->template get<typename Index::key_type>());
        }
        else if(f.le)
        {
            e = index.upper_bound(f.le->template get<typename Index::key_type>());
        }
        return std::make_pair(b, e);
    }

private:
    std::set<O const *> all;
    std::set<O const *> nulls;
    std::array<std::unordered_set<O const *>, 2> booleans;
    std::map<std::string, std::unordered_set<O const *>> strings;
    std::map<std::int64_t, std::unordered_set<O const *>> integers;
    std::unordered_map<std::string, std::unique_ptr<JsonIndex<O>>> keys;
};

template<typename T, typename MemberFunction>
class TableIndex<T, nlohmann::json, MemberFunction> : private MemberFunction
{
public:
    using UnderlyingType = nlohmann::json;

public:
    void insert(T const *value)
    {
        index.insert(MemberFunction::operator ()(*value), value);
    }

    void remove(T const *value)
    {
        index.remove(MemberFunction::operator ()(*value), value);
    }

    void modify(T const &oldValue, T const *value)
    {
        remove(&oldValue);
        insert(value);
    }

    template<typename Search, typename Functor>
    void search(Search &&search, Functor &&functor) const
    {
        index.search(search.query, std::forward<Functor>(functor));
    }

    template<typename Search>
    std::size_t count(Search &&search) const
    {
        return index.count(search.query);
    }

private:
    JsonIndex<T> index;
};

template<typename T, typename = typename make_indexes<T>::indexes, typename = typename make_indexes<T>::uniques>
class TableIndexes;

template<typename T, typename ...Ts>
class TableIndexes<T, ct::tuple<Ts...>, ct::tuple<>> : TableIndex<T, std::decay_t<decltype(std::declval<ct::get_t<ct::index<1>, Ts>>()(std::declval<T>()))>, ct::get_t<ct::index<1>, Ts>>...
{
    template<typename Accessor>
    using Index = TableIndex<T, std::decay_t<decltype(std::declval<Accessor>()(std::declval<T>()))>, Accessor>;

public:
    void insert(T const *value)
    {
        if constexpr (sizeof ...(Ts) > 0)
        {
            insert_<Ts...>(value);
        }
    }

    void remove(T const *value)
    {
        if constexpr (sizeof ...(Ts) > 0)
        {
            remove_<Ts...>(value);
        }
    }

    template<typename Accessor>
    void modify(T const &oldValue, T const *value)
    {
        if constexpr (0 < ct::count_v<Accessor, ct::transform_t<ct::detail::get_second, ct::tuple<Ts...>>>)
        {
            Index<Accessor>::modify(oldValue, value);
        }
    }

    template<typename F, typename Functor>
    void for_each(F const &value, Functor &&functor)
    {
        if constexpr (sizeof ...(Ts) > 0)
        {
            for_each_<F, Functor, Ts...>(value, std::forward<Functor>(functor));
        }
    }

    template<auto MemPtr, typename Container>
    void search(Search<MemPtr> &&search, Container &container)
    {
        TableIndex<T, typename MemberTraits<MemPtr>::Type, boost::hana::struct_detail::member_ptr<
            decltype(MemPtr), MemPtr
        >>::search(std::forward<Search<MemPtr>>(search), [&container](T const *ptr){ container.insert(ptr); });
    }

    template<auto MemPtr, typename Functor>
    void search(Search<MemPtr> &&search, Functor &&functor) const
    {
        TableIndex<T, typename MemberTraits<MemPtr>::Type, boost::hana::struct_detail::member_ptr<
            decltype(MemPtr), MemPtr
        >>::search(std::forward<Search<MemPtr>>(search), std::forward<Functor>(functor));
    }

    template<auto MemPtr>
    std::size_t count(Search<MemPtr> &&search) const
    {
        return TableIndex<T, typename MemberTraits<MemPtr>::Type, boost::hana::struct_detail::member_ptr<
            decltype(MemPtr), MemPtr
        >>::count(std::forward<Search<MemPtr>>(search));
    }

    template<auto MemPtr, typename Functor>
    std::size_t count(Search<MemPtr> &&search, Functor &&functor) const
    {
        return TableIndex<T, typename MemberTraits<MemPtr>::Type, boost::hana::struct_detail::member_ptr<
            decltype(MemPtr), MemPtr
        >>::count(std::forward<Search<MemPtr>>(search), std::forward<Functor>(functor));
    }

    constexpr static bool HasUnique = false;

private:
    template<typename U, typename ...Us>
    void insert_(T const *value)
    {
        Index<ct::get_t<ct::index<1>, U>>::insert(value);
        if constexpr (sizeof ...(Us) > 0)
            insert_<Us...>(value);
    }

    template<typename U, typename ...Us>
    void remove_(T const *value)
    {
        Index<ct::get_t<ct::index<1>, U>>::remove(value);
        if constexpr (sizeof ...(Us) > 0)
            remove_<Us...>(value);
    }

    template<typename F, typename Functor, typename U, typename ...Us>
    void for_each_(F const value, Functor &&functor)
    {
        if constexpr (index_accepts_v<F, typename Index<ct::get_t<ct::index<1>, U>>::UnderlyingType>)
            Index<ct::get_t<ct::index<1>, U>>::for_each(value, functor);
        if constexpr (sizeof ...(Us) > 0)
            for_each_<F, Functor, Us...>(value, std::move(functor));
    }
};

template<typename T, typename ...Ts, typename Unique>
class TableIndexes<T, ct::tuple<Ts...>, ct::tuple<Unique>> : private ct::get_t<ct::index<1>, Unique>, private TableIndexes<T, ct::tuple<Ts...>, ct::tuple<>>
{
    static_assert(0 == ct::count_v<Unique, ct::tuple<Ts...>>);

    using Base = TableIndexes<T, ct::tuple<Ts...>, ct::tuple<>>;
    using MemberFunction = ct::get_t<ct::index<1>, Unique>;
public:
    using UnderlyingType = std::decay_t<decltype(std::declval<ct::get_t<ct::index<1>, Unique>>()(std::declval<T>()))>;

    T const * find(UnderlyingType const &value) const
    {
        if(auto it = index.find(value); it != index.end())
        {
            return it->second;
        }
        return nullptr;
    }

    T const * find(T const &value) const
    {
        if(auto it = index.find(MemberFunction::operator () (value)); it != index.end())
        {
            return it->second;
        }
        return nullptr;
    }

    void insert(T const *value)
    {
        index[MemberFunction::operator () (*value)] = value;
        Base::insert(value);
    }

    void remove(T const *value)
    {
        index.erase(MemberFunction::operator () (*value));
        Base::remove(value);
    }

    using Base::count;
    using Base::for_each;
    using Base::modify;
    using Base::search;
    using MemberFunction::operator ();

    constexpr static bool HasUnique = true;

private:
    std::unordered_map<UnderlyingType, T const *> index;
};

} // namespace detail
} // namespace jsondb
