#pragma once

#include <ct/tuple.hpp>
#include <ct/graph.hpp>

#include <boost/hana.hpp>
#include <boost/type_traits.hpp>

#include <nlohmann/json.hpp>

#include <map>
#include <set>
#include <variant>
#include <vector>
#include <optional>
#include <unordered_map>
#include <unordered_set>

namespace jsondb {
template<typename> class Pointer;

namespace detail {

template<typename T>
struct Table;

template<typename T>
struct table_for
{
    using type = ct::tuple<Table<T>>;
};

template<typename T>
struct table_for<T const>
{
    using type = ct::tuple<Table<T> const>;
};

template<typename T>
struct table_of;

template<typename T>
struct table_of<Table<T>>
{
    using type = T;
};

template<std::size_t I, typename F, typename ...T>
void apply(std::tuple<T...> &tuple, F &&functor)
{
    if constexpr(I < sizeof ...(T))
    {
        functor(ct::index<I>{}, std::get<I>(tuple));
        apply<I + 1>(tuple, functor);
    }
}

template<std::size_t I, typename F, typename ...T>
constexpr void apply(ct::tuple<T...> *tuple, F &&functor)
{
    if constexpr(I < sizeof ...(T))
    {
        functor((ct::get_t<ct::index<I>, ct::tuple<T...>>*)nullptr);
        apply<I + 1>(tuple, functor);
    }
}

template<typename Tuple, typename P, std::size_t ...Is>
Tuple _instantiate(P &value, std::index_sequence<Is...>)
{
    return Tuple{((void)sizeof(Is), value)...};
}

template<typename Tuple, typename P>
Tuple instantiate(P &value)
{
    return _instantiate<Tuple>(value, std::make_index_sequence<std::tuple_size_v<Tuple>>{});
}

template<typename>
struct tuple_from;

template<typename ...T>
struct tuple_from<boost::hana::tuple<T...>>
{
    using t = ct::tuple<T...>;
    using type = ct::tuple<ct::tuple<T...>>;
};

template<typename T1, typename T2>
struct tuple_from<boost::hana::pair<T1, T2>>
{
    using t = ct::tuple<T1, T2>;
    using type = ct::tuple<ct::tuple<T1, T2>>;
};

template<typename T>
struct decay
{
    using type = ct::tuple<std::decay_t<T>>;
};

template<typename T>
struct add_const
{
    using type = ct::tuple<T const>;
};

template<typename T>
struct add_const<T const>
{
    using type = ct::tuple<T const>;
};

#define STORAGE_DEFINE_2ND_METAFUNCTION(Name, X, ...) \
    template<typename T>          \
    struct Name                   \
    {                             \
        template<typename X>      \
        struct f __VA_ARGS__ ;    \
    };                            \

STORAGE_DEFINE_2ND_METAFUNCTION(prepend, X, {
    using type = ct::tuple<ct::tuple<T, X>>;
});

STORAGE_DEFINE_2ND_METAFUNCTION(second_is, Tuple, {
    static constexpr bool value = std::is_same_v<T, ct::get_t<ct::index<1>, Tuple>>;
});

template<typename T>
struct find_references
{
    using type = ct::tuple<>;
};

template<typename T>
struct find_references<Pointer<T>>
{
    using type = ct::tuple<T>;
};

template<typename Head, typename ...Tail>
struct find_references<std::variant<Head, Tail...>>
{
    using type = ct::join_t<typename find_references<Head>::type, typename find_references<std::variant<Tail...>>::type>;
};

template<typename Head>
struct find_references<std::variant<Head>>
{
    using type = typename find_references<Head>::type;
};

template<typename T>
struct make_edges
{
    using type = ct::unique_t<ct::transform_t<prepend<T>::template f, ct::transform_t<find_references, typename tuple_from<
        decltype(boost::hana::members(std::declval<T>()))
    >::t>>>;
};

template<typename T>
struct get_first
{
    using type = ct::tuple<ct::get_t<ct::index<0>, T>>;
};

CT_TYPE(bfs);

template<typename ...Visiting, typename ...Visited>
struct bfs<ct::tuple<Visiting...>, ct::tuple<Visited...>>
{
    using joined = ct::tuple<Visited..., Visiting...>;

    using discovered = ct::filter_t
    <
        ct::detail::not_in_tuple<joined>::template filter,
        ct::unique_t<ct::transform_t<
            ct::detail::get_second, ct::transform_t<make_edges, ct::tuple<Visiting...>>
        >>
    >;

    using type = typename bfs<discovered, joined>::type;
};

template<typename ...Visited>
struct bfs<ct::tuple<>, ct::tuple<Visited...>>
{
    using type = ct::tuple<Visited...>;
};

template<typename T, typename ...Ts>
struct edges_to
{
    using type = ct::transform_t<get_first, ct::filter_t<
        second_is<T>::template f, ct::transform_t<make_edges, ct::tuple<Ts...>>
    >>;
};

template<typename R, typename A, typename ...Tail>
auto first_argument(R(*)(A, Tail...)) -> A;

template<typename R, typename T, typename A, typename ...Tail>
auto first_argument(R(T::*)(A, Tail...)) -> A;

template<typename R, typename T, typename A, typename ...Tail>
auto first_argument(R(T::*)(A, Tail...) const) -> A;

template<typename T>
using first_argument_t = decltype(first_argument(&T::operator()));

template<typename F, typename ...Args>
auto is_invocable_with(Args&&... args) -> decltype(
    std::declval<F>()(std::forward<Args>(args)...), std::true_type{}
);

template<typename F>
std::false_type is_invocable_with(...);

template<typename F, typename ...Args>
constexpr bool is_invocable_with_v = decltype(is_invocable_with<F>(std::declval<Args>()...))::value;

template<typename T>
struct is_pointer : std::false_type {};

template<typename T>
struct is_pointer<Pointer<T>> : std::true_type {};

template<typename ...T>
struct is_pointer<std::variant<T...>> : std::conjunction<is_pointer<T>...> {};

template<typename T>
auto is_std_hashable(T*) -> decltype(
    std::hash<T>()(std::declval<T>()), std::true_type{}
);

template<typename T>
std::false_type is_std_hashable(...);

template<typename T>
constexpr bool is_std_hashable_v = decltype(is_std_hashable<T>(0))::value;

template<typename T>
struct is_vector : std::false_type {};

template<typename T>
struct is_vector<std::vector<T>> : std::true_type {};

template<typename T>
struct is_set : std::false_type {};

template<typename K, typename C, typename A>
struct is_set<std::set<K, C, A>> : std::true_type {};

template<typename K, typename H, typename E, typename A>
struct is_set<std::unordered_set<K, H, E, A>> : std::true_type {};

template<typename T>
struct is_map : std::false_type {};

template<typename K, typename V, typename C, typename A>
struct is_map<std::map<K, V, C, A>> : std::true_type {};

template<typename K, typename V, typename H, typename E, typename A>
struct is_map<std::unordered_map<K, V, H, E, A>> : std::true_type {};

template<typename T>
struct is_variant : std::false_type {};

template<typename T>
struct is_variant<std::optional<T>> : std::true_type
{
    static constexpr bool optional = true;
};

template<typename ...T>
struct is_variant<std::variant<T...>> : std::true_type
{
    static constexpr bool optional = false;

    template<typename F>
    static void for_each_type(F &&functor)
    {
        for_each_type_<F, T...>(std::forward<F>(functor));
    }
 private:
    template<typename F, typename U, typename ...Us>
    static void for_each_type_(F &&functor)
    {
        functor((U*)nullptr);
        if constexpr (0 != sizeof ...(Us))
        {
            for_each_type_<F, Us...>(std::forward<F>(functor));
        }
    }
};

template<typename T>
auto has_additional_serializer(T const *p) -> decltype(
    (*p)(std::declval<nlohmann::json &>()), std::true_type{}
);

template<typename T>
std::false_type has_additional_serializer(...);

template<typename T>
constexpr bool has_additional_serializer_v = decltype(has_additional_serializer<T>(0))::value;

template<typename T>
auto has_additional_deserializer(T *p) -> decltype(
    (*p)(std::declval<nlohmann::json const &>()), std::true_type{}
);

template<typename T>
std::false_type has_additional_deserializer(...);

template<typename T>
constexpr bool has_additional_deserializer_v = decltype(has_additional_deserializer<T>(0))::value;

template<typename T>
auto is_persistent_impl(T const *p) -> decltype(
    std::integral_constant<bool, T::Persistent>{}
);

template<typename T>
std::true_type is_persistent_impl(...);

template<typename T>
using is_persistent = decltype(is_persistent_impl<T>(0));

template<typename T>
constexpr bool is_persistent_v = is_persistent<T>::value;

} // namespace detail
} // namespace jsondb
