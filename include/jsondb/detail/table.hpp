#pragma once

#include <jsondb/pointer.hpp>
#include <jsonio/serialize.hpp>
#include <jsondb/detail/index.hpp>
#include <jsondb/detail/types.hpp>
#include <jsondb/search.hpp>

#include <fstream>
#include <regex>
#include <vector>
#include <shared_mutex>
#include <unordered_map>
#include <unordered_set>

namespace jsondb {
namespace detail {

template<typename> class Loader;
template<typename, typename ...> class InvertLock;
template<typename, template <typename> typename> class Lock;

using Batch = std::vector<std::pair<std::filesystem::path, std::optional<std::string>>>;

template<typename T, bool Persistent = is_persistent_v<T>>
class TableBase
{
public:
    std::string const name;

protected:
    TableBase(std::filesystem::path const &root)
    : name{boost::core::demangle(typeid(T).name())}
    , storage(root / std::regex_replace(name, std::regex{"::"}, "/"))
    {
        std::filesystem::create_directories(this->storage / "tmp");
    }

    std::filesystem::path const storage;
};

template<typename T>
class TableBase<T, false>
{
public:
    std::string const name;

protected:
    TableBase(std::filesystem::path const & /* unused */)
    : name{boost::core::demangle(typeid(T).name())}
    {
    }
};

template<typename T>
class Table : public TableBase<T>
{
public:
    Table(std::filesystem::path const &root)
    : TableBase<T>(root) {}

    Opaque<T> make(T &&source)
    {
        if constexpr (TableIndexes<T>::HasUnique)
        {
            if(T *obj = const_cast<T*>(index.find(source)); obj)
            {
                bool changed = false;
                boost::hana::for_each(boost::hana::accessors<T>(), [&, this](auto key) mutable {
                    if(boost::hana::second(key)(*obj) != boost::hana::second(key)(source))
                    {
                        std::swap(boost::hana::second(key)(*obj), boost::hana::second(key)(source));
                        this->index.template modify<std::decay_t<decltype(boost::hana::second(key))>>(source, obj);
                        changed = true;
                    }
                });
                if(changed)
                {
                    dirty.insert(inverted[obj]);
                }
                return obj;
            }
        }
        boost::uuids::uuid uuid = generator();
        T* obj = (objects[uuid] = std::make_unique<T>(std::forward<T>(source))).get();
        inverted[obj] = uuid;
        dirty.insert(uuid);
        index.insert(obj);

        return obj;
    }

    T const * make(T &&source, boost::uuids::uuid &uuid)
    {
        if constexpr (TableIndexes<T>::HasUnique)
        {
            if(T *obj = const_cast<T*>(index.find(source)); obj)
            {
                uuid = inverted[obj];
                bool changed = false;
                boost::hana::for_each(boost::hana::accessors<T>(), [&, this](auto key) mutable {
                    if(boost::hana::second(key)(*obj) != boost::hana::second(key)(source))
                    {
                        std::swap(boost::hana::second(key)(*obj), boost::hana::second(key)(source));
                        this->index.template modify<std::decay_t<decltype(boost::hana::second(key))>>(source, obj);
                        changed = true;
                    }
                });
                if(changed)
                {
                    dirty.insert(uuid);
                }
                return obj;
            }
        }

        uuid = generator();
        T* obj = (objects[uuid] = std::make_unique<T>(std::forward<T>(source))).get();
        inverted[obj] = uuid;
        dirty.insert(uuid);
        index.insert(obj);

        return obj;
    }

    template<typename U, typename Functor>
    void unique(U &&value, Functor &&functor)
    {
        static_assert(TableIndexes<T>::HasUnique);

        if(T const *p = index.find(value); p)
        {
            auto copy = *p;
            T *obj = const_cast<T*>(p);
            if constexpr (detail::is_invocable_with_v<Functor, T *, bool>)
                functor(obj, false);
            else
                functor(obj);
            maybe_changed(copy, obj, inverted[p]);
        }
        else
        {
            auto uuid = generator();

            T* obj = (objects[uuid] = std::make_unique<T>()).get();
            index(*obj) = value;
            inverted[obj] = uuid;
            dirty.insert(uuid);

            if constexpr (detail::is_invocable_with_v<Functor, T *, bool>)
                functor(obj, true);
            else
                functor(obj);

            index(*obj) = std::forward<U>(value);
            index.insert(obj);
        }
    }

    template<typename U, typename Functor>
    void unique(U &&value, Functor &&functor) const
    {
        static_assert(TableIndexes<T>::HasUnique);

        if(T const *p = index.find(std::forward<U>(value)); p)
        {
            if constexpr (detail::is_invocable_with_v<Functor, T *, boost::uuids::uuid const &>)
                functor(const_cast<T*>(p), inverted.find(p)->second);
            else
                functor(const_cast<T*>(p));
        }
    }

    template<typename U>
    bool contains(U &&value) const
    {
        static_assert(TableIndexes<T>::HasUnique);
        return index.find(std::forward<U>(value)) != nullptr;
    }

    T const * load(Opaque<T> o)
    {
        if(auto it = inverted.find(o.pointer); it != inverted.end())
        {
            return o.pointer;
        }
        return nullptr;
    }

    Pointer<T> load(Opaque<T> o) const
    {
        if(auto it = inverted.find(o.pointer); it != inverted.end())
        {
            return o.pointer;
        }
        return nullptr;
    }

    void remove(Opaque<T> o)
    {
        if(auto it = inverted.find(o.pointer); it != inverted.end())
        {
            dirty.insert(it->second);
            index.remove(it->first);
            auto jt = objects.find(it->second);

            garbage.insert(std::make_pair(jt->first, std::move(jt->second)));
            inverted.erase(it);
        }
    }

    void remove(Pointer<T> &p)
    {
        remove(Opaque<T>{const_cast<T*>(p.pointer)});
        p = nullptr;
    }

    void remove(T* &p)
    {
        remove(Opaque<T>{p});
        p = nullptr;
    }

    bool has_garbage() const
    {
        return !garbage.empty();
    }

    template<typename F>
    bool clean(F const *referenced)
    {
        bool found = false;
        index.for_each(referenced, [&found](auto *index, auto *set) mutable {
            found = !set->empty();
            for(auto it = set->begin(); it != set->end(); it = set->erase(it))
            {
                index->set(*it, nullptr);
            }
        });
        return found;
    }

    template<typename Tables>
    void sync(Tables &tables, Batch &batch)
    {
        for(auto &uuid : dirty)
        {
            if constexpr (is_persistent_v<T>)
            {
                auto const out = this->storage / boost::uuids::to_string(uuid);
                if(auto it = objects.find(uuid); it != objects.end())
                {
                    if(garbage.find(uuid) == garbage.end())
                    {
                        nlohmann::json json;
                        tables.serialize(it->second.get(), json);
                        batch.emplace_back(std::pair{out, std::optional{json.dump()}});
                    }
                    else
                    {
                        objects.erase(it);
                        batch.emplace_back(std::pair{out, std::nullopt});
                    }
                }
                else
                {
                    batch.emplace_back(std::pair{out, std::nullopt});
                }
            }
            else
            {
                if(auto it = objects.find(uuid); it != objects.end())
                {
                    if(garbage.find(uuid) != garbage.end())
                    {
                        objects.erase(it);
                    }
                }
            }
        }
        dirty.clear();
    }

    template<typename Functor>
    void each(Functor &&functor)
    {
        for(auto it = objects.begin(); it != objects.end();)
        {
            if(garbage.find(it->first) == garbage.end())
            {
                auto copy = *it->second.get();
                if constexpr (detail::is_invocable_with_v<Functor, T *, boost::uuids::uuid const &>)
                    functor(it->second.get(), it->first);
                else
                    functor(it->second.get());
                if(garbage.find(it->first) != garbage.end())
                {
                    it = objects.erase(it);
                }
                else
                {
                    maybe_changed(copy, it->second.get(), it->first);
                    ++it;
                }
            }
            else
            {
                it = objects.erase(it);
            }
        }
    }

    template<typename F, F T::*MemPtr, typename Functor>
    void each(Search<MemPtr> &&search, Functor &&functor)
    {
        std::set<T const *> objects;
        index.search(std::forward<Search<MemPtr>>(search), objects);
        for(T const *object : objects)
        {
            if(auto it = inverted.find(object); it != inverted.end())
            {
                if(garbage.find(it->second) == garbage.end())
                {
                    auto copy = *object;
                    if constexpr (detail::is_invocable_with_v<Functor, T *, boost::uuids::uuid const &>)
                        functor(const_cast<T*>(object), it->second);
                    else
                        functor(const_cast<T*>(object));
                    if(garbage.find(it->second) == garbage.end())
                    {
                        maybe_changed(copy, const_cast<T*>(object), it->second);
                    }
                    else
                    {
                        this->objects.erase(it->second);
                    }
                }
            }
        }
    }

    template<typename Functor>
    bool load(Pointer<T> const &p, Functor &&functor) 
    {
        if(auto it = inverted.find(p.pointer); it != inverted.end())
        {
            if(garbage.find(it->second) == garbage.end())
            {
                auto copy = *it->first;
                functor(const_cast<T*>(it->first));
                if(garbage.find(it->second) == garbage.end())
                {
                    if constexpr (TableIndexes<T>::HasUnique)
                        maybe_changed(copy, const_cast<T*>(it->first), it->second);
                    else
                        maybe_changed(copy, it->first, it->second);
                }
                else
                {
                    this->objects.erase(it->second);
                }
                return true;
            }
        }
        return false;
    }

    template<typename Functor>
    bool get(boost::uuids::uuid uuid, Functor &&functor) 
    {
        if(auto it = objects.find(uuid); it != objects.end())
        {
            if(garbage.find(uuid) == garbage.end())
            {
                auto copy = *it->second;
                functor(const_cast<T*>(it->second.get()));
                if(garbage.find(uuid) == garbage.end())
                {
                    maybe_changed(copy, it->second.get(), uuid);
                }
                else
                {
                    this->objects.erase(it);
                }
                return true;
            }
        }
        return false;
    }

    template<typename Functor>
    void each(Functor &&functor) const
    {
        for(auto &[uuid, object] : objects)
        {
            if(garbage.find(uuid) == garbage.end())
            {
                if constexpr (detail::is_invocable_with_v<Functor, T const *, boost::uuids::uuid const &>)
                    functor(object.get(), uuid);
                else
                    functor(object.get());
            }
        }
    }

    template<typename F, F T::*MemPtr, typename Functor>
    void each(Search<MemPtr> &&search, Functor &&functor) const
    {
        if constexpr (detail::is_invocable_with_v<Functor, T const *, boost::uuids::uuid const &>)
            index.search(std::forward<Search<MemPtr>>(search), [&, this](T const *object){
                functor(object, inverted.find(object)->second);
            });
        else
            index.search(std::forward<Search<MemPtr>>(search), std::forward<Functor>(functor));
    }

    template<typename F, F T::*MemPtr>
    std::size_t count(Search<MemPtr> &&search) const
    {
        return index.count(std::forward<Search<MemPtr>>(search));
    }

    template<typename F, F T::*MemPtr, typename Functor>
    std::size_t count(Search<MemPtr> &&search, Functor &&functor) const
    {
        if constexpr (detail::is_invocable_with_v<Functor, T const *, boost::uuids::uuid const &>)
            return index.count(std::forward<Search<MemPtr>>(search), [&, this](T const *object){
                return functor(object, inverted.find(object)->second);
            });
        else
            return index.count(std::forward<Search<MemPtr>>(search), std::forward<Functor>(functor));
    }

    template<typename Functor>
    bool get(boost::uuids::uuid uuid, Functor &&functor) const
    {
        if(auto it = objects.find(uuid); it != objects.end())
        {
            if(garbage.find(uuid) == garbage.end())
            {
                functor(it->second.get());
                return true;
            }
        }
        return false;
    }

    std::optional<boost::uuids::uuid> uuidof(Pointer<T> const &p) const
    {
        if(auto it = inverted.find(p.pointer); it != inverted.end())
        {
            return it->second;
        }
        return std::nullopt;
    }

    T const * ptrof(boost::uuids::uuid const &uuid) const
    {
        if(auto it = objects.find(uuid); it != objects.end())
        {
            return it->second.get();
        }
        return nullptr;
    }

private:
    void maybe_changed(T const &copy, std::conditional_t<TableIndexes<T>::HasUnique, T*, T const*> pointer, boost::uuids::uuid const &uuid)
    {
        if constexpr (TableIndexes<T>::HasUnique)
        {
            index(*pointer) = index(copy);
        }

        bool changed = false;
        boost::hana::for_each(boost::hana::accessors<T>(), [&, this](auto key) mutable {
            if(boost::hana::second(key)(copy) != boost::hana::second(key)(*pointer))
            {
                this->index.template modify<std::decay_t<decltype(boost::hana::second(key))>>(copy, pointer);
                changed = true;
            }
        });
        if(changed)
        {
            dirty.insert(uuid);
        }
    }

private:
    TableIndexes<T> index;
    InternalStorage<T> objects;
    InvertedStorage<T> inverted;
    InternalStorage<T> garbage;
    std::unordered_set<boost::uuids::uuid, boost::hash<boost::uuids::uuid>> dirty; 
    boost::uuids::random_generator generator;

private:
    std::unique_ptr<std::shared_mutex> mutex = std::make_unique<std::shared_mutex>();
    template<typename> friend class Loader;
    template<typename, typename ...> friend class InvertLock;
    template<typename, template <typename> typename> friend class Lock;
};

} // namespace detail
} // namespace jsondb
