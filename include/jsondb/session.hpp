#pragma once

#include <jsondb/detail/remote.hpp>
#include <jsondb/detail/meta.hpp>

#include <weblib/client.hpp>

#include <boost/container_hash/hash.hpp>
#include <unordered_map>
#include <string>

#include <crow.h>

namespace jsondb {
namespace detail {

template<typename ...Ts>
struct SessionTraits
{
    using Decay = ct::transform_t<decay, ct::tuple<Ts...>>;
    using Nodes = typename bfs<Decay, ct::tuple<>>::type;

    using Types = ct::join_t
    <
        ct::tuple<Ts...>,
        ct::filter_t
        <
            ct::detail::not_in_tuple<Decay>::template filter,
            Nodes
        >
    >;
};

CT_TYPE(Session_);

template<typename ...Ts>
struct Session_<ct::tuple<Ts...>> : Remote<Ts>...
{
};

} // namespace detail

template<typename ...Ts> 
class Session : protected detail::Session_<typename detail::SessionTraits<Ts...>::Types>
{
public:
    Session(weblib::Client &client, std::string const &prefix, int32_t port)
    : client{client}
    , prefix{prefix}
    , port{port}
    {}

    Session(Session &&) = default;
    Session(Session const &) = default;

public:
    template<typename T>
    auto uuidof(T const *object) const
    {
        return detail::Remote<T>::uuidof(object);
    }

    template<typename T>
    auto uuidof(Pointer<T> const &object) const
    {
        return detail::Remote<T>::uuidof(object);
    }

    template<typename Functor>
    bool each(Functor &&functor)
    {
        using T = std::remove_cv_t<std::remove_pointer_t<detail::first_argument_t<Functor>>>;
        return detail::Remote<T>::each(this, std::forward<Functor>(functor));
    }

    template<auto U, typename Functor>
    bool each(Search<U> &&search, Functor &&functor)
    {
        using T = typename detail::MemberTraits<U>::Owner;
        return detail::Remote<T>::each(this, std::forward<Search<U>>(search), std::forward<Functor>(functor));
    }

    template<typename T>
    auto make(T &&value)
    {
        return detail::Remote<T>::make(this, std::forward<T>(value));
    }

    template<typename T>
    auto make(T &&value, boost::uuids::uuid &id)
    {
        return detail::Remote<T>::make(this, std::forward<T>(value), &id);
    }

    template<typename T>
    bool remove(T const *object)
    {
        return detail::Remote<T>::remove(this, object);
    }

    template<typename Functor>
    bool get(boost::uuids::uuid const &uuid, Functor &&functor, bool cache = false)
    {
        using T = std::remove_cv_t<std::remove_pointer_t<detail::first_argument_t<Functor>>>;
        return detail::Remote<T>::get(this, uuid, std::forward<Functor>(functor), cache);
    }

    template<typename V, typename Functor>
    std::enable_if_t<detail::TableIndexes<std::remove_cv_t<std::remove_pointer_t<detail::first_argument_t<Functor>>>>::HasUnique, bool>
    unique(V &&value, Functor &&functor)
    {
        using T = std::remove_cv_t<std::remove_pointer_t<detail::first_argument_t<Functor>>>;
        return detail::Remote<T>::unique(this, std::forward<V>(value), std::move(functor));
    }

private:
    template<typename T> friend class detail::Remote;
    friend class detail::Cache<Session<Ts...>>;

    weblib::Client &client;
    std::string const &prefix;
    int32_t port;
    bool cache{false};
};

} // namespace jsondb
