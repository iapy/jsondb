#pragma once
#include <jsondb/search.hpp>

#include <type_traits>
#include <ct/tuple.hpp>

namespace jsondb {

template<auto...>
struct Index;

template<auto MemPtr, auto ...Tail>
struct Index<MemPtr, Tail...>
{
    using type = std::conditional_t<
        !detail::is_pointer<typename detail::MemberTraits<MemPtr>::Type>::value,
        ct::join_t<ct::tuple<typename detail::MemberTraits<MemPtr>>, typename Index<Tail...>::type>,
        typename Index<Tail...>::type
    >;
};

template<>
struct Index<>
{
    using type = ct::tuple<>;
};

} // namespace jsondb
