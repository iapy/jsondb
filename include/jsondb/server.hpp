#pragma once
#include <jsondb/pointer.hpp>
#include <jsondb/lock.hpp>
#include <jsondb/tags.hpp>

#include <jsondb/detail/api.hpp>
#include <jsondb/detail/meta.hpp>

#include <msgbus/state.hpp>
#include <msgbus/handler.hpp>
#include <msgbus/workers.hpp>

#include <weblib/router.hpp>

#include <cg/bind.hpp>
#include <cg/config.hpp>
#include <cg/component.hpp>

#include <crow.h>

#include <filesystem>
#include <mutex>
#include <shared_mutex>

namespace jsondb {

template<typename ...Ts>
struct Server : cg::Component
{
    struct Writer;

    struct Service
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            using S = msgbus::State<Service>;
            using W = msgbus::State<Writer>;

            Interface(Base &&base) : cg::Bind<Interface, Base>{base}
            {
                this->state.batches.reserve(64);
            }

            auto handler()
            {
                using Self = Interface<Base>;
                return msgbus::Handler
                {
                    msgbus::Event
                    {
                        [this]{ return this->state.dirty.any(); },
                        this->bind(&Self::process)
                    },
                    msgbus::Event
                    {
                        [this]{ return this->state.S::is(msgbus::Stopping); },
                        this->bind(&Self::finish)
                    }
                };
            }
        private:
            void process(std::unique_lock<std::mutex> &lock)
            {
                auto dirty = this->state.dirty;
                lock.unlock();

                detail::Batch batch;
                wash(dirty, batch);
               
                if(!batch.empty())
                {
                    this->state.W::notify([&, this]{
                        this->state.batches.emplace_back(std::move(batch));
                    });
                }
            }

            void finish(std::unique_lock<std::mutex> &lock)
            {
                if(lock)
                {
                    // This happens when process() is called right before finish()
                    lock.unlock();
                }
                detail::Batch batch;
                {
                    detail::LockGuard<sizeof ...(Ts)> guard{[](auto){}};
                    Lock<Ts...> lock(this->state.tables, guard);
                    this->state.tables.sync(batch);
                }

                if(!batch.empty())
                {
                    this->state.W::notify([&, this]{
                        this->state.batches.emplace_back(std::move(batch));
                    });
                }
            }

            void wash(std::bitset<sizeof ...(Ts)> &dirty, detail::Batch &batch)
            {
                while(dirty.any())
                {
                    std::bitset<sizeof ...(Ts)> collected{0};
                    this->state.tables.each([this, &dirty, &collected, &batch](auto index, auto &table) {
                        using T = typename detail::table_of<std::remove_reference_t<decltype(table)>>::type;

                        bool has_garbage = false;
                        if(dirty[index.value])
                        {
                            detail::DirectLock<T> lock(this->state.tables);
                            this->state.tables.sync(table, batch);
                            has_garbage = table.has_garbage();

                            this->state.S::lock([&, this]{
                                this->state.dirty[index.value] = has_garbage;
                            });
                        }
                        if(has_garbage)
                        {
                            detail::InvertLock<T, Ts...> lock(this->state.tables);
                            lock.collect_garbage(table, collected);
                        }
                    });

                    this->state.S::notify([&, this] {
                        dirty = (this->state.dirty |= collected);
                    });
                }
            }
        };
    };

    struct Writer
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            using S = msgbus::State<Service>;
            using W = msgbus::State<Writer>;

            auto handler()
            {
                using Self = Interface<Base>;
                return msgbus::Handler
                {
                    msgbus::Event
                    {
                        [this] { return !this->state.batches.empty(); },
                        this->bind(&Self::process)
                    },
                    msgbus::Event
                    {
                        [this] { return this->state.W::is(msgbus::Stopping); },
                        this->bind(&Self::finish)
                    }
                };
            }
        private:
            void process(std::unique_lock<std::mutex> &lock)
            {
                decltype(this->state.batches) batches;
                batches.reserve(64);
                batches.swap(this->state.batches);
                lock.unlock();

                write(std::move(batches));
            }

            void finish(std::unique_lock<std::mutex> &lock)
            {
                if(lock)
                {
                    // This happens when process() is called right before finish()
                    lock.unlock();
                }
                this->state.S::wait([this]{
                    return this->state.S::is(msgbus::Stopped);
                });

                decltype(this->state.batches) batches;
                this->state.W::lock([&, this]{
                    batches.swap(this->state.batches);
                });

                if(!batches.empty())
                {
                    write(std::move(batches));
                }
            }

            void write(std::vector<detail::Batch> &&batches)
            {
                for(auto const &batch : batches)
                {
                    for(auto const &[out, content] : batch)
                    {
                        if(content)
                        {
                            auto tmp = out.parent_path() / "tmp" / out.filename();
                            {
                                std::ofstream{tmp} << *content;
                            }
                            std::filesystem::rename(tmp, out);
                        }
                        else
                        {
                            std::filesystem::remove(out);
                        }
                    }
                }
            }
        };
    };

    struct Client
    {
        template<typename Base>
        struct Interface : Base
        {
            using S = msgbus::State<Service>;
            using W = msgbus::State<Writer>;

            template<typename Functor>
            auto transaction(Functor &&functor)
            {
                detail::LockGuard<sizeof ...(Ts)> guard{[this](auto const &dirty){
                    this->state.S::notify([this, &dirty]{
                        this->state.dirty |= dirty;
                    });
                }};
                if constexpr (std::is_same_v<decltype(functor({this->state.tables, guard})), void>)
                    functor({this->state.tables, guard});
                else
                    return functor({this->state.tables, guard});
            }

            void wait()
            {
                this->state.S::wait([this]{
                    return !this->state.dirty.any();
                });
            }

            void stop()
            {
                this->state.S::stop();
                this->state.W::stop();
            }
        };
    };

    struct Rest
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            template<typename App>
            void setup(App &app)
            {
                using Self = Interface<Base>;
                this->state.tables.each([this, &app](auto, auto &table) mutable {
                    using T = typename detail::table_of<std::remove_reference_t<decltype(table)>>::type;
                    auto const url = '/' + std::regex_replace(table.name, std::regex{"::"}, ":");

                    WEBLIB_BP_DYNAMIC_ROUTE(std::string{url}).methods("GET"_method)(this->bind(&Self::all<T>));
                    WEBLIB_BP_DYNAMIC_ROUTE(std::string{url}).methods("PUT"_method)(this->bind(&Self::edit<T>));
                    WEBLIB_BP_DYNAMIC_ROUTE(std::string{url}).methods("POST"_method)(this->bind(&Self::make<T>));
                    WEBLIB_BP_DYNAMIC_ROUTE(std::string{url}).methods("DELETE"_method)(this->bind(&Self::remove<T>));

                    using Indexes = detail::make_indexes<T>;
                    detail::template apply<0>((typename Indexes::indexes*)nullptr, [&, this](auto index){
                        using Name = ct::get_t<ct::index<0>, std::remove_pointer_t<decltype(index)>>;
                        using Type = ct::get_t<ct::index<1>, std::remove_pointer_t<decltype(index)>>;
                        WEBLIB_BP_DYNAMIC_ROUTE(url + "/" + Name::c_str()).methods("GET"_method)(this->bind(&Self::search<T, Type>));
                    });

                    detail::template apply<0>((typename Indexes::uniques*)nullptr, [&, this](auto index){
                        using Name = ct::get_t<ct::index<0>, std::remove_pointer_t<decltype(index)>>;
                        using Type = ct::get_t<ct::index<1>, std::remove_pointer_t<decltype(index)>>;
                        WEBLIB_BP_DYNAMIC_ROUTE(url + "/" + Name::c_str()).methods("GET"_method)(this->bind(&Self::unique<T, Type>));
                    });

                    WEBLIB_BP_DYNAMIC_ROUTE(url + "/<string>").methods("GET"_method)(this->bind(&Self::get<T>));
                });

                std::ostringstream js, py;
                api::js(js, this->state.prefix + "/" + this->state.blueprint.prefix(), this->state.tables);
                api::py(py, this->state.prefix + "/" + this->state.blueprint.prefix(), this->state.tables);

                WEBLIB_BP_ROUTE("/api.py").methods("GET"_method)([s=py.str()](){ return s; });

                std::string jss = js.str();
                WEBLIB_BP_ROUTE("/api.js").methods("GET"_method)([s=jss](){ return s; });
                WEBLIB_BP_ROUTE("/api.min.js").methods("GET"_method)([s=api::minify(std::move(jss))](){ return s; });

                app.register_blueprint(this->state.blueprint);
            }
        private:
            crow::response jsorjson(crow::request const &req, nlohmann::json const &result)
            {
                crow::response res{crow::status::OK};
                if(auto var = req.url_params.get("var"); var)
                {
                    res.set_header("Content-Type", "text/javascript");
                    res.body = [&]{
                        std::ostringstream ss;
                        ss << "var " << var << " = " << result.dump() << ';';
                        return ss.str();
                    }();
                }
                else
                {
                    res.set_header("Content-Type", "application/json");
                    res.body = result.dump();
                }
                return res;
            }

            template<typename T>
            crow::response all(crow::request const &request)
            {
                auto result = nlohmann::json::array();
                this->local(tag::Client{}).transaction([&](Lock<T const> lock){
                    lock.each([&](T const *object, boost::uuids::uuid const &uuid){
                        result.push_back(nlohmann::json{});
                        lock.serialize(object, result.back());
                        if constexpr (detail::has_additional_serializer_v<T>)
                        {
                            const_cast<T const &>(*object)(result.back());
                        }
                        result.back()["_id"] = boost::uuids::to_string(uuid);
                    });
                });
                return jsorjson(request, result);
            }

            template<typename T, typename Index>
            crow::response unique(crow::request const &request)
            {
                using Traits = typename detail::search_for<Index>::traits;

                auto result = nlohmann::json::array();
                this->local(tag::Client{}).transaction([&](Lock<T const> lock) {
                    if(char *value = request.url_params.get("eq"); value) {
                        lock.unique(boost::lexical_cast<typename Traits::Type>(value),[&](T const *object, boost::uuids::uuid const &uuid) {
                            result.push_back(nlohmann::json{});
                            lock.serialize(object, result.back());
                            if constexpr (detail::has_additional_serializer_v<T>)
                            {
                                const_cast<T const &>(*object)(result.back());
                            }
                            result.back()["_id"] = boost::uuids::to_string(uuid);
                        });
                    } else {
                        lock.each([&](T const *object, boost::uuids::uuid const &uuid) {
                            result.push_back(nlohmann::json{});
                            lock.serialize(object, result.back());
                            if constexpr (detail::has_additional_serializer_v<T>)
                            {
                                const_cast<T const &>(*object)(result.back());
                            }
                            result.back()["_id"] = boost::uuids::to_string(uuid);
                        });
                    }
                });
                return jsorjson(request, result);
            }

            template<typename T, typename Index>
            crow::response search(crow::request const &request)
            {
                auto result = nlohmann::json::array();

                using Traits = typename detail::search_for<Index>::traits;
                using Search = typename detail::search_for<Index>::type;

                this->local(tag::Client{}).transaction([&](Lock<T const> lock){
                    Search search;
                    bool empty = false;
                    char *value = nullptr;
                    using namespace boost::hana;
                    for_each(accessors<Search>(), [&](auto key) {
                        if((value = request.url_params.get(first(key).c_str()))) {
                            if constexpr (detail::is_pointer<typename Traits::Type>::value) {
                                if constexpr (detail::is_variant<typename Traits::Type>::value)
                                {
                                    if(!std::strcmp(value, "null")) {
                                        second(key)(search) = nullptr;
                                    } else {
                                        auto uuid = boost::lexical_cast<boost::uuids::uuid>(value);
                                        detail::is_variant<typename Traits::Type>::for_each_type([&](auto *p){
                                            using U = typename std::remove_pointer_t<decltype(p)>::UnderlyingType;
                                            if(!empty && lock.get(uuid, [&](U object){
                                                second(key)(search) = object;
                                            }))
                                            {
                                                empty = false;
                                            }
                                        });
                                    }
                                }
                                else
                                {
                                    if(!std::strcmp(value, "null")) {
                                        second(key)(search) = nullptr;
                                    } else {
                                        auto uuid = boost::lexical_cast<boost::uuids::uuid>(value);
                                        empty = !lock.get(uuid, [&](typename Traits::Type::UnderlyingType object){
                                            second(key)(search) = object;
                                        });
                                    }
                                }
                            } else if constexpr (detail::is_vector<typename Traits::Type>::value || detail::is_set<typename Traits::Type>::value) {
                                if(!(empty = !std::strcmp(value, "null"))) {
                                    if constexpr (std::is_same_v<typename Traits::Type::value_type, DateTime>)
                                        second(key)(search) = jsonio::Serializer<DateTime>::parse(value);
                                    else
                                        second(key)(search) = boost::lexical_cast<typename Traits::Type::value_type>(value);
                                }
                            } else if constexpr (detail::is_map<typename Traits::Type>::value) {
                                second(key)(search) = boost::lexical_cast<std::remove_cv_t<typename Traits::Type::value_type::first_type>>(value);
                            } else if constexpr (std::is_same_v<typename Traits::Type, DateTime>) {
                                second(key)(search) = jsonio::Serializer<DateTime>::parse(value);
                            } else if constexpr (std::is_same_v<typename Traits::Type, bool>) {
                                second(key)(search) = !std::strcmp(value, "true") || !std::strcmp(value, "True") || !std::strcmp(value, "1");
                            } else if constexpr (std::is_enum_v<typename Traits::Type>) {
                                if constexpr (std::is_same_v<std::underlying_type_t<typename Traits::Type>, char>)
                                    second(key)(search) = static_cast<typename Traits::Type>(*value);
                                else
                                    second(key)(search) = static_cast<typename Traits::Type>(boost::lexical_cast<std::underlying_type_t<typename Traits::Type>>(value));
                            } else {
                                second(key)(search) = boost::lexical_cast<typename Traits::Type>(value);
                            }
                        }
                    });

                    if(!empty) {
                        lock.each(std::move(search), [&](T const *object, boost::uuids::uuid const &uuid) {
                            result.push_back(nlohmann::json{});
                            lock.serialize(object, result.back());
                            if constexpr (detail::has_additional_serializer_v<T>)
                            {
                                const_cast<T const &>(*object)(result.back());
                            }
                            result.back()["_id"] = boost::uuids::to_string(uuid);
                        }); 
                    }
                });

                return jsorjson(request, result);
            }

            template<typename T>
            crow::response get(crow::request const &request, std::string id)
            {
                std::istringstream ss{id};
                std::vector<boost::uuids::uuid> uuids;
                for(std::string u; std::getline(ss, u, ',');)
                    uuids.push_back(boost::lexical_cast<boost::uuids::uuid>(u));

                auto result = uuids.size() == 1 ? nlohmann::json{} : nlohmann::json::array();
                return this->local(tag::Client{}).transaction([&](Lock<T const> lock) {
                    for(auto const &uuid : uuids)
                    {
                        bool found = lock.get(uuid, [&](T const *object) {
                            if(uuids.size() == 1)
                            {
                                lock.serialize(object, result);
                                if constexpr (detail::has_additional_serializer_v<T>)
                                {
                                    const_cast<T const &>(*object)(result);
                                }
                                result["_id"] = boost::uuids::to_string(uuid);
                            }
                            else
                            {
                                result.push_back(nlohmann::json{});
                                lock.serialize(object, result.back());
                                if constexpr (detail::has_additional_serializer_v<T>)
                                {
                                    const_cast<T const &>(*object)(result.back());
                                }
                                result.back()["_id"] = boost::uuids::to_string(uuid);
                            }
                        });
                        if(!found)
                        {
                            return crow::response{crow::status::NOT_FOUND};
                        }
                    }
                    return jsorjson(request, result);
                });
            }

            template<typename T>
            crow::response remove(crow::request const &request)
            {
                return this->local(tag::Client{}).transaction([&](Lock<T> lock) {
                    std::istringstream ss(request.body); 
                    for(std::string u; std::getline(ss, u, ',');)
                    {
                        if(!lock.get(boost::lexical_cast<boost::uuids::uuid>(u), [&](T *object) {
                            lock.remove(object);
                        })) return crow::response{crow::status::NOT_FOUND};
                    }
                    return crow::response{crow::status::OK};
                });
            }

            template<typename T>
            crow::response make(crow::request const &request)
            {
                auto json = nlohmann::json::parse(request.body);
                return this->local(tag::Client{}).transaction([&](Lock<T> lock) {
                    auto make_ = [&lock](auto &j) {
                        T object{};
                        lock.deserialize(&object, j);
                        if constexpr (detail::has_additional_deserializer_v<T>)
                        {
                            object(const_cast<nlohmann::json const &>(j));
                        }

                        boost::uuids::uuid uuid;
                        auto saved = lock.make(std::move(object), uuid);
                        lock.serialize(saved, j);
                        if constexpr (detail::has_additional_serializer_v<T>)
                        {
                            const_cast<T const &>(*saved)(j);
                        }
                        j["_id"] = boost::uuids::to_string(uuid);
                    };

                    if(json.is_array())
                    {
                        for(auto &o : json)
                        {
                            make_(o);
                        }
                    }
                    else
                    {
                        make_(json);
                    }
                    return crow::response{crow::status::OK, "json", json.dump()};
                });
            }

            template<typename T>
            crow::response edit(crow::request const &request)
            {
                auto json = nlohmann::json::parse(request.body);
                return this->local(tag::Client{}).transaction([&](Lock<T> lock) {
                    auto edit_ = [&lock](auto &j) {
                        try {
                            auto uuid = boost::lexical_cast<boost::uuids::uuid>(j["_id"].template get<std::string>());
                            if(!lock.get(uuid, [&](T *object) {
                                lock.deserialize(object, j);
                                if constexpr (detail::has_additional_deserializer_v<T>)
                                {
                                    (*object)(const_cast<nlohmann::json const &>(j));
                                }

                                lock.serialize(object, j);
                                if constexpr (detail::has_additional_serializer_v<T>)
                                {
                                    const_cast<T const &>(*object)(j);
                                }
                            }))
                            {
                                j = nullptr;
                            }
                        } catch(...) {
                            j = nullptr;
                        }
                    };
                    if(json.is_array())
                    {
                        for(auto &o : json)
                        {
                            edit_(o);
                        }
                    }
                    else
                    {
                        edit_(json);
                    }
                    return crow::response{crow::status::OK, "json", json.dump()};
                });
            }
        };
    };

    template<typename Resolver>
    struct State
    : msgbus::State<Service>
    , msgbus::State<Writer>
    {
        std::filesystem::path const root;
        crow::Blueprint blueprint;
        std::string const prefix = "";

        State(std::filesystem::path const &root, std::string const &blueprint = "")
        : root{root}, blueprint{blueprint}, tables{root} {}

        CONFIG_DECLARE_STATE_CONSTRUCTORS(root, blueprint, prefix)
        , tables{root} {}

        typename detail::Tables<Ts...> tables;
        std::bitset<sizeof ...(Ts)> dirty{0};
        std::vector<detail::Batch> batches;
    };

    using Ports = ct::map<
        ct::pair<tag::Client, Client>,
        ct::pair<weblib::tag::Handler, Rest>
    >;
    using Services = msgbus::Workers<Service, Writer>;
};

} // namespace jsondb
