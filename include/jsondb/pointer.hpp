#pragma once

#include <jsonio/serializer.hpp>
#include <jsondb/detail/meta.hpp>

#include <boost/container_hash/hash.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid_io.hpp>

namespace jsondb {
namespace detail {

template<typename> class Table;
template<typename> class Remote;
template<typename> class Loader;
template<typename, typename, typename> class TableIndex;

} // namespace detail

template<typename T> class Pointer;

template<typename...> class Lock;
template<typename...> class Session;

template<typename T>
class Opaque
{
public:
    Opaque(T const *pointer) : pointer{pointer} {}
private:
    T const *pointer;
    friend class Pointer<T>;
    friend class detail::Table<T>;
};

template<typename T>
class Pointer
{
public:
    Pointer() = default;
    Pointer(Pointer &&) = default;
    Pointer(Pointer const &other) = default;
    Pointer(T const *p) : pointer{p} {}
    Pointer(Opaque<T> const &p) : pointer{p.pointer} {}
    Pointer(std::nullptr_t) : pointer{nullptr} {}

    T const* operator -> () { return pointer; }
    T const* const operator -> () const { return pointer; }

    Pointer<T> &operator = (Pointer const &other)
    {
        pointer = other.pointer;
        return *this;
    }

    Pointer<T> &operator = (Pointer &&other) = default;
    bool operator ! () const { return pointer == nullptr; }

    bool operator == (std::nullptr_t) const { return pointer == nullptr; }
    bool operator == (Pointer<T> const &other) const { return pointer == other.pointer; }
    bool operator != (Pointer<T> const &other) const { return pointer != other.pointer; }

    using UnderlyingType = T const *;
private:
    T const * pointer = nullptr;

    friend class detail::Table<T>;
    friend class detail::Remote<T>;

    template<typename, typename, typename>
    friend class detail::TableIndex;

    template<typename, typename>
    friend class jsonio::Serializer;
};

} // namespace jsondb

namespace jsonio {

template<typename T>
struct Serializer<jsondb::Pointer<T>, void>
{
    template<typename ...Ts>
    static void save(nlohmann::json &json, jsondb::Pointer<T> const &value, std::tuple<Ts...> const *tables)
    {
        if(auto const uuid = std::get<jsondb::detail::Table<T>>(*tables).uuidof(value); uuid)
        {
            json = boost::uuids::to_string(*uuid);
        }
        else
        {
            json = nullptr;
        }
    }

    template<typename ...Ts>
    static void save(nlohmann::json &json, jsondb::Pointer<T> const &value, jsondb::Lock<Ts...> *lock)
    {
        if(auto const uuid = lock->uuidof(value.pointer); uuid)
        {
            json = boost::uuids::to_string(*uuid);
        }
        else
        {
            json = nullptr;
        }
    }

    template<typename ...Ts>
    static void save(nlohmann::json &json, jsondb::Pointer<T> const &value, jsondb::Session<Ts...> *session)
    {
        if(auto const uuid = session->uuidof(value.pointer); uuid)
        {
            json = boost::uuids::to_string(*uuid);
        }
        else
        {
            json = nullptr;
        }
    }

    template<typename Ts>
    static void load(nlohmann::json const &json, jsondb::Pointer<T> &value, jsondb::detail::Loader<Ts> *tables)
    {
        using Map = std::unordered_map<
            boost::uuids::uuid,
            std::vector<jsondb::Pointer<T>*>,
            boost::hash<boost::uuids::uuid>>;
        if(json.is_null())
        {
            value = nullptr;
        }
        else if constexpr(jsondb::detail::is_persistent_v<T>)
        {
            auto uuid = boost::lexical_cast<boost::uuids::uuid>(json.get<std::string>());
            tables->Map::operator [](uuid).push_back(&value);
        }
    }

    template<typename ...Ts>
    static void load(nlohmann::json const &json, jsondb::Pointer<T> &value, jsondb::Lock<Ts...> *lock)
    {
        value = nullptr;
        if(!json.is_null())
        {
            auto uuid = boost::lexical_cast<boost::uuids::uuid>(json.get<std::string>());
            lock->get(uuid, [&value](T const *ptr) mutable {
                value = ptr;
            });
        }
    }

    template<typename ...Ts>
    static void load(nlohmann::json const &json, jsondb::Pointer<T> &value, jsondb::Session<Ts...> *session)
    {
        value = nullptr;
        if(!json.is_null())
        {
            auto uuid = boost::lexical_cast<boost::uuids::uuid>(json.get<std::string>());
            session->get(uuid, [&value](T const *ptr) mutable {
                value = ptr;
            }, true);
        }
    }
};

namespace detail {

template<typename T>
constexpr bool is_container<jsondb::Pointer<T>> = true;

template<typename T>
struct variant_key_t<jsondb::Pointer<T>>
{
    static std::string get()
    {
        return boost::core::demangle(typeid(T).name());
    }
};

} // namespace detail
} // namespace jsonio
