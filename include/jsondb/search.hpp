#pragma once
#include <jsondb/detail/meta.hpp>
#include <boost/hana.hpp>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include <type_traits>
#include <optional>
#include <vector>

namespace jsondb {
namespace detail {

template<auto>
struct MemberTraits;

template<typename T, typename F, F T::*P>
struct MemberTraits<P>
{
    using Owner = T;
    using Type  = F;
};

template<typename Index>
struct search_for;

template<typename Pointer>
struct underlying_for
{
    using type = std::optional<typename Pointer::UnderlyingType>;
};

template<typename ...Ts>
struct underlying_for<std::variant<Ts...>>
{
    struct type
    {
        type() = default; // constructs std::monostate

        type(std::nullptr_t) : value{
            ct::get_t<ct::index<0>, ct::tuple<typename Ts::UnderlyingType...>>{nullptr}
        } {}

        template<typename T>
        type(T const *p) : value{p} {}

        std::variant<std::monostate, typename Ts::UnderlyingType...> value;
    };
};

template<typename T>
struct custom_index
{
    static constexpr bool value{false};
};

} // namespace detail

template<auto MemPtr, typename Enabler = void>
struct Search;

template<auto MemPtr>
struct Search<MemPtr, std::enable_if_t<
    detail::is_vector<typename detail::MemberTraits<MemPtr>::Type>::value
||  detail::is_set<typename detail::MemberTraits<MemPtr>::Type>::value
>>
{
    BOOST_HANA_DEFINE_STRUCT(Search,
        (typename detail::MemberTraits<MemPtr>::Type::value_type, contains)
    );
};

template<auto MemPtr>
struct Search<MemPtr, std::enable_if_t<
    detail::is_map<typename detail::MemberTraits<MemPtr>::Type>::value
>>
{
    BOOST_HANA_DEFINE_STRUCT(Search,
        (std::remove_cv_t<typename detail::MemberTraits<MemPtr>::Type::value_type::first_type>, contains)
    );
};

template<auto MemPtr>
struct Search<MemPtr, std::enable_if_t<detail::is_pointer<typename detail::MemberTraits<MemPtr>::Type>::value>>
{
    BOOST_HANA_DEFINE_STRUCT(Search,
        (typename detail::underlying_for<typename detail::MemberTraits<MemPtr>::Type>::type, eq)
    );

    template<typename T>
    struct Rebind
    {
        std::optional<std::enable_if_t<detail::is_variant<typename detail::MemberTraits<MemPtr>::Type>::value, T const>> eq;
    };
};

template<auto MemPtr>
struct Search<MemPtr, std::enable_if_t<std::is_same_v<typename detail::MemberTraits<MemPtr>::Type, bool>>>
{
    BOOST_HANA_DEFINE_STRUCT(Search,
        (std::optional<typename detail::MemberTraits<MemPtr>::Type>, eq)
    );
};

template<auto MemPtr>
struct Search<MemPtr, std::enable_if_t<std::is_same_v<typename detail::MemberTraits<MemPtr>::Type, nlohmann::json>>>
{
    BOOST_HANA_DEFINE_STRUCT(Search,
        (std::optional<typename detail::MemberTraits<MemPtr>::Type>, query)
    );
};

template<auto MemPtr>
struct Search<MemPtr, std::enable_if_t<std::is_enum_v<typename detail::MemberTraits<MemPtr>::Type>>>
{
    BOOST_HANA_DEFINE_STRUCT(Search,
        (std::optional<typename detail::MemberTraits<MemPtr>::Type>, eq)
    );
};

template<auto MemPtr>
struct Search<MemPtr, std::enable_if_t<
    !detail::is_pointer<typename detail::MemberTraits<MemPtr>::Type>::value
&&  !detail::is_vector<typename detail::MemberTraits<MemPtr>::Type>::value
&&  !detail::is_set<typename detail::MemberTraits<MemPtr>::Type>::value
&&  !detail::is_map<typename detail::MemberTraits<MemPtr>::Type>::value
&&  !std::is_same_v<typename detail::MemberTraits<MemPtr>::Type, bool>
&&  !std::is_same_v<typename detail::MemberTraits<MemPtr>::Type, nlohmann::json>
&&  !std::is_enum_v<typename detail::MemberTraits<MemPtr>::Type>
&&  !detail::custom_index<typename detail::MemberTraits<MemPtr>::Type>::value
>>
{
    BOOST_HANA_DEFINE_STRUCT(Search,
        (std::optional<typename detail::MemberTraits<MemPtr>::Type>, gt),
        (std::optional<typename detail::MemberTraits<MemPtr>::Type>, eq),
        (std::optional<typename detail::MemberTraits<MemPtr>::Type>, lt)
    );
};

template<auto T>
struct Search<T, std::enable_if_t<boost::hana::Struct<decltype(std::declval<T>())>::value>>
{
    using Type = decltype(std::declval<T>());
    BOOST_HANA_DEFINE_STRUCT(Search,
        (boost::uuids::uuid, id)
    );
};

namespace detail {

template<typename MemPtr, MemPtr ptr>
struct search_for<boost::hana::struct_detail::member_ptr<MemPtr, ptr>>
{
    using type = Search<ptr>;
    using traits = MemberTraits<ptr>;
};

} // namespace detail
} // namespace jsondb
