#include "lib/framework.hpp"

JSONDB_TEST_SUITE(EphemeralRef, Ephemeral, Persistent)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    bool first{true};
    run_test(
        [&](auto db)
        {
            db.transaction([&](Lock<Ephemeral const> lock){
                BOOST_TEST(lock.count(Search<&Ephemeral::name>{.eq = "foo"}) == static_cast<int>(first));
                BOOST_TEST(lock.count(Search<&Ephemeral::name>{.eq = "bar"}) == static_cast<int>(first));
                BOOST_TEST(lock.count(Search<&Ephemeral::name>{.eq = "baz"}) == 0);
            });

            db.transaction([&](Lock<Persistent const> lock){
                lock.each([&](Persistent const *p){
                    if(first)
                    {
                        BOOST_TEST(p->name == p->eref->name);
                    }
                    else
                    {
                        BOOST_TEST(p->eref == nullptr);
                    }
                });
            });
            first = false;
        },
        [&](auto db, auto &sync)
        {
            for(auto name : {"foo", "bar"})
            {
                db.transaction([&](Lock<Ephemeral, Persistent> lock){
                    lock.make(Persistent{
                        .name = name,
                        .eref = lock.make(Ephemeral{
                            .name = name
                        })
                    });
                });
            }
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
