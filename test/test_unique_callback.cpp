#include "lib/framework.hpp"

JSONDB_TEST_SUITE(UniqueCallback, Config)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;
    run_test(
        [](auto db)
        {
            BOOST_TEST(2 == Config::counter());
        },
        [](auto db, auto &sync)
        {
            db.transaction([&](Lock<Config> lock){
                lock.unique("foo", [&](Config *config, bool created) {
                    config->value["a"] = 1;
                    BOOST_TEST(created);
                });
            });
            db.transaction([&](Lock<Config> lock){
                lock.unique("foo", [&](Config *config, bool created) {
                    config->value["a"] = 2;
                    BOOST_TEST(!created);
                });
            });
            return 0;
        },
        [](auto db, auto &sync)
        {
            db.transaction([&](Lock<Config> lock){
                lock.unique("bar", [&](Config *config, bool created) {
                    config->value["b"] = 1;
                    BOOST_TEST(created);
                });
            });
            db.transaction([&](Lock<Config> lock){
                lock.unique("bar", [&](Config *config, bool created) {
                    config->value["b"] = 2;
                    BOOST_TEST(!created);
                });
            });
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
