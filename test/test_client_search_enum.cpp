#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(ClientSearchEnum, jsondb::tester::JsondbClient)

BOOST_AUTO_TEST_CASE(Test)
{
    run_test([](auto sv, auto db){
        db.transaction(sv, [&](Session<Enum> session) {
            session.make(Enum{
                .option = Enum::Option::A,
                .name = "a1"
            });
            session.make(Enum{
                .option = Enum::Option::A,
                .name = "a2"
            });
            session.make(Enum{
                .option = Enum::Option::A,
                .name = "a3"
            });
            session.make(Enum{
                .option = Enum::Option::B,
                .name = "b1"
            });
            session.make(Enum{
                .option = Enum::Option::B,
                .name = "b2"
            });
        });

        db.transaction(sv, [&](Session<Enum> session) {
            std::size_t count{0};
            session.each(Search<&Enum::option>{.eq = Enum::Option::A}, [&](Enum *e) {
                if(e->name == "a1")
                {
                    e->option = Enum::Option::B;
                }
                ++count;
            });
            BOOST_TEST(3 == count);
        });

        db.transaction(sv, [&](Session<Enum> session) {
            std::size_t count{0};
            session.each(Search<&Enum::option>{.eq = Enum::Option::A}, [&](Enum const *e) {
                ++count;
            });
            BOOST_TEST(2 == count);
        });
    });
}

BOOST_AUTO_TEST_SUITE_END()
