#include "lib/framework.hpp"

JSONDB_TEST_SUITE(Order, Json)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    auto J = R"({})"_json;

    run_test(
        [&](auto db)
        {
            db.transaction([&](Lock<Json const> lock){
                std::size_t count = 0;
                std::optional<std::string> previous;
                lock.each(Search<&Json::name>{}, [&](auto *p) {
                    if(previous)
                    {
                        BOOST_TEST(p->name >= *previous);
                        previous = p->name;
                    }
                    else
                    {
                        previous = p->name;
                    }
                    ++count;
                });
                BOOST_TEST(count == 4);
            });
        },
        [&](auto db, auto &sync)
        {
            for(auto name : {"foo", "bar", "var", "boo"})
            {
                db.transaction([&](Lock<Json> lock){
                    lock.make(Json{
                        .name = "foo"s,
                        .value = J
                    });
                });
            }
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
