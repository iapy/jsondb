#include <jsondb/server.hpp>
#include <weblib/server.hpp>

#include <msgbus/msgbus.hpp>
#include <msgbus/service.hpp>

#include <cg/graph.hpp>
#include <cg/service.hpp>

#include "lib/model.hpp"

int main()
{
    using namespace objects;
    using Database = jsondb::Server<TEST_OBJECTS>;

    using Graph = cg::Graph
    <
        cg::Connect<Database, msgbus::Msgbus>,
        cg::Connect<weblib::Server, msgbus::Msgbus>,
        cg::Connect<weblib::Server, Database, weblib::tag::Handler>
    >;

    return msgbus::Service<Graph>(
        cg::Args<weblib::Server>("127.0.0.1", SERVER_PORT, 2, false),
        cg::Args<Database>(DATA_DIR, "db")
    );
}
