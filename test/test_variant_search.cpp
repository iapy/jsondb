#include "lib/framework.hpp"

JSONDB_TEST_SUITE(VariantSearch, A, B, Variant)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    run_test(
        [&](auto db)
        {
            db.transaction([&](Lock<Variant const> lock){
                std::size_t count = 0;
                lock.each(Search<&Variant::object>{}, [&count](Variant const *v){
                    ++count;
                });
                BOOST_TEST(22 == count);

                count = lock.count(Search<&Variant::object>{});
                BOOST_TEST(22 == count);

                count = lock.count(Search<&Variant::object>{}, [](Variant const *v){
                    return std::holds_alternative<int>(v->primitive);
                });
                BOOST_TEST(11 == count);

                count = 0;
                lock.each(Search<&Variant::object>{.eq = nullptr}, [&count](Variant const *v){
                    ++count;
                });
                BOOST_TEST(2 == count);

                count = lock.count(Search<&Variant::object>{.eq = nullptr});
                BOOST_TEST(2 == count);

                count = 0;
                lock.each([&](A const *a) {
                    lock.each(Search<&Variant::object>{.eq = a}, [&](Variant const *v) {
                        ++count;
                        BOOST_TEST(std::holds_alternative<int>(v->primitive));
                        BOOST_TEST(std::get<Pointer<A>>(v->object) == a);
                    });
                    BOOST_TEST(10 == lock.count(Search<&Variant::object>{.eq = a}));
                    BOOST_TEST( 5 == lock.count(Search<&Variant::object>{.eq = a}, [](Variant const *v){
                        return std::get<int>(v->primitive) <= 5;
                    }));
                });
                BOOST_TEST(10 == count);

                count = 0;
                lock.each([&](B const *b) {
                    lock.each(Search<&Variant::object>{.eq = b}, [&](Variant const *v) {
                        ++count;
                        BOOST_TEST(std::holds_alternative<std::string>(v->primitive));
                        BOOST_TEST(std::get<Pointer<B>>(v->object) == b);
                    });
                    BOOST_TEST(10 == lock.count(Search<&Variant::object>{.eq = b}));
                    BOOST_TEST( 5 == lock.count(Search<&Variant::object>{.eq = b}, [](Variant const *v){
                        return std::get<std::string>(v->primitive).size() > 5;
                    }));
                });
                BOOST_TEST(10 == count);
            });
        },
        [&](auto db, auto &sync)
        {
            db.transaction([&](Lock<A, B, Variant> lock){
                auto a = lock.make(A{
                    .name = "a"
                });

                for(int i = 1; i <= 10; ++i)
                {
                    lock.make(Variant{
                        .object = a,
                        .primitive = i
                    });
                }

                lock.make(Variant{
                    .object = (A*)nullptr,
                    .primitive = 0
                });
            });
            return 0;
        },
        [&](auto db, auto &sync)
        {
            db.transaction([&](Lock<A, B, Variant> lock){
                auto b = lock.make(B{
                    .value = 42
                });

                for(int i = 1; i <= 10; ++i)
                {
                    lock.make(Variant{
                        .object = b,
                        .primitive = "abcdefghij" + (i - 1)
                    });
                }

                lock.make(Variant{
                    .object = (B*)nullptr,
                    .primitive = "xxx"
                });
            });
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
