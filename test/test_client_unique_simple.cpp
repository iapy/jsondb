#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(ClientUniqueSimple, jsondb::tester::JsondbClient)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;
    run_test([](auto sv, auto db){
        db.transaction(sv, [&](Session<IntegerValue> session) {
            BOOST_TEST(session.unique("foo"s, [](IntegerValue *value) {
                value->whatever = 100;
            }));
        });

        db.transaction(sv, [&](Session<IntegerValue> session) {
            BOOST_TEST(session.unique("foo"s, [](IntegerValue *value) {
                BOOST_TEST(100 == value->whatever);
            }));
            BOOST_TEST(session.unique("foo"s, [](IntegerValue *value) {
                value->whatever = 200;
            }));
            BOOST_TEST(session.unique("bar"s, [](IntegerValue *value) {
                value->whatever = 100;
            }));
        });

        db.transaction(sv, [&](Session<IntegerValue> session) {
            BOOST_TEST(session.unique("foo"s, [](IntegerValue *value) {
                BOOST_TEST(200 == value->whatever);
            }));
            BOOST_TEST(session.unique("bar"s, [](IntegerValue *value) {
                BOOST_TEST(100 == value->whatever);
            }));
        });
    });
}

BOOST_AUTO_TEST_SUITE_END()
