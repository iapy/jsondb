#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(ClientChange, jsondb::tester::JsondbClient)

BOOST_AUTO_TEST_CASE(Test)
{
    run_test([](auto sv, auto db){
        boost::uuids::uuid a;
        boost::uuids::uuid b;

        db.transaction(sv, [&](Session<Book> session) {
            auto author = session.make(Author{
                .name = "Umberto Eco"
            });

            auto au = session.uuidof(author);
            BOOST_TEST(au);

            a = *au;

            auto book = session.make(Book{
                .name = "Baudolino",
                .author = author
            });

            auto bu = session.uuidof(book);
            BOOST_TEST(bu);

            b = *bu;
        });

        db.transaction(sv, [&](Session<Author> session) {
            auto country = session.make(Country{
                .name = "Italy"
            });

            session.get(a, [&](Author *author) {
                author->country = country;
            });
        });

        db.transaction(sv, [&](Session<Book> session) {
            session.get(b, [&](Book *book) {
                book->name = "The Name of the Rose";
            });
        });

        db.transaction(sv, [&](Session<Book> session) {
            session.get(b, [&](Book const *book) {
                BOOST_TEST(book->name == "The Name of the Rose");
                BOOST_TEST(book->author->name == "Umberto Eco");
                BOOST_TEST(book->author->country->name == "Italy");
            });
        });
    });
}

BOOST_AUTO_TEST_SUITE_END()
