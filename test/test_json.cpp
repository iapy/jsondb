#include "lib/framework.hpp"

JSONDB_TEST_SUITE(Json, objects::Json)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    auto J = R"({
        "a": [1, 2, 3],
        "b": "foo"
    })"_json;

    run_test(
        [&](auto db)
        {
            db.transaction([&](Lock<objects::Json const> lock){
                bool found = false;
                lock.each(Search<&objects::Json::name>{.eq = "foo"s}, [&](auto *p) {
                    BOOST_TEST(p->value == J);
                    found = true;
                });
                BOOST_TEST(found);
            });
        },
        [&](auto db, auto &sync)
        {
            db.transaction([&](Lock<objects::Json> lock){
                lock.make(objects::Json{
                    .name = "foo"s,
                    .value = J
                });
            });
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
