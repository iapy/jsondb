#include "lib/framework.hpp"

JSONDB_TEST_SUITE(Ephemeral, objects::Ephemeral)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    bool first{true};
    run_test(
        [&](auto db)
        {
            db.transaction([&](Lock<objects::Ephemeral const> lock){
                BOOST_TEST(lock.count(Search<&objects::Ephemeral::name>{.eq = "foo"}) == static_cast<int>(first));
                BOOST_TEST(lock.count(Search<&objects::Ephemeral::name>{.eq = "bar"}) == static_cast<int>(first));
                BOOST_TEST(lock.count(Search<&objects::Ephemeral::name>{.eq = "baz"}) == 0);
            });
            first = false;
        },
        [&](auto db, auto &sync)
        {
            for(auto name : {"foo", "bar"})
            {
                db.transaction([&](Lock<objects::Ephemeral> lock){
                    lock.make(objects::Ephemeral{
                        .name = name
                    });
                });
            }
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
