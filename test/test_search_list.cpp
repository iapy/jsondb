#include "lib/framework.hpp"

JSONDB_TEST_SUITE(SearchList, Lists)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;
    run_test(
        [](auto db)
        {
            int count = 0;
            db.transaction([&](Lock<Lists const> lock) {
                lock.each(Search<&Lists::ints>{.contains = 4}, [&](auto *lists) {
                    ++count;
                    BOOST_TEST(3 == lists->ints.size());
                });
            });
            BOOST_TEST(2 == count);

            db.transaction([&](Lock<Lists const> lock) {
                count = lock.count(Search<&Lists::ints>{.contains = 4});
            });
            BOOST_TEST(2 == count);

            db.transaction([&](Lock<Lists const> lock) {
                count = lock.count(Search<&Lists::ints>{.contains = 4}, [](auto *l){
                    return l->ints[0] == 2;
                });
            });
            BOOST_TEST(1 == count);

            count = 0;
            db.transaction([&](Lock<Lists const> lock) {
                lock.each(Search<&Lists::ints>{.contains = 1}, [&](auto *lists) {
                    ++count;
                });
            });
            BOOST_TEST(0 == count);

            db.transaction([&](Lock<Lists const> lock) {
                count = lock.count(Search<&Lists::ints>{.contains = 1});
            });
            BOOST_TEST(0 == count);

            count = 0;
            db.transaction([&](Lock<Lists const> lock) {
                lock.each(Search<&Lists::strings>{.contains = "a"s}, [&](auto *lists) {
                    ++count;
                });
            });
            BOOST_TEST(2 == count);

            db.transaction([&](Lock<Lists const> lock) {
                count = lock.count(Search<&Lists::strings>{.contains = "a"s});
            });
            BOOST_TEST(2 == count);

            count = 0;
            db.transaction([&](Lock<Lists const> lock) {
                lock.each(Search<&Lists::strings>{.contains = "b"s}, [&](auto *lists) {
                    ++count;
                });
            });
            BOOST_TEST(1 == count);

            db.transaction([&](Lock<Lists const> lock) {
                count = lock.count(Search<&Lists::strings>{.contains = "b"s});
            });
            BOOST_TEST(1 == count);

        },
        [](auto db, auto &sync)
        {
            db.transaction([](Lock<Lists> lock){
                lock.make(Lists{
                    .ints = {1, 2, 3},
                    .strings = {"foo"s, "bar"s, "baz"s}
                });
                lock.make(Lists{
                    .ints = {4, 5, 6},
                    .strings = {"a"s, "b"s, "c"s}
                });
            });

            int count = 0;
            db.transaction([&](Lock<Lists> lock) {
                lock.each(Search<&Lists::ints>{.contains = 1},[&](auto *lists) {
                    lists->ints.push_back(4);
                    lists->ints.erase(lists->ints.begin());
                    ++count;
                });
                lock.each(Search<&Lists::strings>{.contains = "bar"s},[](auto *lists){
                    lists->strings.push_back("a"s);
                });
            });
            BOOST_TEST(1 == count);
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
