#include "lib/framework.hpp"

JSONDB_TEST_SUITE(Remove, Author, Country)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    run_test(
        [](auto db)
        {
            db.transaction([](Lock<Author const> lock){
                lock.each([](Author const *author){
                    BOOST_TEST(author->country == nullptr);
                });
            });
        },
        [](auto db, auto &sync)
        {
            db.transaction([](Lock<Author, Country> lock){
                lock.make(Author{
                    .name = "Rudyard Kipling"s,
                    .country = lock.make(Country{
                        .name = "UK"s
                    })
                });
            });
            sync.fire(0);
            return 0;
        },
        [](auto db, auto &sync)
        {
            sync.wait(0);
            db.transaction([](Lock<Country> lock){
                lock.each([&lock](Country *country){
                    lock.remove(country);
                });
            });
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
