#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(ClientProgress, jsondb::tester::JsondbClient)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    run_test([](auto sv, auto db){
        boost::uuids::uuid a;
        boost::uuids::uuid b;

        db.transaction(sv, [&](Session<Progress> session) {
            session.make(Progress{
                .name = "foo"s,
                .total = 10,
                .count = 0
            });

            session.make(Progress{
                .name = "bar"s,
                .total = 5,
                .count = 0
            });
        });

        for(std::size_t i = 0; i < 5; ++i)
        {
            db.transaction(sv, [&](Session<Progress> session) {
                session.unique("foo"s, [&](Progress *progress){
                    BOOST_TEST(progress->total == 10);
                    BOOST_TEST(progress->count == i);
                    progress->count += 1;
                });
                session.unique("bar"s, [&](Progress *progress){
                    BOOST_TEST(progress->count == i);
                    BOOST_TEST(progress->total == 5);
                    BOOST_TEST(progress->count == i);
                    if(5 == ++progress->count)
                    {
                        session.remove(progress);
                    }
                });
            });
        }

        for(std::size_t i = 5; i < 10; ++i)
        {
            db.transaction(sv, [&](Session<Progress> session) {
                session.each([&](Progress *progress) {
                    BOOST_TEST(progress->name == "foo"s);
                    BOOST_TEST(progress->count == i);
                    BOOST_TEST(progress->total == 10);
                    if(10 == ++progress->count)
                    {
                        session.remove(progress);
                    }
                });
            });
        }
    });
}

BOOST_AUTO_TEST_SUITE_END()
