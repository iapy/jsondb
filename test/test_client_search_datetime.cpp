#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(ClientSearchDatetime, jsondb::tester::JsondbClient)

BOOST_AUTO_TEST_CASE(Test)
{
    jsondb::DateTime dt;
    jsonio::from("\"2022-01-01T00:00:00\""_json, dt);

    boost::uuids::uuid dt_1h;
    boost::uuids::uuid dt_2h;
    boost::uuids::uuid dt_3h;

    run_test([&](auto sv, auto db){
        db.transaction(sv, [&](Session<Times> session) {
            session.make(Times{
                .time = dt - std::chrono::hours(1),
                .times = {
                    dt - std::chrono::hours(2),
                    dt - std::chrono::hours(3)
                }
            }, dt_1h);
            session.make(Times{
                .time = dt - std::chrono::hours(2),
                .times = {
                    dt - std::chrono::hours(3)
                }
            }, dt_2h);
            session.make(Times{
                .time = dt - std::chrono::hours(3)
            }, dt_3h);
        });

        db.transaction(sv, [&](Session<Times> session) {
            session.each(Search<&Times::time>{.eq = (dt - std::chrono::hours(1))}, [&](Times const *time) {
                BOOST_TEST(session.uuidof(time) == dt_1h);
            });

            session.each(Search<&Times::time>{.eq = (dt - std::chrono::hours(2))}, [&](Times const *time) {
                BOOST_TEST(session.uuidof(time) == dt_2h);
            });

            session.each(Search<&Times::time>{.eq = (dt - std::chrono::hours(3))}, [&](Times const *time) {
                BOOST_TEST(session.uuidof(time) == dt_3h);
            });

            std::size_t count = 0;
            session.each(Search<&Times::time>{.gt = (dt - std::chrono::hours(3))}, [&](Times const *time) {
                BOOST_TEST((session.uuidof(time) == dt_1h || session.uuidof(time) == dt_2h));
                ++count;
            });
            BOOST_TEST(2 == count);

            count = 0;
            session.each(Search<&Times::time>{.lt = (dt - std::chrono::hours(1))}, [&](Times const *time) {
                BOOST_TEST((session.uuidof(time) == dt_2h || session.uuidof(time) == dt_3h));
                ++count;
            });
            BOOST_TEST(2 == count);

            count = 0;
            session.each(Search<&Times::time>{.gt = (dt - std::chrono::hours(3)), .lt = (dt - std::chrono::hours(1))}, [&](auto *time) {
                BOOST_TEST(session.uuidof(time) == dt_2h);
                ++count;
            });
            BOOST_TEST(1 == count);

            count = 0;
            session.each(Search<&Times::times>{.contains = (dt - std::chrono::hours(1))}, [&](Times const *time) {
                ++count;
            });
            BOOST_TEST(0 == count);

            count = 0;
            session.each(Search<&Times::times>{.contains = (dt - std::chrono::hours(2))}, [&](Times const *time) {
                BOOST_TEST(session.uuidof(time) == dt_1h);
                ++count;
            });
            BOOST_TEST(1 == count);

            count = 0;
            session.each(Search<&Times::times>{.contains = (dt - std::chrono::hours(3))}, [&](Times const *time) {
                BOOST_TEST((session.uuidof(time) == dt_1h || session.uuidof(time) == dt_2h));
                ++count;
            });
            BOOST_TEST(2 == count);
        });
    });
}

BOOST_AUTO_TEST_SUITE_END()
