#include "lib/framework.hpp"

JSONDB_TEST_SUITE(UniqueIndex, IntegerValue)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;
    run_test(
        [](auto db)
        {
            db.transaction([&](Lock<IntegerValue const> lock){
                std::size_t count{0};

                count = lock.count(Search<&IntegerValue::whatever>{.eq = 1});
                BOOST_TEST(count == 1);

                count = lock.count(Search<&IntegerValue::whatever>{.eq = 2});
                BOOST_TEST(count == 2);
            });
        },
        [](auto db, auto &sync)
        {
            std::size_t count{0};

            db.transaction([&](Lock<IntegerValue> lock){
                lock.unique("foo", [&](IntegerValue *value, bool created){
                    value->whatever = 1;
                    BOOST_TEST(created);
                });

                count = lock.count(Search<&IntegerValue::whatever>{.eq = 1});
                BOOST_TEST(count == 1);
            });

            db.transaction([&](Lock<IntegerValue> lock){
                lock.unique("foo", [&](IntegerValue *value, bool created){
                    ++value->whatever;
                    BOOST_TEST(!created);
                });

                count = lock.count(Search<&IntegerValue::whatever>{.eq = 1});
                BOOST_TEST(count == 0);

                count = lock.count(Search<&IntegerValue::whatever>{.eq = 2});
                BOOST_TEST(count == 1);
            });

            db.transaction([&](Lock<IntegerValue> lock){
                lock.unique("bar", [&](IntegerValue *value, bool created){
                    value->whatever = 2;
                    BOOST_TEST(created);
                });

                count = lock.count(Search<&IntegerValue::whatever>{.eq = 1});
                BOOST_TEST(count == 0);

                count = lock.count(Search<&IntegerValue::whatever>{.eq = 2});
                BOOST_TEST(count == 2);
            });

            db.transaction([&](Lock<IntegerValue> lock){
                lock.unique("baz", [&](IntegerValue *value, bool created){
                    value->whatever = 1;
                    BOOST_TEST(created);
                });
            });
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
