#include "lib/framework.hpp"

JSONDB_TEST_SUITE(SearchId, Author, Book, Country)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    boost::uuids::uuid a, b;

    run_test(
        [&](auto db)
        {
            db.transaction([&](Lock<Book const> lock) {
                bool found;
                found = lock.get(b, [](Book const *book){
                    BOOST_TEST(book->name == "Metamorphosis"s);
                    BOOST_TEST(book->author->name == "Franz Kafka"s);
                });
                BOOST_TEST(found);

                found = lock.get(a, [](Author const *author){
                    BOOST_TEST(author->name == "Franz Kafka"s);
                });
                BOOST_TEST(found);

                found = lock.get(a, [](Book const *book){});
                BOOST_TEST(!found);

                found = lock.get(b, [](Author const *book){});
                BOOST_TEST(!found);
            });
        },
        [&](auto db, auto &sync) mutable
        {
            db.transaction([&](Lock<Author, Book> lock) {
                auto Kafka = lock.make(Author{
                    .name = "Franz Kafka"s
                }, a);
                lock.make(Book{
                    .name = "metamorphosis"s,
                    .author = Kafka
                }, b);
            });
            sync.fire(0);
            return 0;
        },
        [&](auto db, auto &sync) mutable
        {
            sync.wait(0);
            db.transaction([&](Lock<Book> lock) {
                bool found = lock.get(b, [](Book *book){
                    book->name = "Metamorphosis"s;
                });
                BOOST_TEST(found);
            });
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
