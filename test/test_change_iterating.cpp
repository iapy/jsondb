#include "lib/framework.hpp"

JSONDB_TEST_SUITE(ChangeIterating, Country)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    run_test(
        [](auto db)
        {
            db.transaction([](Lock<Country const> lock){
                lock.each([](Country const *country) {
                    BOOST_TEST(country->name.size() == 2);
                });
            });
        },
        [](auto db, auto &sync)
        {
            db.transaction([](Lock<Country> lock){
                lock.make(Country{
                    .name = "A"s
                });
                lock.make(Country{
                    .name = "B"s
                });
            });
            sync.fire(0);
            return 0;
        },
        [](auto db, auto &sync)
        {
            sync.wait(0);
            db.transaction([](Lock<Country> lock){
                lock.each([](Country *country) {
                    country->name = country->name + "_"s;
                });
            });
            db.transaction([](Lock<Country const> lock){
                lock.each([](Country const *country) {
                    BOOST_TEST(country->name.size() == 2);
                });
            });
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
