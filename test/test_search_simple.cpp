#include "lib/framework.hpp"

JSONDB_TEST_SUITE(SearchSimple, Author, Book, Country)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;
    run_test(
        [](auto db)
        {
            db.transaction([](Lock<Book const> lock){
                std::set<std::string> strings;
                lock.each(Search<&Book::name>{.eq = "Metamorphosis"s}, [&](Book const *book) {
                    strings.insert(book->author->name);
                });
                BOOST_TEST(std::set<std::string>{"Franz Kafka"s} == strings);
                
                strings.clear();
                BOOST_TEST(strings.empty());

                lock.each(Search<&Book::name>{.eq = "The Trial"s}, [&](auto *book) {
                    strings.insert(book->author->name);
                });
                BOOST_TEST(std::set<std::string>{"Franz Kafka"s} == strings);
                strings.clear();

                lock.each(Search<&Author::name>{.eq = "Franz Kafka"s}, [&](auto *a) {
                    strings.insert(a->name);
                    lock.each(Search<&Book::author>{.eq = a}, [&](Book const *b) {
                        strings.insert(b->name);
                    });
                });
                BOOST_TEST((strings == std::set<std::string>{"Franz Kafka"s, "Metamorphosis"s, "The Trial"s}));
            });
        },
        [](auto db, auto &sync)
        {
            db.transaction([](Lock<Author, Book> lock){
                auto Kafka = lock.make(Author{
                    .name = "Franz Kafka"s
                });
                lock.make(Book{
                    .name = "metamorphosis"s,
                    .author = Kafka
                });
                lock.make(Book{
                    .name = "The Trial"s,
                    .author = Kafka 
                });
            });
            sync.fire(0);
            return 0;
        },
        [](auto db, auto &sync)
        {
            db.transaction([](Lock<Author, Book> lock){
                lock.make(Book{
                    .name = "metamorphosis"s,
                    .author = lock.make(Author{
                        .name = "William Jackson"s
                    })
                });
            });
            sync.fire(1);
            return 0;
        },
        [](auto db, auto &sync)
        {
            sync.wait(0);
            sync.wait(1);

            db.transaction([](Lock<Book> lock){
                std::set<std::string> authors;
                lock.each(Search<&Book::name>{.eq = "metamorphosis"s}, [&](Book *book) {
                    authors.insert(book->author->name);
                    if(book->author->name == "William Jackson"s)
                        lock.remove(book);
                    else
                        book->name = "Metamorphosis"s;
                });
                BOOST_TEST((authors == std::set<std::string>{"William Jackson"s, "Franz Kafka"s}));
            });

            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
