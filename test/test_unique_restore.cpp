#include "lib/framework.hpp"

JSONDB_TEST_SUITE(UniqueRestore, Config)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;
    run_test(
        [](auto db)
        {
            BOOST_TEST(1 == Config::counter());
            db.transaction([&](Lock<Config> lock) {
                lock.unique("foo", [&](Config *config, bool created) {
                    config->value["a"] = 2;
                    BOOST_TEST(!created);
                    BOOST_TEST(config->name == "foo");
                });
            });
        },
        [](auto db, auto &sync)
        {
            db.transaction([&](Lock<Config> lock) {
                lock.unique("foo", [&](Config *config, bool created) {
                    config->value["a"] = 1;
                    BOOST_TEST(created);
                });
            });
            db.transaction([&](Lock<Config> lock) {
                lock.unique("foo", [&](Config *config, bool created) {
                    config->value["a"] = 2;
                    config->name = "bar";
                    BOOST_TEST(!created);
                });
            });
            sync.fire(0);
            return 0;
        },
        [](auto db, auto &sync)
        {
            sync.wait(0);
            db.transaction([&](Lock<Config> lock) {
                lock.unique("foo", [&](Config *config, bool created) {
                    BOOST_TEST(!created);
                });
            });
            db.transaction([&](Lock<Config> lock) {
                lock.each([&](Config *config) {
                    config->name = "bar";
                });
            });
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
