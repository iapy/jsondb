#include "lib/framework.hpp"

JSONDB_TEST_SUITE(SearchComplex, Node)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    run_test(
        [](auto db)
        {
            db.transaction([](Lock<Node const> lock){
                int sum = 0;
                std::size_t count = 0;

                lock.each(Search<&Node::value>{.gt = 45, .lt = 5}, [&](auto *node) {
                    sum += node->value;
                });
                BOOST_TEST(sum == 0);

                count = lock.count(Search<&Node::value>{.gt = 45, .lt = 5});
                BOOST_TEST(count == 0);

                lock.each(Search<&Node::value>{.lt = 5}, [&](auto *node) {
                    sum += node->value;
                });
                BOOST_TEST(sum == 10);

                count = lock.count(Search<&Node::value>{.lt = 5});
                BOOST_TEST(count == 4);

                count = lock.count(Search<&Node::value>{.lt = 5}, [](auto *node) {
                    return node->value != 2;
                });
                BOOST_TEST(count == 3);

                sum = 0;
                lock.each(Search<&Node::value>{.eq = 10, .lt = 45}, [&](auto *node) {
                    sum += node->value;
                });
                BOOST_TEST(sum == 10);

                count = lock.count(Search<&Node::value>{.eq = 10, .lt = 45});
                BOOST_TEST(count == 1);

                count = lock.count(Search<&Node::value>{.eq = 10, .lt = 45}, [](auto *node) {
                    return node->value != 10;
                });
                BOOST_TEST(count == 0);

                sum = 0;
                lock.each(Search<&Node::value>{.gt = 45}, [&](auto *node) {
                    sum += node->value;
                });
                BOOST_TEST(sum == 240);

                count = lock.count(Search<&Node::value>{.gt = 45});
                BOOST_TEST(count == 5);

                count = lock.count(Search<&Node::value>{.gt = 45}, [](auto *node) {
                    return node->value != 47;
                });
                BOOST_TEST(count == 4);

                sum = 0;
                lock.each(Search<&Node::value>{.gt = 5, .lt = 45}, [&](auto *node) {
                    sum += node->value;
                    BOOST_TEST(node->next);
                    BOOST_TEST(node->next->value == node->value + 1);
                });
                BOOST_TEST(sum == 975);

                count = lock.count(Search<&Node::value>{.gt = 5, .lt = 45});
                BOOST_TEST(count == 39);

                count = lock.count(Search<&Node::value>{.gt = 5, .lt = 45}, [](auto *node) {
                    return node->value != 33 && node->value != 20;
                });
                BOOST_TEST(count == 37);

                sum = 0;
                lock.each(Search<&Node::value>{}, [&](auto *node) {
                    sum += node->value;
                });
                BOOST_TEST(sum == 1275);

                count = lock.count(Search<&Node::value>{});
                BOOST_TEST(count == 50);

                count = lock.count(Search<&Node::value>{}, [](auto *node) {
                    return node->value % 2 == 0;
                });
                BOOST_TEST(count == 25);

                sum = 0;
                lock.each(Search<&Node::next>{}, [&](auto *node) {
                    sum += node->value;
                });
                BOOST_TEST(sum == 1275);

                count = lock.count(Search<&Node::next>{});
                BOOST_TEST(count == 50);

                count = lock.count(Search<&Node::next>{}, [](auto *node) {
                    return node->value % 2 == 0;
                });
                BOOST_TEST(count == 25);

                sum = 0;
                lock.each(Search<&Node::next>{.eq = nullptr}, [&](auto *node) {
                    sum += node->value;
                });
                BOOST_TEST(sum == 50);

                count = lock.count(Search<&Node::next>{.eq = nullptr});
                BOOST_TEST(count == 1);

                count = lock.count(Search<&Node::next>{}, [](auto *node) {
                    return false;
                });
                BOOST_TEST(count == 0);
            });
        },
        [](auto db, auto &sync)
        {
            db.transaction([](Lock<Node> lock){
                Opaque<Node> tail{nullptr};
                for(int i = 50; i > 25; --i)
                {
                    tail = lock.make(Node{
                        .value = i,
                        .next = tail
                    });
                }
            });
            sync.wait(0);
            db.transaction([&](Lock<Node> lock){
                lock.each(Search<&Node::value>{.eq = 25}, [&](auto *n25){
                    lock.each(Search<&Node::value>{.eq = 26}, [&](auto *n26){
                        BOOST_TEST(n25->next == nullptr);
                        n25->next = n26;
                    });
                });
            });
            return 0;
        },
        [](auto db, auto &sync)
        {
            db.transaction([](Lock<Node> lock){
                Opaque<Node> tail{nullptr};
                for(int i = 25; i > 0; --i)
                {
                    tail = lock.make(Node{
                        .value = i,
                        .next = tail
                    });
                }
            });
            sync.fire(0);
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
