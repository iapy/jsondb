#include "lib/framework.hpp"

JSONDB_TEST_SUITE(SearchJson, Json)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;
    run_test(
        [](auto db)
        {
            return 0;
        },
        [](auto db, auto &sync)
        {
            db.transaction([](Lock<Json> lock){
                lock.make(Json{
                    .name = "null:0"s,
                    .value = nullptr 
                });
                lock.make(Json{
                    .name = "null:1"s,
                    .value = nullptr 
                });
                lock.make(Json{
                    .name = "bool:0"s,
                    .value = true 
                });
                lock.make(Json{
                    .name = "bool:1"s,
                    .value = true 
                });
                lock.make(Json{
                    .name = "bool:2"s,
                    .value = false 
                });
                lock.make(Json{
                    .name = "int:0"s,
                    .value = 1 
                });
                lock.make(Json{
                    .name = "int:2"s,
                    .value = 1 
                });
                lock.make(Json{
                    .name = "int:42"s,
                    .value = 42 
                });
                lock.make(Json{
                    .name = "str:foo:0"s,
                    .value = "foo"
                });
                lock.make(Json{
                    .name = "str:foo:1"s,
                    .value = "foo"
                });
                lock.make(Json{
                    .name = "str:bar:0"s,
                    .value = "bar"
                });
                lock.make(Json{
                    .name = "obj:name:bar:0"s,
                    .value = R"({
                        "name": "bar",
                        "value": 0
                    })"_json
                });
                lock.make(Json{
                    .name = "obj:name:bar:1"s,
                    .value = R"({
                        "name": "bar",
                        "value": 1
                    })"_json
                });
                lock.make(Json{
                    .name = "obj:name:foo:0"s,
                    .value = R"({
                        "name": "foo",
                        "value": 0
                    })"_json
                });
            });
            sync.fire(0);
            return 0;
        },
        [](auto db, auto &sync)
        {
            sync.wait(0);
            db.transaction([&](Lock<Json> lock) {
                std::set<std::string> nulls;
                lock.each(Search<&Json::value>{.query = nullptr}, [&nulls](Json const *value){
                    nulls.insert(value->name);
                });
                BOOST_TEST((nulls == std::set<std::string>{"null:0", "null:1"}));
                BOOST_TEST(nulls.size() == lock.count(Search<&Json::value>{.query = nullptr}));

                std::set<std::string> trues;
                lock.each(Search<&Json::value>{.query = true}, [&trues](Json const *value){
                    trues.insert(value->name);
                });
                BOOST_TEST((trues == std::set<std::string>{"bool:0", "bool:1"}));
                BOOST_TEST(trues.size() == lock.count(Search<&Json::value>{.query = true}));

                std::set<std::string> falses;
                lock.each(Search<&Json::value>{.query = false}, [&falses](Json const *value){
                    falses.insert(value->name);
                });
                BOOST_TEST(falses == std::set<std::string>{"bool:2"});
                BOOST_TEST(falses.size() == lock.count(Search<&Json::value>{.query = false}));

                std::set<std::string> bools;
                lock.each(Search<&Json::value>{.query = R"({".ge": false})"_json}, [&bools](Json *value){
                    bools.insert(value->name);
                });
                BOOST_TEST((bools == std::set<std::string>{"bool:0", "bool:1", "bool:2"}));

                bools.clear();
                lock.each(Search<&Json::value>{.query = R"({".le": true})"_json}, [&bools](Json *value){
                    bools.insert(value->name);
                });
                BOOST_TEST((bools == std::set<std::string>{"bool:0", "bool:1", "bool:2"}));

                bools.clear();
                lock.each(Search<&Json::value>{.query = R"({".ge": false, ".le": true})"_json}, [&bools](Json *value){
                    bools.insert(value->name);
                });
                BOOST_TEST((bools == std::set<std::string>{"bool:0", "bool:1", "bool:2"}));

                bools.clear();
                lock.each(Search<&Json::value>{.query = R"({".ge": true})"_json}, [&bools](Json *value){
                    bools.insert(value->name);
                });
                BOOST_TEST((bools == std::set<std::string>{"bool:0", "bool:1"}));

                bools.clear();
                lock.each(Search<&Json::value>{.query = R"({".le": false})"_json}, [&bools](Json *value){
                    bools.insert(value->name);
                });
                BOOST_TEST(bools == std::set<std::string>{"bool:2"});

                bools.clear();
                lock.each(Search<&Json::value>{.query = R"({".le": true})"_json}, [&bools](Json *value){
                    bools.insert(value->name);
                });
                BOOST_TEST((bools == std::set<std::string>{"bool:0", "bool:1", "bool:2"}));

                bools.clear();
                lock.each(Search<&Json::value>{.query = R"({".le": true, ".lt": true})"_json}, [&bools](Json *value){
                    bools.insert(value->name);
                });
                BOOST_TEST(bools == std::set<std::string>{"bool:2"});

                bools.clear();
                lock.each(Search<&Json::value>{.query = R"({".ge": false, ".gt": false})"_json}, [&bools](Json *value){
                    bools.insert(value->name);
                });
                BOOST_TEST((bools == std::set<std::string>{"bool:0", "bool:1"}));

                lock.each([&](Json *value){
                    if(trues.count(value->name)) value->value = false;
                    else if(falses.count(value->name)) value->value = true;
                });

                lock.each(Search<&Json::value>{.query = true}, [&trues](Json const *value){
                    trues.insert(value->name);
                });
                BOOST_TEST((trues == std::set<std::string>{"bool:0", "bool:1", "bool:2"}));
                BOOST_TEST(trues.size() == lock.count(Search<&Json::value>{.query = true}));

                lock.each(Search<&Json::value>{.query = false}, [&falses](Json const *value){
                    falses.insert(value->name);
                });
                BOOST_TEST((falses == std::set<std::string>{"bool:0", "bool:1", "bool:2"}));

                std::set<std::string> foos;
                lock.each(Search<&Json::value>{.query = "foo"}, [&foos](Json *value){
                    foos.insert(value->name);
                    value->value = true;
                });
                BOOST_TEST((foos == std::set<std::string>{"str:foo:0", "str:foo:1"}));

                std::set<std::string> bars;
                lock.each(Search<&Json::value>{.query = "bar"}, [&bars](Json *value){
                    bars.insert(value->name);
                    value->value = false;
                });
                BOOST_TEST(bars == std::set<std::string>{"str:bar:0"});

                std::set<std::string> namebars;
                lock.each(Search<&Json::value>{.query = R"({"name": "bar"})"_json}, [&namebars](Json *value){
                    namebars.insert(value->name);
                });
                BOOST_TEST((namebars == std::set<std::string>{"obj:name:bar:0", "obj:name:bar:1"}));

                std::set<std::string> namefoos;
                lock.each(Search<&Json::value>{.query = R"({"name": "foo"})"_json}, [&namefoos](Json *value){
                    namefoos.insert(value->name);
                });
                BOOST_TEST(namefoos == std::set<std::string>{"obj:name:foo:0"});

                std::set<std::string> ints;
                lock.each(Search<&Json::value>{.query = 1}, [&ints](Json *value){
                    ints.insert(value->name);
                });
                BOOST_TEST((ints == std::set<std::string>{"int:0", "int:2"}));

                std::set<std::string> greaters;
                lock.each(Search<&Json::value>{.query = R"({".gt": 1})"_json}, [&greaters](Json *value){
                    greaters.insert(value->name);
                });
                BOOST_TEST(greaters == std::set<std::string>{"int:42"});

                lock.each(Search<&Json::value>{.query = R"({".ge": 1})"_json}, [&greaters](Json *value){
                    greaters.insert(value->name);
                });
                BOOST_TEST((greaters == std::set<std::string>{"int:0", "int:2", "int:42"}));

                std::set<std::string> strings;
                lock.each(Search<&Json::value>{.query = R"({".ge": ""})"_json}, [&strings](Json *value){
                    strings.insert(value->name);
                });
                BOOST_TEST((strings == std::set<std::string>{"str:foo:0", "str:foo:1", "str:bar:0"}));
            });
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
