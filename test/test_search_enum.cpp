#include "lib/framework.hpp"

JSONDB_TEST_SUITE(SearchEnum, Enum)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    jsondb::DateTime dt;
    jsonio::from("\"2022-01-01T00:00:00\""_json, dt);

    boost::uuids::uuid dt_1h;
    boost::uuids::uuid dt_2h;
    boost::uuids::uuid dt_3h;

    run_test(
        [](auto db)
        {
            db.transaction([](Lock<Enum const> lock){
                std::size_t count{0};
                lock.each(Search<&Enum::option>{.eq = Enum::Option::A}, [&](Enum const *e) {
                    ++count;
                });
                BOOST_TEST(2 == count);
                BOOST_TEST(2 == lock.count(Search<&Enum::option>{.eq = Enum::Option::A}));
                BOOST_TEST(3 == lock.count(Search<&Enum::option>{.eq = Enum::Option::B}));
            });
        },
        [](auto db, auto &sync)
        {
            db.transaction([](Lock<Enum> lock){
                lock.make(Enum{
                    .option = Enum::Option::A,
                    .name = "a1"
                });
                lock.make(Enum{
                    .option = Enum::Option::A,
                    .name = "a2"
                });
                lock.make(Enum{
                    .option = Enum::Option::A,
                    .name = "a3"
                });
                lock.make(Enum{
                    .option = Enum::Option::B,
                    .name = "b1"
                });
                lock.make(Enum{
                    .option = Enum::Option::B,
                    .name = "b2"
                });
            });
            sync.fire(0);
            return 0;
        },
        [](auto db, auto &sync)
        {
            sync.wait(0);
            db.transaction([&](Lock<Enum> lock){
                std::size_t count{0};
                lock.each(Search<&Enum::option>{.eq = Enum::Option::A}, [&](Enum *e) {
                    if(e->name == "a1")
                    {
                        e->option = Enum::Option::B;
                    }
                    ++count;
                });
                BOOST_TEST(3 == count);
            });
            sync.fire(0);
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
