#include "lib/framework.hpp"

JSONDB_TEST_SUITE(VariantRemove, A, B, Variant)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    run_test(
        [&](auto db)
        {
            db.transaction([&](Lock<Variant const> lock){
                lock.each([](Variant const *variant){
                    if(std::holds_alternative<Pointer<A>>(variant->object))
                    {
                        BOOST_TEST(std::get<Pointer<A>>(variant->object) == nullptr);
                    }
                    else if(std::holds_alternative<Pointer<B>>(variant->object))
                    {
                        BOOST_TEST(std::get<Pointer<B>>(variant->object)->value == 42);
                    }
                    else
                    {
                        BOOST_TEST(false);
                    }

                    if(std::holds_alternative<int>(variant->primitive))
                    {
                        BOOST_TEST(std::get<int>(variant->primitive) == 1);
                    }
                    else if(std::holds_alternative<std::string>(variant->primitive))
                    {
                        BOOST_TEST(std::get<std::string>(variant->primitive) == "foobar");
                    }
                    else
                    {
                        BOOST_TEST(false);
                    }
                });
            });
        },
        [&](auto db, auto &sync)
        {
            db.transaction([&](Lock<A, B, Variant> lock){
                auto a = lock.make(A{
                    .name = "a"
                });

                lock.make(Variant{
                    .object = a,
                    .primitive = 1
                });
            });

            db.transaction([&](Lock<A> lock) {
                lock.each([&](A* a){ lock.remove(a); });
            });
        },
        [&](auto db, auto &sync)
        {
            db.transaction([&](Lock<A, B, Variant> lock){
                auto b = lock.make(B{
                    .value = 42
                });

                lock.make(Variant{
                    .object = b,
                    .primitive = "foobar"
                });
            });
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
