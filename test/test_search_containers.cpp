#include "lib/framework.hpp"

JSONDB_TEST_SUITE(SearchContainers, StlContainers)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    run_test(
        [](auto db)
        {
            db.transaction([](Lock<StlContainers> lock){
                BOOST_TEST(1 == lock.count(Search<&StlContainers::a>{.contains = "a"s}));
                BOOST_TEST(1 == lock.count(Search<&StlContainers::a>{.contains = "b"s}));
                BOOST_TEST(0 == lock.count(Search<&StlContainers::a>{.contains = "x"s}));
                BOOST_TEST(1 == lock.count(Search<&StlContainers::a>{.contains = "y"s}));
                BOOST_TEST(1 == lock.count(Search<&StlContainers::a>{.contains = "z"s}));

                BOOST_TEST(1 == lock.count(Search<&StlContainers::b>{.contains = "a"s}));
                BOOST_TEST(2 == lock.count(Search<&StlContainers::b>{.contains = "b"s}));
                BOOST_TEST(0 == lock.count(Search<&StlContainers::b>{.contains = "c"s}));
                BOOST_TEST(1 == lock.count(Search<&StlContainers::b>{.contains = "d"s}));
            });
        },
        [](auto db, auto &sync)
        {
            db.transaction([](Lock<StlContainers> lock){
                auto o1 = lock.make(StlContainers{
                    .a = {
                        {"a"s, 1}, {"b"s, 2}
                    },
                    .b = {
                        {"a"s, "b"s}
                    }
                });
                auto o2 = lock.make(StlContainers{
                    .a = {
                        {"a"s, 1}
                    },
                    .b = {
                        {"b"s}
                    }
                });
                auto o3 = lock.make(StlContainers{
                    .a = {
                        {"x"s, 3}, {"y"s, 4}
                    },
                    .b = {
                        {"b"s, "c"s}
                    }
                });
                auto o4 = lock.make(StlContainers{
                    .a = {
                        {"y"s, 5}, {"z"s, 1}
                    },
                    .b = {
                        {"b"s, "d"s}
                    }
                });

                BOOST_TEST(2 == lock.count(Search<&StlContainers::a>{.contains = "a"s}));
                BOOST_TEST(1 == lock.count(Search<&StlContainers::a>{.contains = "b"s}));
                BOOST_TEST(1 == lock.count(Search<&StlContainers::a>{.contains = "x"s}));
                BOOST_TEST(2 == lock.count(Search<&StlContainers::a>{.contains = "y"s}));
                BOOST_TEST(1 == lock.count(Search<&StlContainers::a>{.contains = "z"s}));

                BOOST_TEST(1 == lock.count(Search<&StlContainers::b>{.contains = "a"s}));
                BOOST_TEST(4 == lock.count(Search<&StlContainers::b>{.contains = "b"s}));
                BOOST_TEST(1 == lock.count(Search<&StlContainers::b>{.contains = "c"s}));
                BOOST_TEST(1 == lock.count(Search<&StlContainers::b>{.contains = "d"s}));

                lock.remove(o2);

                BOOST_TEST(1 == lock.count(Search<&StlContainers::a>{.contains = "a"s}));
                BOOST_TEST(1 == lock.count(Search<&StlContainers::a>{.contains = "b"s}));
                BOOST_TEST(1 == lock.count(Search<&StlContainers::a>{.contains = "x"s}));
                BOOST_TEST(2 == lock.count(Search<&StlContainers::a>{.contains = "y"s}));
                BOOST_TEST(1 == lock.count(Search<&StlContainers::a>{.contains = "z"s}));

                BOOST_TEST(1 == lock.count(Search<&StlContainers::b>{.contains = "a"s}));
                BOOST_TEST(3 == lock.count(Search<&StlContainers::b>{.contains = "b"s}));
                BOOST_TEST(1 == lock.count(Search<&StlContainers::b>{.contains = "c"s}));
                BOOST_TEST(1 == lock.count(Search<&StlContainers::b>{.contains = "d"s}));

                lock.remove(o3);
            });
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
