#include "lib/framework.hpp"

JSONDB_TEST_SUITE(UniqueSimple, Config)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;
    run_test(

        [](auto db)
        {
            db.transaction([&](Lock<Config const> lock) {
                std::size_t count = 0;
                lock.each([&count](Config const *config) {
                    ++count;
                    if(config->name == "foo")
                    {
                        BOOST_TEST(config->value["a"].get<int>() == 3);
                    }
                    else if(config->name == "bar")
                    {
                        BOOST_TEST(config->value["b"].get<int>() == 2);
                    }
                    else
                    {
                        BOOST_TEST(false);
                    }
                });
                BOOST_TEST(count == 2);
            });
        },
        [](auto db, auto &sync)
        {
            db.transaction([&](Lock<Config> lock) {
                auto o1 = lock.make(Config{
                    .name = "foo",
                    .value = "{\"a\": 1}"_json
                });
                BOOST_TEST(1 == Config::counter());

                auto u1 = lock.uuidof(lock(o1));
                BOOST_TEST(!!u1);

                auto o2 = lock.make(Config{
                    .name = "foo",
                    .value = "{\"a\": 2}"_json
                });

                auto u2 = lock.uuidof(lock(o2));
                BOOST_TEST(!!u2);

                BOOST_TEST(u1 == u2);
                BOOST_TEST(1 == Config::counter());

                lock.make(Config{
                    .name = "foo",
                    .value = "{\"a\": 3}"_json
                }, *u2);

                BOOST_TEST(u1 == u2);
                BOOST_TEST(1 == Config::counter());

                boost::uuids::uuid u3;
                lock.make(Config{
                    .name = "bar",
                    .value = "{\"b\": 1}"_json
                }, u3);

                BOOST_TEST(u1 != u3);
                BOOST_TEST(2 == Config::counter());

                boost::uuids::uuid u4;
                lock.make(Config{
                    .name = "bar",
                    .value = "{\"b\": 2}"_json
                }, u4);

                BOOST_TEST(u4 == u3);
                BOOST_TEST(2 == Config::counter());
            });
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
