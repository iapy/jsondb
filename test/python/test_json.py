from . import jsondb
import json

class Json(jsondb.TestCase):
    def test_json(self):
        self.db['Json'].create(name="foo", value={
            'a': 'test',
            'b': [1,2,3,4],
            'c': {
                'x': 1, 'y': 'bar'
             }
        })

        for j in self.db['Json']:
            self.assertEqual(j.name, "foo")
            self.assertEqual(j.value, {
                'a': 'test',
                'b': [1,2,3,4],
                'c': {
                    'x': 1, 'y': 'bar'
                 }
            })

        for i in range(10, 20):
            self.db['Json'].create(name=f'int:{i}', value=i)
            self.db['Json'].create(name=f'int:{i}', value=i)

        self.db['Json'].create(name='true:1', value=True)
        self.db['Json'].create(name='true:2', value=True)
        self.db['Json'].create(name='true:3', value=True)
        self.db['Json'].create(name='false:1', value=False)
        self.db['Json'].create(name='false:2', value=False)

        self.db['Json'].create(name='name:true:1', value={'v': True})
        self.db['Json'].create(name='name:true:2', value={'v': True})
        self.db['Json'].create(name='name:false:1', value={'v': False})
        self.db['Json'].create(name='name:int:1', value={'v': 1})
        self.db['Json'].create(name='name:int:2', value={'v': 2})
        self.db['Json'].create(name='name:int:3', value={'v': 3})
        self.db['Json'].create(name='name:int:4', value={'v': 4})
        self.db['Json'].create(name='name:int:5', value={'v': 5})
        self.db['Json'].create(name='name:str:1', value={'v': 'foo'})
        self.db['Json'].create(name='name:str:2', value={'v': 'bar'})
        self.db['Json'].create(name='name:obj:1', value={'v': {'k': 1}})
        self.db['Json'].create(name='name:obj:2', value={'v': {'k': 'a'}})
        self.db['Json'].create(name='name:obj:3', value={'v': {'k': True}})

        self.assertEqual(len(list(self.db['Json']['value', dict(query=15)])), 2)
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'.lt': 13}))])), 6)
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'.gt': 10, '.lt': 13}))])), 4)
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'.gt': 18}))])), 2)
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'a': 'test'}))])), 1)
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'c': {'x': 1}}))])), 1)
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'c': {'y': 'bar'}}))])), 1)

        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps(True))])), 3)
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps(False))])), 2)

        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'.lt': True}))])), 2)
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'.gt': False}))])), 3)

        BOOLEAN = {'.ge': False}
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'.le': True}))])), 5)
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps(BOOLEAN))])), 5)

        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'v': True}))])), 2)
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'v': False}))])), 1)
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'v': BOOLEAN}))])), 3)

        INTEGER = {'.le': 9223372036854775807}
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'v': INTEGER}))])), 5)
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'v': 3}))])), 1)
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'v': {'.ge': 3}}))])), 3)
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'v': {'.gt': 1, '.le': 3}}))])), 2)
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'v': {'.gt': 1, '.lt': 3}}))])), 1)
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'v': {'.ge': 1, '.lt': 3}}))])), 2)

        STRING = {'.ge': ''}
        self.assertEqual(len(list(self.db['Json']['value', dict(query=json.dumps({'v': STRING}))])), 2)
        self.assertEqual(list(self.db['Json']['value', dict(query=json.dumps({'v': {'k': INTEGER}}))])[0].name, "name:obj:1")
        self.assertEqual(list(self.db['Json']['value', dict(query=json.dumps({'v': {'k': 1}}))])[0].name, "name:obj:1")
        self.assertEqual(list(self.db['Json']['value', dict(query=json.dumps({'v': {'k': STRING}}))])[0].name, "name:obj:2")
        self.assertEqual(list(self.db['Json']['value', dict(query=json.dumps({'v': {'k': 'a'}}))])[0].name, "name:obj:2")
        self.assertEqual(list(self.db['Json']['value', dict(query=json.dumps({'v': {'k': True}}))])[0].name, "name:obj:3")

if __name__ == '__main__':
    unittest.main()
