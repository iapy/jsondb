from . import jsondb
from datetime import datetime, timedelta

class Datetime(jsondb.TestCase):
    def test_create_search(self):
        n = datetime.now()
        for i in range(0, 10):
            self.db['Times'].create(time = n - timedelta(hours=i))

        c = len(list(self.db['Times']['time', dict(lt=n)]))
        self.assertEqual(c, 9)

        c = len(list(self.db['Times']['time', dict(lt=(n - timedelta(hours=1)))]))
        self.assertEqual(c, 8)

        c = len(list(self.db['Times']['time', dict(lt=(n - timedelta(hours=2)))]))
        self.assertEqual(c, 7)

        c = len(list(self.db['Times']['time', dict(gt=(n - timedelta(hours=9)))]))
        self.assertEqual(c, 9)

        c = len(list(self.db['Times']['time', dict(gt=(n - timedelta(hours=8)))]))
        self.assertEqual(c, 8)

        c = len(list(self.db['Times']['time', dict(gt=(n - timedelta(hours=7)))]))
        self.assertEqual(c, 7)

        c = len(list(self.db['Times']['time', dict(gt=(n - timedelta(hours=7)), lt=(n - timedelta(hours=1)))]))
        self.assertEqual(c, 5)

if __name__ == '__main__':
    unittest.main()
