from . import jsondb

class Variant(jsondb.TestCase):
    def test_variant_create(self):
        a = self.db['A'].create(name='test')
        b = self.db['B'].create(value=42)

        v1 = self.db['Variant'].create(object={'objects::A': a}, primitive={'int': 24})
        v2 = self.db['Variant'].create(object={'objects::B': b}, primitive={'string': 'foobar'})

        for v in self.db['Variant']:
            if 'objects::A' in v.object:
                self.assertEqual(v.object['objects::A'], a._id)
            elif 'objects::B' in v.object:
                self.assertEqual(v.object['objects::B'], b._id)
            else:
                self.assertTrue(False)

            if 'int' in v.primitive:
                self.assertEqual(v.primitive['int'], 24)
            elif 'string' in v.primitive:
                self.assertEqual(v.primitive['string'], 'foobar')
            else:
                self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
