from . import jsondb

class Unique(jsondb.TestCase):
    def test_unique_create(self):
        c1 = self.db['Config'].create(name='foo', value=[1,2,3])
        c2 = self.db['Config'].create(name='foo', value=[4,5,6])
        self.assertEqual(c1.id, c2.id)
        self.assertEqual(c2.value, [4,5,6])

        c3 = self.db['Config'].create(name='bar', value=[1,2,3])
        c4 = self.db['Config'].create(name='bar', value=[])
        self.assertEqual(c3.id, c4.id)
        self.assertEqual(c4.value, [])

        self.assertNotEqual(c1.id, c3.id)

    def test_unique_search(self):
        c1 = self.db['Config'].create(name='x')
        c2 = self.db['Config'].create(name='y')

        m = [o.name for o in self.db['Config']['name', dict(eq='x')]]
        self.assertEqual(m, ['x'])
        
        m = [o.name for o in self.db['Config']['name', dict(eq='y')]]
        self.assertEqual(m, ['y'])


if __name__ == '__main__':
    unittest.main()
