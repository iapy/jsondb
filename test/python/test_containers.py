from . import jsondb

class Calculated(jsondb.TestCase):
    def test_create_modify(self):
        c = self.db['StlContainers'].create(
            a = dict(a=1, b=2),
            b = {"x", "y", "z"},
            c = dict(
                a=[1,2,3],
                b=["foo"]
            )
        )

        self.assertTrue(isinstance(c.a, dict))
        self.assertTrue(isinstance(c.b, set))
        self.assertTrue(isinstance(c.c, dict))

        self.assertEqual(c.a, dict(a=1, b=2))
        self.assertEqual(c.b, {"x", "y", "z"})
        self.assertEqual(c.c, dict(a=[1,2,3], b=["foo"]))

        c.b.add("f")
        del c.a["a"]
        del c.c["a"]

        c = self.db["StlContainers"].update(c)

        self.assertTrue(isinstance(c.a, dict))
        self.assertTrue(isinstance(c.b, set))
        self.assertTrue(isinstance(c.c, dict))

        self.assertEqual(c.a, dict(b=2))
        self.assertEqual(c.b, {"f", "x", "y", "z"})
        self.assertEqual(c.c, dict(b=["foo"]))

    def test_search(self):
        self.db["StlContainers"].create(a=dict(foo=1, bar=2), b={"foo", "bar"})
        self.db["StlContainers"].create(a=dict(foo=1, baz=2), b={"a", "b"})
        self.db["StlContainers"].create(b={"b", "c", "d"})
        self.db["StlContainers"].create(a=dict(baz=2),b={"bar"})

        self.assertEqual(len(list(self.db["StlContainers"]["b", dict(contains="a")])), 1)
        self.assertEqual(len(list(self.db["StlContainers"]["b", dict(contains="b")])), 2)
        self.assertEqual(len(list(self.db["StlContainers"]["b", dict(contains="c")])), 1)
        self.assertEqual(len(list(self.db["StlContainers"]["b", dict(contains="d")])), 1)
        self.assertEqual(len(list(self.db["StlContainers"]["b", dict(contains="foo")])), 1)
        self.assertEqual(len(list(self.db["StlContainers"]["b", dict(contains="bar")])), 2)

        self.assertEqual(len(list(self.db["StlContainers"]["a", dict(contains="foo")])), 2)
        self.assertEqual(len(list(self.db["StlContainers"]["a", dict(contains="bar")])), 1)
        self.assertEqual(len(list(self.db["StlContainers"]["a", dict(contains="baz")])), 2)

if __name__ == '__main__':
    unittest.main()
