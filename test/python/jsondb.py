import os
import time
import json
import uuid
import types
import shutil
import urllib
import unittest
import requests
import subprocess
import urllib.request
from datetime import datetime

class TestCase(unittest.TestCase):
    DB_URL = f'http://127.0.0.1:59613/db'

    def setUpClass():
        binary = os.path.join(os.getcwd(), 'tests', 'webserver')
        storage = os.path.join(os.getcwd(), 'tests', 'data')
        if os.path.exists(storage):
            shutil.rmtree(storage)
        TestCase._p = subprocess.Popen([binary], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        while True:
            try:
                exec(urllib.request.urlopen(f'{TestCase.DB_URL}/api.py').read().decode('utf-8')) in locals()
                break
            except:
                pass
        TestCase.Database = locals()['Database']

    def setUp(self):
        self.db = TestCase.Database(self.DB_URL, 'objects')

    def tearDownClass():
        TestCase._p.kill()
        TestCase._p.wait()
