from . import jsondb

class Calculated(jsondb.TestCase):
    def test_create_modify(self):
        c = self.db['Calculated'].create(a=1, b=2)
        self.assertEqual(c.c, 3)

        c.a = 2
        c.b = 7

        c = self.db['Calculated'].update(c)
        self.assertEqual(c.c, 9)


if __name__ == '__main__':
    unittest.main()
