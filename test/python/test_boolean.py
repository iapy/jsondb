from . import jsondb

class Boolean(jsondb.TestCase):
    def test_create_search(self):
        o1 = self.db['Flags'].create(a=True,  b=True,  c=True,  name="abc")
        o2 = self.db['Flags'].create(a=True,  b=True,  c=False, name="ab")
        o3 = self.db['Flags'].create(a=True,  b=False, c=False, name="a")
        o4 = self.db['Flags'].create(a=False, b=True,  c=False, name="b")
        o5 = self.db['Flags'].create(a=False, b=False, c=False, name="")

        s = set(a.name for a in self.db['Flags']["a", dict(eq=True)])
        self.assertEqual(s, {"abc", "ab", "a"})

        s = set(a.name for a in self.db['Flags']["b", dict(eq=True)])
        self.assertEqual(s, {"abc", "ab", "b"})

        s = set(a.name for a in self.db['Flags']["c", dict(eq=True)])
        self.assertEqual(s, {"abc"})

        s = set(a.name for a in self.db['Flags']["a", dict(eq=False)])
        self.assertEqual(s, {"b", ""})

        s = set(a.name for a in self.db['Flags']["b", dict(eq=False)])
        self.assertEqual(s, {"a", ""})

        s = set(a.name for a in self.db['Flags']["c", dict(eq=False)])
        self.assertEqual(s, {"ab", "a", "b", ""})

        self.db['Flags'].remove(o5)

        s = set(a.name for a in self.db['Flags']["a", dict(eq=True)])
        self.assertEqual(s, {"abc", "ab", "a"})

        s = set(a.name for a in self.db['Flags']["b", dict(eq=True)])
        self.assertEqual(s, {"abc", "ab", "b"})

        s = set(a.name for a in self.db['Flags']["c", dict(eq=True)])
        self.assertEqual(s, {"abc"})

        s = set(a.name for a in self.db['Flags']["a", dict(eq=False)])
        self.assertEqual(s, {"b"})

        s = set(a.name for a in self.db['Flags']["b", dict(eq=False)])
        self.assertEqual(s, {"a"})

        s = set(a.name for a in self.db['Flags']["c", dict(eq=False)])
        self.assertEqual(s, {"ab", "a", "b"})

        self.db['Flags'].remove(o4)

        s = set(a.name for a in self.db['Flags']["a", dict(eq=True)])
        self.assertEqual(s, {"abc", "ab", "a"})

        s = set(a.name for a in self.db['Flags']["b", dict(eq=True)])
        self.assertEqual(s, {"abc", "ab"})

        s = set(a.name for a in self.db['Flags']["c", dict(eq=True)])
        self.assertEqual(s, {"abc"})

        s = set(a.name for a in self.db['Flags']["a", dict(eq=False)])
        self.assertEqual(s, set())

        s = set(a.name for a in self.db['Flags']["b", dict(eq=False)])
        self.assertEqual(s, {"a"})

        s = set(a.name for a in self.db['Flags']["c", dict(eq=False)])
        self.assertEqual(s, {"ab", "a"})

        self.db['Flags'].remove(o3)

        s = set(a.name for a in self.db['Flags']["a", dict(eq=True)])
        self.assertEqual(s, {"abc", "ab"})

        s = set(a.name for a in self.db['Flags']["b", dict(eq=True)])
        self.assertEqual(s, {"abc", "ab"})

        s = set(a.name for a in self.db['Flags']["c", dict(eq=True)])
        self.assertEqual(s, {"abc"})

        s = set(a.name for a in self.db['Flags']["a", dict(eq=False)])
        self.assertEqual(s, set())

        s = set(a.name for a in self.db['Flags']["b", dict(eq=False)])
        self.assertEqual(s, set())

        s = set(a.name for a in self.db['Flags']["c", dict(eq=False)])
        self.assertEqual(s, {"ab"})

        self.db['Flags'].remove(o2)

        s = set(a.name for a in self.db['Flags']["a", dict(eq=True)])
        self.assertEqual(s, {"abc"})

        s = set(a.name for a in self.db['Flags']["b", dict(eq=True)])
        self.assertEqual(s, {"abc"})

        s = set(a.name for a in self.db['Flags']["c", dict(eq=True)])
        self.assertEqual(s, {"abc"})

        s = set(a.name for a in self.db['Flags']["a", dict(eq=False)])
        self.assertEqual(s, set())

        s = set(a.name for a in self.db['Flags']["b", dict(eq=False)])
        self.assertEqual(s, set())

        s = set(a.name for a in self.db['Flags']["c", dict(eq=False)])
        self.assertEqual(s, set())

        self.db['Flags'].remove(o1)

        s = set(a.name for a in self.db['Flags']["a", dict(eq=True)])
        self.assertEqual(s, set())

        s = set(a.name for a in self.db['Flags']["b", dict(eq=True)])
        self.assertEqual(s, set())

        s = set(a.name for a in self.db['Flags']["c", dict(eq=True)])
        self.assertEqual(s, set())

        s = set(a.name for a in self.db['Flags']["a", dict(eq=False)])
        self.assertEqual(s, set())

        s = set(a.name for a in self.db['Flags']["b", dict(eq=False)])
        self.assertEqual(s, set())

        s = set(a.name for a in self.db['Flags']["c", dict(eq=False)])
        self.assertEqual(s, set())

if __name__ == '__main__':
    unittest.main()
