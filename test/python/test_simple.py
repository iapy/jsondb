from . import jsondb

class Simple(jsondb.TestCase):
    def test_create_modify(self):
        c = self.db['Country'].create(name='UK')
        self.assertEqual(c.name, 'UK')
        self.assertEqual(c['name'], 'UK')

        c.name = 'United Kindgdom'
        d = self.db['Country'].update(c)

        self.assertEqual(c.name, d.name)
        self.assertEqual(c['name'], d.name)
        self.assertEqual(c['name'], d['name'])
        self.assertEqual(c.id, d.id)

    def test_create_reference(self):
        c = self.db['Country'].create(name='Germany')

        a1 = self.db['Author'].create(name='Hermann Hesse', country=c)
        self.assertEqual(a1.country, c.id)
        
        a2 = self.db['Author'].create(name='Karl Marx', country=c.id)
        self.assertEqual(a2.country, c.id)

        names = set(a.name for a in self.db['Author'])
        self.assertEqual(names, {'Karl Marx', 'Hermann Hesse'})

        names = set()
        for a in self.db['Author']['country', dict(eq=c)]:
            names.add(a.name)
            if len(names) == 1:
                self.db['Author'].remove(a)
            else:
                self.db['Author'].remove(a.id)

        self.assertEqual(names, {'Karl Marx', 'Hermann Hesse'})
        self.assertEqual(list(self.db['Author']), list())


if __name__ == '__main__':
    unittest.main()
