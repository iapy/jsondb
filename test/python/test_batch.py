from . import jsondb

class Batch(jsondb.TestCase):
    def test_create_modify(self):
        c = self.db['A'].create(name='foo')
        self.assertEqual(c.name, 'foo')
        self.assertEqual(c['name'], 'foo')

        cs = list(self.db['A'].create([
            dict(name='bar'),
            dict(name='baz')
        ]))
        self.assertEqual(len(cs), 2)
        self.assertEqual(cs[0].name, 'bar')
        self.assertEqual(cs[1].name, 'baz')

        cs.insert(0, c)
        for o in cs:
            o.name += '_'

        cs = list(self.db['A'].update(cs))
        self.assertEqual(len(cs), 3)
        self.assertEqual(cs[0].name, 'foo_')
        self.assertEqual(cs[1].name, 'bar_')
        self.assertEqual(cs[2].name, 'baz_')
        self.assertEqual(cs[0]._id, c._id)

        cs = list(self.db['A'].create(
            dict(name=str(i)) for i in range(5)
        ))
        self.assertEqual(len(cs), 5)
        for i in range(5):
            self.assertEqual(cs[i].name, str(i))

        self.db['A'].remove(cs)
        self.assertEqual(len(list(self.db['A'].all())), 3)
        self.db['A'].remove(self.db['A'].all())
        self.assertEqual(len(list(self.db['A'].all())), 0)

if __name__ == '__main__':
    unittest.main()
