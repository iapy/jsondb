from . import jsondb

class Lists(jsondb.TestCase):
    def test_create_search(self):
        self.db['Lists'].create(ints=[1,2], strings=["foo", "bar"])
        self.db['Lists'].create(ints=[2,3], strings=["bar", "baz"])
        self.db['Lists'].create(ints=[3,4], strings=["baz", "xxx"])

        o = [o.ints for o in self.db['Lists']['ints', dict(contains=1)]]
        self.assertEqual(o, [[1,2]])

        o = sorted([o.ints for o in self.db['Lists']['ints', dict(contains=2)]], key=lambda l: l[0])
        self.assertEqual(o, [[1,2], [2,3]])

        o = sorted([o.ints for o in self.db['Lists']['ints', dict(contains=3)]], key=lambda l: l[0])
        self.assertEqual(o, [[2,3], [3,4]])

        o = [o.ints for o in self.db['Lists']['ints', dict(contains=4)]]
        self.assertEqual(o, [[3,4]])

        o = [o.strings for o in self.db['Lists']['strings', dict(contains='foo')]]
        self.assertEqual(o, [['foo', 'bar']])

        o = sorted([o.strings for o in self.db['Lists']['strings', dict(contains='bar')]], key=lambda l: l[0])
        self.assertEqual(o, [['bar', 'baz'], ['foo', 'bar']])

        o = sorted([o.strings for o in self.db['Lists']['strings', dict(contains='baz')]], key=lambda l: l[0])
        self.assertEqual(o, [['bar', 'baz'], ['baz', 'xxx']])

        o = [o.strings for o in self.db['Lists']['strings', dict(contains='xxx')]]
        self.assertEqual(o, [['baz', 'xxx']])



if __name__ == '__main__':
    unittest.main()
