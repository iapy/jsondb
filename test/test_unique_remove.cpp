#include "lib/framework.hpp"

JSONDB_TEST_SUITE(UniqueRemove, Config)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;
    run_test(
        [](auto db)
        {
            db.transaction([&](Lock<Config const> lock) {
                std::size_t count = 0;
                lock.each([&count](Config const *) {
                    ++count;
                });
                BOOST_TEST(count == 0);
            });
        },
        [](auto db, auto &sync)
        {
            db.transaction([&](Lock<Config> lock){
                auto o1 = lock.make(Config{
                    .name = "foo",
                    .value = "{\"a\": 1}"
                });
                BOOST_TEST(1 == Config::counter());

                auto u1 = lock.uuidof(lock(o1));
                BOOST_TEST(!!u1);

                lock.unique("foo", [&](Config *config){
                    BOOST_TEST(config->name == "foo");
                    lock.remove(config);
                });

                std::size_t count = 0;
                lock.each([&count](Config *) {
                    ++count;
                });
                BOOST_TEST(count == 0);
            });

            db.wait();

            BOOST_TEST(0 == Config::counter());
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
