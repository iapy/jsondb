#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(ClientSimple, jsondb::tester::JsondbClient)

BOOST_AUTO_TEST_CASE(Test)
{
    run_test([](auto sv, auto db){
        boost::uuids::uuid c;
        boost::uuids::uuid a;

        db.transaction(sv, [&](Session<Author> session) {
            auto *country = session.make(Country{
                .name = "UK"
            });
            BOOST_TEST(country != nullptr);
            auto *author = session.make(Author{
                .name = "Geoffrey Chaucer",
                .country = country
            });
            BOOST_TEST(author != nullptr);

            auto cu = session.uuidof(country);
            BOOST_TEST(!!cu);

            c = *cu;

            auto au = session.uuidof(author);
            BOOST_TEST(!!au);

            a = *au;
        });

        db.transaction(sv, [&](Session<Author> session) {
            int called{0};
            BOOST_TEST(session.get(a, [&](Author const *author) {
                BOOST_TEST(author);
                BOOST_TEST(a == session.uuidof(author));
                BOOST_TEST(c == session.uuidof(author->country));
                ++called;

                BOOST_TEST(session.remove(author));
            }));

            BOOST_TEST(session.get(c, [&](Country const *country) {
                BOOST_TEST(country);
                BOOST_TEST(c == session.uuidof(country));
                ++called;

                BOOST_TEST(session.remove(country));
            }));
            BOOST_TEST(2 == called);
        });
    });
}

BOOST_AUTO_TEST_SUITE_END()
