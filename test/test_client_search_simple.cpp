#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(ClientSearchSimple, jsondb::tester::JsondbClient)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;
    run_test([](auto sv, auto db){
        db.transaction(sv, [&](Session<Book> session) {
            auto Kafka = session.make(Author{
                    .name = "Franz Kafka"s
            });
            session.make(Book{
                .name = "metamorphosis"s,
                .author = Kafka
            });
            session.make(Book{
                .name = "The Trial"s,
                .author = Kafka 
            });
        });

        db.transaction(sv, [&](Session<Book> session) {
            session.make(Book{
                .name = "metamorphosis"s,
                .author = session.make(Author{
                    .name = "William Jackson"s
                })
            });
        });

        db.transaction(sv, [](Session<Book> session){
            std::set<std::string> authors;
            session.each(Search<&Book::name>{.eq = "metamorphosis"s}, [&](Book *book) {
                authors.insert(book->author->name);
                if(book->author->name == "William Jackson"s)
                    session.remove(book);
                else
                    book->name = "Metamorphosis"s;
            });
            BOOST_TEST((authors == std::set<std::string>{"William Jackson"s, "Franz Kafka"s}));
        });

        db.transaction(sv, [](Session<Book> session){
            std::set<std::string> strings;
            session.each(Search<&Book::name>{.eq = "Metamorphosis"s}, [&](Book const *book) {
                strings.insert(book->author->name);
            });
            BOOST_TEST(std::set<std::string>{"Franz Kafka"s} == strings);
            
            strings.clear();
            BOOST_TEST(strings.empty());

            session.each(Search<&Book::name>{.eq = "The Trial"s}, [&](auto *book) {
                strings.insert(book->author->name);
            });
            BOOST_TEST(std::set<std::string>{"Franz Kafka"s} == strings);
            strings.clear();

            session.each(Search<&Author::name>{.eq = "Franz Kafka"s}, [&](auto *a) {
                strings.insert(a->name);
                session.each(Search<&Book::author>{.eq = a}, [&](Book const *b) {
                    strings.insert(b->name);
                });
            });
            BOOST_TEST((strings == std::set<std::string>{"Franz Kafka"s, "Metamorphosis"s, "The Trial"s}));
        });
    });
}

BOOST_AUTO_TEST_SUITE_END()
