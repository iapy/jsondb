#include "lib/framework.hpp"

JSONDB_TEST_SUITE(SearchDatetime, Times)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    jsondb::DateTime dt;
    jsonio::from("\"2022-01-01T00:00:00\""_json, dt);

    boost::uuids::uuid dt_1h;
    boost::uuids::uuid dt_2h;
    boost::uuids::uuid dt_3h;

    run_test(
        [&](auto db)
        {
            db.transaction([&](Lock<Times const> lock){
                lock.each(Search<&Times::time>{.eq = (dt - std::chrono::hours(1))}, [&](auto *time) {
                    BOOST_TEST(lock.uuidof(time) == dt_1h);
                });
            });
            db.transaction([&](Lock<Times const> lock){
                lock.each(Search<&Times::time>{.eq = (dt - std::chrono::hours(2))}, [&](auto *time) {
                    BOOST_TEST(lock.uuidof(time) == dt_2h);
                });
            });
            db.transaction([&](Lock<Times const> lock){
                lock.each(Search<&Times::time>{.eq = (dt - std::chrono::hours(3))}, [&](auto *time) {
                    BOOST_TEST(lock.uuidof(time) == dt_3h);
                });
            });
            db.transaction([&](Lock<Times const> lock){
                std::size_t count = 0;
                lock.each(Search<&Times::time>{.gt = (dt - std::chrono::hours(3))}, [&](auto *time) {
                    BOOST_TEST((lock.uuidof(time) == dt_1h || lock.uuidof(time) == dt_2h));
                    ++count;
                });
                BOOST_TEST(2 == count);
            });
            db.transaction([&](Lock<Times const> lock){
                std::size_t count = 0;
                lock.each(Search<&Times::time>{.lt = (dt - std::chrono::hours(1))}, [&](auto *time) {
                    BOOST_TEST((lock.uuidof(time) == dt_2h || lock.uuidof(time) == dt_3h));
                    ++count;
                });
                BOOST_TEST(2 == count);
            });
            db.transaction([&](Lock<Times const> lock){
                std::size_t count = 0;
                lock.each(Search<&Times::time>{.gt = (dt - std::chrono::hours(3)), .lt = (dt - std::chrono::hours(1))}, [&](auto *time) {
                    BOOST_TEST(lock.uuidof(time) == dt_2h);
                    ++count;
                });
                BOOST_TEST(1 == count);
            });
            db.transaction([&](Lock<Times const> lock){
                std::size_t count = 0;
                lock.each(Search<&Times::times>{.contains = (dt - std::chrono::hours(1))}, [&](auto *time) {
                    ++count;
                });
                BOOST_TEST(0 == count);
            });
            db.transaction([&](Lock<Times const> lock){
                std::size_t count = 0;
                lock.each(Search<&Times::times>{.contains = (dt - std::chrono::hours(2))}, [&](auto *time) {
                    BOOST_TEST(lock.uuidof(time) == dt_1h);
                    ++count;
                });
                BOOST_TEST(1 == count);
            });
            db.transaction([&](Lock<Times const> lock){
                std::size_t count = 0;
                lock.each(Search<&Times::times>{.contains = (dt - std::chrono::hours(3))}, [&](auto *time) {
                    BOOST_TEST((lock.uuidof(time) == dt_1h || lock.uuidof(time) == dt_2h));
                    ++count;
                });
                BOOST_TEST(2 == count);
            });
        },
        [&](auto db, auto &sync)
        {
            db.transaction([&](Lock<Times> lock) {
                lock.make(Times{
                    .time = dt - std::chrono::hours(1),
                    .times = {
                        dt - std::chrono::hours(2),
                        dt - std::chrono::hours(3)
                    }
                }, dt_1h);
                lock.make(Times{
                    .time = dt - std::chrono::hours(2),
                    .times = {
                        dt - std::chrono::hours(3)
                    }
                }, dt_2h);
                lock.make(Times{
                    .time = dt - std::chrono::hours(3)
                }, dt_3h);
            });
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
