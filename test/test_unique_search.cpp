#include "lib/framework.hpp"

JSONDB_TEST_SUITE(UniqueSearch, Config)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;
    run_test(
        [](auto db)
        {
            BOOST_TEST(1 == Config::counter());
        },
        [](auto db, auto &sync)
        {
            db.transaction([&](Lock<Config> lock) {
                lock.unique("foo", [&](Config *config) {
                    config->value["a"] = 1;
                    BOOST_TEST(config->name == "foo");
                });
            });
            BOOST_TEST(1 == Config::counter());
            sync.fire(0);
            return 0;
        },
        [](auto db, auto &sync)
        {
            sync.wait(0);
            db.transaction([&](Lock<Config> lock) {
                lock.unique("foo", [&](Config *config) {
                    config->value["a"] = 2;
                    BOOST_TEST(config->name == "foo");
                });
            });
            BOOST_TEST(1 == Config::counter());
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
