#include "lib/framework.hpp"

JSONDB_TEST_SUITE(EditReference, Author, Book, Country)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    run_test(
        [](auto db)
        {
            bool found = false;
            db.transaction([&](Lock<Book const> lock){
                lock.each(Search<&Book::name>{.eq = "Metamorphosis"}, [&](auto *book){
                    BOOST_TEST(book->author->name == "Franz Kafka");
                    found = true;
                });
            });
            BOOST_TEST(found);
        },
        [](auto db, auto &sync)
        {
            db.transaction([](Lock<Book, Author> lock){
                lock.make(Book{
                    .name = "Metamorphosis",
                    .author = lock.make(Author{
                        .name = "Kafka"
                    })
                });
            });

            db.transaction([](Lock<Book const, Author> lock){
                lock.each(Search<&Book::name>{.eq = "Metamorphosis"}, [&](auto *book){
                    lock(book->author, [](Author *author){
                        author->name = "Franz Kafka";
                    });
                });
            });

            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
