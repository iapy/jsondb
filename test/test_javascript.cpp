#include <jsondb/server.hpp>
#include <msgbus/msgbus.hpp>
#include <msgbus/service.hpp>

#include <weblib/server.hpp>
#include <tester/tester.hpp>

#include <script/browser/jquery.hpp>
#include <script/browser/window.hpp>
#include <script/runtime.hpp>
#include <script/testing.hpp>

#include "lib/model.hpp"

BOOST_AUTO_TEST_SUITE(Javascript)

struct T : cg::Component
{
    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                using namespace std::chrono_literals;
                std::this_thread::sleep_for(500ms);
                return !this->remote(script::tag::Runtime{}).ctx().execute(std::filesystem::path{TEST_JS});
            }
        };
    };

    struct Handler
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            void setup(crow::SimpleApp &app)
            {
                using Self = Interface<Base>;
                CROW_ROUTE(app, "/stop")(this->bind(&Self::stop));
            }

            std::string stop()
            {
                auto db = this->remote(jsondb::tag::Client{});
                db.stop();
                return "Stopped";
            }
        };
    };

    using Services = ct::tuple<Main>;
    using Ports = ct::map<ct::pair<weblib::tag::Handler, Handler>>;
};

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace objects;
    using Database = jsondb::Server<A, B, Author, Book, Country, Enum, Flags, IntegerValue, Lists, StlContainers, Times, Variant>;
    std::filesystem::remove_all(DATA_DIR);

    using G = cg::Graph<
        cg::Connect<Database, msgbus::Msgbus>,
        cg::Connect<weblib::Server, msgbus::Msgbus>,
        cg::Connect<T, Database, jsondb::tag::Client>,
        cg::Connect<T, script::Runtime>,
        weblib::Handlers<
            T,
            Database
        >,
        script::Modules<
            script::Testing,
            script::browser::JQuery,
            script::browser::Window
        >
    >;

    auto rc = msgbus::Service<G>(
        cg::Args<Database>(DATA_DIR, "db"),
        cg::Args<weblib::Server>("127.0.0.1", SERVER_PORT, 2, false),
        cg::Args<script::Runtime>(std::filesystem::path(TEST_JS).parent_path())
    );
    BOOST_TEST(0 == rc);
}

BOOST_AUTO_TEST_SUITE_END()
