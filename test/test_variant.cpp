#include "lib/framework.hpp"

JSONDB_TEST_SUITE(Variant, A, B, objects::Variant)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    run_test(
        [&](auto db)
        {
            db.transaction([&](Lock<objects::Variant const> lock){
                lock.each([](objects::Variant const *variant){
                    if(std::holds_alternative<Pointer<A>>(variant->object))
                    {
                        BOOST_TEST(std::get<Pointer<A>>(variant->object)->name == "a");
                    }
                    else if(std::holds_alternative<Pointer<B>>(variant->object))
                    {
                        BOOST_TEST(std::get<Pointer<B>>(variant->object)->value == 42);
                    }
                    else
                    {
                        BOOST_TEST(false);
                    }

                    if(std::holds_alternative<int>(variant->primitive))
                    {
                        BOOST_TEST(std::get<int>(variant->primitive) == 1);
                    }
                    else if(std::holds_alternative<std::string>(variant->primitive))
                    {
                        BOOST_TEST(std::get<std::string>(variant->primitive) == "foobar");
                    }
                    else
                    {
                        BOOST_TEST(false);
                    }
                });
            });
        },
        [&](auto db, auto &sync)
        {
            db.transaction([&](Lock<A, B, objects::Variant> lock){
                auto a = lock.make(A{
                    .name = "a"
                });

                lock.make(objects::Variant{
                    .object = a,
                    .primitive = 1
                });
            });
            return 0;
        },
        [&](auto db, auto &sync)
        {
            db.transaction([&](Lock<A, B, objects::Variant> lock){
                auto b = lock.make(B{
                    .value = 42
                });

                lock.make(objects::Variant{
                    .object = b,
                    .primitive = "foobar"
                });
            });
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
