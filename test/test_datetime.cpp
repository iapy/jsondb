#include "lib/framework.hpp"

JSONDB_TEST_SUITE(Datetime, Times)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    DateTime dt;
    jsonio::from("\"2022-01-01T00:00:00\""_json, dt);

    run_test(
        [&](auto db)
        {
            db.transaction([&](Lock<Times const> lock){
                bool found = false;
                lock.each([&](Times const *p) {
                    BOOST_TEST(p->time == dt);
                    BOOST_TEST(2 == p->times.size());
                    BOOST_TEST(p->times[0] == (dt - std::chrono::hours(1)));
                    BOOST_TEST(p->times[1] == (dt - std::chrono::hours(2)));
                    found = true;
                });
                BOOST_TEST(found);
            });
        },
        [&](auto db, auto &sync)
        {
            db.transaction([&](Lock<Times> lock){
                lock.make(Times{
                    .time = dt,
                    .times = {
                        dt - std::chrono::hours(1),
                        dt - std::chrono::hours(2)
                    }
                });
            });
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
