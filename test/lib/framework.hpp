#pragma once

#include <jsondb/lock.hpp>
#include <jsondb/server.hpp>
#include <jsondb/client.hpp>
#include <jsondb/search.hpp>

#include "model.hpp"

#include <msgbus/msgbus.hpp>
#include <msgbus/service.hpp>
#include <tester/metacg.hpp>

#include <sys/wait.h>
#include <filesystem>
#include <iostream>

namespace jsondb {
namespace tester {

class Synchronizer
{
private:
    volatile size_t running;
    std::mutex mu;
    std::condition_variable cv;
    std::bitset<64> events;

public:
    Synchronizer(std::size_t running)
    : running{running}
    {}

    void finish()
    {
        std::unique_lock<std::mutex> lock(mu);
        --running;
        lock.unlock();
        cv.notify_all();
    }

    void wait()
    {
        std::unique_lock<std::mutex> lock(mu);
        cv.wait(lock, [this] { return this->running == 0; });
    }

    void fire(int event)
    {
        std::unique_lock<std::mutex> lock(mu);
        this->events[event] = true;
        lock.unlock();
        cv.notify_all();
    }

    void wait(int event)
    {
        std::unique_lock<std::mutex> lock(mu);
        cv.wait(lock, [this, event] {
            return this->events[event];
        });
        this->events[event] = false;
    }
};

template<typename Functor>
struct Client : cg::Component
{
    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                this->state.functor(this->remote(jsondb::tag::Client{}), *this->state.sync);
                this->state.sync->finish();
                return 0;
            }
        };
    };

    template<typename>
    struct State
    {
        Functor functor;
        Synchronizer *sync;
    };

    using Services = ct::tuple<Service>;
};

template<typename Functor>
struct Master : cg::Component
{
    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                this->state.sync->wait();
                auto db = this->remote(jsondb::tag::Client{});
                db.wait();
                this->state.functor(db);
                db.stop();
                return 0;
            }
        };
    };

    template<typename>
    struct State
    {
        Functor functor;
        Synchronizer *sync;
    };

    using Services = ct::tuple<Service>;
};

template<typename Functor>
struct Remote : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        using Server = typename Resolver::template Types<jsondb::tag::Remote>::Server;
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                auto db = this->remote(jsondb::tag::Remote{});
                auto sv = typename Base::Server{.port = this->state.port, .prefix = this->state.prefix};

                int retries{5};
                while(!db.ping(sv) && 0 != --retries)
                {
                    using namespace std::chrono_literals;
                    std::this_thread::sleep_for(500ms);
                }

                this->state.functor(sv, db);
                return retries ? 0 : 1;
            }
        };
    };

    template<typename>
    struct State
    {
        Functor functor;
        std::uint16_t port;
        std::string prefix;
    };

    using Services = ct::tuple<Service>;
};

template<typename ...Model>
struct Jsondb
{
    using Database = Server<Model...>;

    Jsondb()
    {
        std::filesystem::create_directories(data);
    }

    template<typename Verify, typename ...Runner>
    void run_test(Verify &&verify, Runner&&... runner)
    {
        Synchronizer sync{sizeof ...(Runner)};
        auto rc1 = msgbus::Service<cg::Graph<
            cg::Connect<Database, msgbus::Msgbus>,
            cg::Connect<Master<Verify>, Database, jsondb::tag::Client>,
            cg::Connect<Client<Runner>, Database, jsondb::tag::Client>...
        >>(
            cg::Args<Database>(data),
            cg::Args<Master<Verify>>(verify, &sync),
            cg::Args<Client<Runner>>(runner, &sync)...
        );
        BOOST_TEST(0 == rc1);

        auto rc2 = msgbus::Service<cg::Graph<
            cg::Connect<Database, msgbus::Msgbus>,
            cg::Connect<Master<Verify>, Database, jsondb::tag::Client>
        >>(
            cg::Args<Database>(data),
            cg::Args<Master<Verify>>(verify, &sync)
        );
        BOOST_TEST(0 == rc2);
    }

    ~Jsondb()
    {
        std::filesystem::remove_all(data);
    }

private:
    std::filesystem::path data{DATA_DIR};
};

struct JsondbClient : ::tester::Metacg
{
    template<typename Runner>
    void run_test(Runner &&runner)
    {
        if(auto pid = fork(); pid < 0)
        {
            exit(1);
        }
        else if(pid == 0)
        {
            execl(SERVER_BIN, SERVER_BIN, NULL);
        }
        else
        {
            using G = cg::Graph<
                cg::Connect<Remote<Runner>, jsondb::Client>
            >;
            test_graph<G>(
                cg::Args<Remote<Runner>>(runner, SERVER_PORT, "/db")
            );
            kill(pid, SIGINT);

            int status{0};
            waitpid(pid, &status, 0);
        }
    }

    ~JsondbClient()
    {
        std::filesystem::remove_all(data);
    }

private:
    std::filesystem::path data{DATA_DIR};
};

} // namespace tester

template<typename T>
struct NakedPointer
{
    T const *pointer;
};

template<typename T>
std::ostream &operator << (std::ostream &stream, Pointer<T> const &p)
{ 
    stream << jsonio::detail::variant_key_t<Pointer<T>>::get() << "@";
    if(!p) stream << "null";

    static_assert(sizeof(Pointer<T>) == sizeof(NakedPointer<T>));
    NakedPointer<T> const *np = reinterpret_cast<NakedPointer<T> const *>(&p);
    return stream << "0x" << std::hex << reinterpret_cast<std::uint64_t>(np->pointer);
}

} // namespace jsondb

namespace std {

inline ostream &operator << (ostream &stream, optional<boost::uuids::uuid> const &uuid)
{
    if(uuid) return stream << boost::uuids::to_string(*uuid);
    return stream << "null";
}

} // namespace std

using namespace jsondb;
using namespace objects;

#define JSONDB_TEST_SUITE(name, ...) \
    using name##Fixture = jsondb::tester::Jsondb<__VA_ARGS__>; \
    BOOST_FIXTURE_TEST_SUITE(name, name##Fixture)
