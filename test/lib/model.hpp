#pragma once
#include <jsondb/index.hpp>
#include <jsondb/datetime.hpp>

namespace objects {
    struct Country {
        BOOST_HANA_DEFINE_STRUCT(Country,
            (std::string, name)
        );
    };

    struct Author {
        BOOST_HANA_DEFINE_STRUCT(Author,
            (std::string, name),
            (jsondb::Pointer<Country>, country)
        );

        using Index = jsondb::Index<&Author::name>;
    };

    struct Book {
        BOOST_HANA_DEFINE_STRUCT(Book,
            (std::string, name),
            (jsondb::Pointer<Author>, author)
        );

        using Index = jsondb::Index<&Book::name>;
    };

    struct Node {
        BOOST_HANA_DEFINE_STRUCT(Node,
            (int, value),
            (jsondb::Pointer<Node>, next)
        );

        using Index = jsondb::Index<&Node::value>;
    };

    struct Json {
        BOOST_HANA_DEFINE_STRUCT(Json,
            (std::string, name),
            (nlohmann::json, value)
        );

        using Index = jsondb::Index<&Json::name, &Json::value>;
    };

    struct Lists {
        BOOST_HANA_DEFINE_STRUCT(Lists,
            (std::vector<int>, ints),
            (std::vector<std::string>, strings)
        );

        using Index = jsondb::Index<&Lists::ints, &Lists::strings>;
    };

    struct Times {
        BOOST_HANA_DEFINE_STRUCT(Times,
            (jsondb::DateTime, time),
            (std::vector<jsondb::DateTime>, times)
        );

        using Index = jsondb::Index<&Times::time, &Times::times>;
    };

    struct Flags {
        BOOST_HANA_DEFINE_STRUCT(Flags,
            (bool, a),
            (bool, b),
            (bool, c),
            (std::string, name)
        );

        using Index = jsondb::Index<&Flags::a, &Flags::b, &Flags::c, &Flags::name>;
    };

    struct Calculated {
        BOOST_HANA_DEFINE_STRUCT(Calculated,
            (int, a),
            (int, b)
        );

        void operator () (nlohmann::json &json) const
        {
            json["c"] = (a + b);
        }
    };

    struct StlContainers {
        BOOST_HANA_DEFINE_STRUCT(StlContainers,
            (std::map<std::string, int>, a),
            (std::set<std::string>, b),
            (std::map<std::string, nlohmann::json>, c)
        );

        using Index = jsondb::Index<&StlContainers::a, &StlContainers::b, &StlContainers::c>;
    };

    namespace detail {
        template<typename T>
        struct Counting
        {
            Counting() { ++counter(); }
            Counting(Counting &&) { ++counter(); }
            Counting(Counting const &) { ++counter(); }
            ~Counting() { --counter(); }

            static std::atomic<std::size_t> &counter()
            {
                static std::atomic<std::size_t> counter_;
                return counter_;
            }
        };
    }

    struct Config : detail::Counting<Config> {
        BOOST_HANA_DEFINE_STRUCT(Config,
            (std::string, name),
            (nlohmann::json, value)
        );

        using Unique = jsondb::Index<&Config::name>;
    };

    struct WithConfig {
        BOOST_HANA_DEFINE_STRUCT(WithConfig,
            (std::string, name),
            (jsondb::Pointer<Config>, config)
        );

        using Unique = jsondb::Index<&WithConfig::name>;
    };

    struct IntegerValue : detail::Counting<IntegerValue> {
        BOOST_HANA_DEFINE_STRUCT(IntegerValue,
            (std::string, name),
            (int, whatever)
        );

        using Index = jsondb::Index<&IntegerValue::whatever>;
        using Unique = jsondb::Index<&IntegerValue::name>;
    };

    struct Enum {
        enum class Option : char {
            A = 'a',
            B = 'b'
        };

        BOOST_HANA_DEFINE_STRUCT(Enum,
            (Option, option),
            (std::string, name)
        );

        using Index = jsondb::Index<&Enum::option>;
    };

    struct A {
        BOOST_HANA_DEFINE_STRUCT(A,
            (std::string, name)
        );
    };

    struct B {
        BOOST_HANA_DEFINE_STRUCT(B,
            (int, value)
        );
    };

    struct Variant {
        using O = std::variant<jsondb::Pointer<A>, jsondb::Pointer<B>>;
        using P = std::variant<int, std::string>;

        BOOST_HANA_DEFINE_STRUCT(Variant,
            (O, object),
            (P, primitive)   
        );
    };

    struct Progress {
        BOOST_HANA_DEFINE_STRUCT(Progress,
            (std::string, name),
            (std::size_t, total)
        );

        std::size_t count;
        void operator () (nlohmann::json &json) const
        {
            json["count"] = count;
        }

        void operator () (nlohmann::json const &json)
        {
            if(json.contains("count"))
            {
                count = json["count"].get<std::size_t>();
            }
        }

        using Unique = jsondb::Index<&Progress::name>;
    };

    struct Ephemeral {
        BOOST_HANA_DEFINE_STRUCT(Ephemeral,
            (std::string, name)
        );

        using Index = jsondb::Index<&Ephemeral::name>;
        static constexpr bool Persistent = false;
    };

    struct Persistent {
        BOOST_HANA_DEFINE_STRUCT(Persistent,
            (std::string, name),
            (jsondb::Pointer<Ephemeral>, eref)
        );
    };
}

#define TEST_OBJECTS Author, Country, Book, Node, Json, Lists, Times, Flags, Config, IntegerValue, Calculated, StlContainers, Enum, A, B, Variant, Progress, Ephemeral, Persistent
