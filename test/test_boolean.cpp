#include "lib/framework.hpp"

JSONDB_TEST_SUITE(Boolean, Flags)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    run_test(
        [&](auto db)
        {
            db.transaction([&](Lock<Flags const> lock){
                std::size_t count = 0;

                count = lock.count(Search<&Flags::a>{.eq = false}); 
                BOOST_TEST(count == 1);

                count = lock.count(Search<&Flags::a>{.eq = true}); 
                BOOST_TEST(count == 2);

                count = lock.count(Search<&Flags::a>{.eq = true}, [](auto *flags){
                    return flags->name == "a=true";
                }); 
                BOOST_TEST(count == 1);

                count = lock.count(Search<&Flags::b>{.eq = false}); 
                BOOST_TEST(count == 2);

                count = lock.count(Search<&Flags::b>{.eq = true}); 
                BOOST_TEST(count == 1);

                count = lock.count(Search<&Flags::c>{.eq = false}); 
                BOOST_TEST(count == 3);

                count = lock.count(Search<&Flags::c>{.eq = true}); 
                BOOST_TEST(count == 0);
            });
        },
        [&](auto db, auto &sync)
        {
            db.transaction([&](Lock<Flags> lock){
                lock.make(Flags{
                    .a = false,
                    .b = false,
                    .c = false,
                    .name = "Falses"s
                });
                lock.make(Flags{
                    .a = true, 
                    .b = false,
                    .c = false,
                    .name = "a=true"s
                });
                lock.make(Flags{
                    .a = true, 
                    .b = false,
                    .c = false,
                    .name = "b=true"s
                });

                lock.each(Search<&Flags::name>{.eq = "b=true"}, [](Flags *flags){
                    flags->b = true;
                });
            });
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
