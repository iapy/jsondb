response = null;

DB.use('objects');
DB['Author'].create({
    name: 'Franz Kafka'
}).then(function(c){
    response = c;
});

runtime.sync();
testing.check("response.name == 'Franz Kafka'");

DB['Author'].remove(response);
runtime.sync();
