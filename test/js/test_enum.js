response = null;
DB['objects::Enum'].create([
    {
        name: 'A',
        option: 'a'
    },
    {
        name: 'B',
        option: 'b'
    }
]).then(function(c){
    response = c;
});

runtime.sync();
testing.check("response.length == 2");
testing.check("response[0].name == 'A'");
testing.check("response[0].option == 'a'");
testing.check("response[1].name == 'B'");
testing.check("response[1].option == 'b'");

response[0].option = 2;

error = null;
try {
    DB['objects::Enum'].update(response);
} catch(e) {
    error = e.errors;
}

testing.check("error != null");
testing.check("error.length == 1");
testing.check("error[0][0] == 'option'");

response[0].option = 'xx';

error = null;
try {
    DB['objects::Enum'].update(response);
} catch(e) {
    error = e.errors;
}

testing.check("error != null");
testing.check("error.length == 1");
testing.check("error[0][0] == 'option'");

DB['objects::Enum'].remove(response, function(){});
runtime.sync();
