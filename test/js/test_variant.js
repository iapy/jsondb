response = null;
DB['objects::Variant'].create({
}).then(function(c){
    response = c;
});

runtime.sync();
testing.check("!('string' in response.primitive)");
testing.check("response.primitive['int'] === 0");
testing.check("!('objects::B' in response.object)");
testing.check("response.object['objects::A'] === null");

var a = null;
DB['objects::A'].create({
    name: 'test'
}).then(function(c){
    a = c;
});
runtime.sync();

response.object = {'objects::A': a};
response.primitive = {'string': 'foo'};
DB['objects::Variant'].update(response, function(c){
    response = c;
});

runtime.sync();
testing.check("!('int' in response.primitive)");
testing.check("response.primitive['string'] === 'foo'");
testing.check("!('objects::B' in response.object)");
testing.check("response.object['objects::A'] === a['_id']");

var b = null;
DB['objects::B'].create({
    value: 24 
}).then(function(c){
    b = c;
});
runtime.sync();

response.object = {'objects::A': b};
DB['objects::Variant'].update(response, function(c){
    response = c;
});

runtime.sync();
testing.check("!('objects::B' in response.object)");
testing.check("response.object['objects::A'] === null");

response.object = {'objects::B': b};
DB['objects::Variant'].update(response, function(c){
    response = c;
});

runtime.sync();
testing.check("!('objects::A' in response.object)");
testing.check("response.object['objects::B'] === b['_id']");

DB['objects::A'].remove(a, function(){});
runtime.sync();

DB['objects::B'].remove(b, function(){});
runtime.sync();

DB['objects::Variant'].remove(response, function(){});
runtime.sync();

error = null;
try {
    DB['objects::Variant'].create({
        'primitive': {'k': 1},
        'object': {'foo': 1, 'bar': 2}
    });
} catch(e) {
    error = e.errors;
}

testing.check("error != null");
testing.check("error.length == 2");
testing.check("error[0][0] == 'object'");
testing.check("error[1][0] == 'primitive'");
