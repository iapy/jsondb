response = null;

var d = new Date();
var ds = [1, 2, 3, 4, 5].map(function(i){
    return new Date(new Date(d).setDate(d.getDate() + i));
});

DB['objects::Times'].create(ds.map(function(d){
    return {time: d, times: [d]};
})).then(function(c){
    response = c;
});
runtime.sync();

var result = null;

for(var i = 0; i < ds.length; ++i)
{
    DB['objects::Times'].search.time({lt: ds[i]}, function(c){
        result = c;
    });

    runtime.sync();
    testing.check("result.length == " + i);
}

for(var i = 0; i < ds.length; ++i)
{
    DB['objects::Times'].search.time({gt: ds[i]}, function(c){
        result = c;
    });

    runtime.sync();
    testing.check("result.length == " + (ds.length - i - 1));
}

for(var i = 0; i < 3; ++i)
{
    DB['objects::Times'].search.time({gt: ds[i], lt: ds[i + 2]}, function(c){
        result = c;
    });

    runtime.sync();
    testing.check("result.length == 1");
}

for(var i = 0; i < ds.length; ++i)
{
    DB['objects::Times'].search.times({contains: ds[i]}, function(c){
        result = c;
    });

    runtime.sync();
    testing.check("result.length == 1");
}

error = null;
try {
    DB['objects::Times'].search.time({gt: 'xxxxxx'});
} catch(e) {
    error = e.errors;
}

testing.check("error != null");
testing.check("error.length == 1");
testing.check("error[0][0] == 'gt'");

error = null;
try {
    DB['objects::Times'].search.times({contains: 'xxxxxx'});
} catch(e) {
    error = e.errors;
}

testing.check("error != null");
testing.check("error.length == 1");
testing.check("error[0][0] == 'contains'");

DB['objects::Times'].remove(response);
runtime.sync();
