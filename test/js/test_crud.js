response = null;
DB['objects::IntegerValue'].create({
    name: 'Answer',
    whatever: 40
}).then(function(c){
    response = c;
});

runtime.sync();
testing.check("response.name == 'Answer'");
testing.check("response.whatever == 40");

DB['objects::IntegerValue'].create([
    {
        name: 'Answer',
        whatever: 42
    },
    {
        name: 'Test',
        whatever: '1'
    },
    {
        name: 4321,
        whatever: 4321
    },
    {
    }
]).then(function(c){
    testing.check("c[0]._id == response._id");
    response = c;
});

runtime.sync();
testing.check('response.length == 4');
testing.check("response[0].name == 'Answer'");
testing.check("response[0].whatever == 42");
testing.check("response[1].name == 'Test'");
testing.check("response[1].whatever == 1");
testing.check("response[2].name == '4321'");
testing.check("response[2].whatever == 4321");
testing.check("response[3].name == ''");

DB['objects::IntegerValue'].all(function(c){
    response = c;
});

runtime.sync();
response.sort(function(a, b) {
    return a.name.localeCompare(b.name);
});

testing.check('response.length == 4');
testing.check("response[0].name == ''");
testing.check("response[1].name == '4321'");
testing.check("response[1].whatever == 4321");
testing.check("response[2].name == 'Answer'");
testing.check("response[2].whatever == 42");
testing.check("response[3].name == 'Test'");
testing.check("response[3].whatever == 1");

response[0].name = 'Shoud not be updated';
response[0].whatever = 24;
response[1].whatever = 25;
delete response[2].whatever;
delete response[3].name;

request = response;
response = null;
DB['objects::IntegerValue'].update(request, function(){
    response = c;
});

runtime.sync();
testing.check("response[0].name = 'Shoud not be updated'");
testing.check("response[3].name = 'Test'");

DB['objects::IntegerValue'].all(function(c){
    response = c;
});

runtime.sync();
response.sort(function(a, b) {
    return a.name.localeCompare(b.name);
});

testing.check('response.length == 4');
testing.check("response[0].name == ''");
testing.check("response[0].whatever == 24");
testing.check("response[1].name == '4321'");
testing.check("response[1].whatever == 25");
testing.check("response[2].name == 'Answer'");
testing.check("response[2].whatever == 42");
testing.check("response[3].name == 'Test'");
testing.check("response[3].whatever == 1");

var byid = null;
DB['objects::IntegerValue'].get(response[0], function(c){
    byid = c;
});
runtime.sync();
testing.check("byid['_id'] == response[0]['_id']");

DB['objects::IntegerValue'].get(response, function(c){
    byid = c;
});
runtime.sync();
testing.check("byid.length == 4");
testing.check("byid['_id'][0] == response[0]['_id']");
testing.check("byid['_id'][1] == response[1]['_id']");
testing.check("byid['_id'][2] == response[2]['_id']");
testing.check("byid['_id'][3] == response[3]['_id']");

DB['objects::IntegerValue'].remove(response[0]);
runtime.sync();

DB['objects::IntegerValue'].remove(response.slice(1));
runtime.sync();

DB['objects::IntegerValue'].all(function(c){
    response = c;
});

runtime.sync();
testing.check('response.length == 0');

error = null;
try {
    DB['objects::IntegerValue'].create({
        name: 'error',
        whatever: 'asdf'
    });
} catch(e) {
    error = e.errors;
}

testing.check("error != null");
testing.check("error.length == 1");
testing.check("error[0][0] == 'whatever'");
