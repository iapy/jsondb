response = null;
DB['objects::Flags'].create([
    {
        name: 'YES',
        a: true,
        b: 'True',
        c: 'true'
    },
    {
        name: 'NO',
        b: false,
        c: 333
    }
]).then(function(c){
    response = c;
});

runtime.sync();

function validate() {
    testing.check('response.length == 2');
    testing.check("response[0].name == 'YES'");
    testing.check("response[0].a");
    testing.check("response[0].b");
    testing.check("response[0].c");
    testing.check("response[1].name == 'NO'");
    testing.check("!response[1].a");
    testing.check("!response[1].b");
    testing.check("!response[1].c");
};
validate();

DB['objects::Flags'].all(function(c){
    response = c;
});
runtime.sync();
response.sort(function(a, b) {
    return b.name.localeCompare(a.name);
});
validate();

DB['objects::Flags'].remove(response);
runtime.sync();

