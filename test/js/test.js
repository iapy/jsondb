window.open('http://127.0.0.1:59613/');
window.load('/db/api.min.js');

var response = null;
var error = null;

$.ajax({
    method: 'GET',
    url: '/db/api.js'
}).then(function(r){
    testing.write('api.js', r);
});
runtime.sync();

$.ajax({
    method: 'GET',
    url: '/db/api.min.js'
}).then(function(r){
    testing.write('api.min.js', r);
});
runtime.sync();

runtime.load('test_array.js');
runtime.load('test_bools.js');
runtime.load('test_crud.js');
runtime.load('test_dates.js');
runtime.load('test_enum.js');
runtime.load('test_generic.js');
runtime.load('test_references.js');
runtime.load('test_search_dates.js');
runtime.load('test_search_enum.js');
runtime.load('test_search_list.js');
runtime.load('test_search_simple.js');
runtime.load('test_search_stl.js');
runtime.load('test_stl.js');
runtime.load('test_variant.js');
runtime.load('test_use.js');

$.ajax({
    method: 'GET',
    url: '/stop'
});
runtime.sync();
