response = null;
DB['objects::StlContainers'].create([
    {
        'a': {
            'a': 1
        },
        'b': ['foo', 'bar'],
        'c': {
            'x': [1, 2, 3]
        }
    },
    {
        'a': {
            'a': 1,
            'b': 2
        },
        'b': ['bar'],
        'c': {
            'x': 'foo',
            'y': 'bar'
        }
    }
]).then(function(c){
    response = c;
});
runtime.sync();

var result = null;
DB['objects::StlContainers'].search.a({contains: 'a'}, function(c){
    result = c;
});

runtime.sync();
testing.check("result.length == 2");

result = null;
DB['objects::StlContainers'].search.a({contains: 'b'}, function(c){
    result = c;
});

runtime.sync();
testing.check("result.length == 1");

result = null;
DB['objects::StlContainers'].search.b({contains: 'foo'}, function(c){
    result = c;
});

runtime.sync();
testing.check("result.length == 1");

result = null;
DB['objects::StlContainers'].search.b({contains: 'bar'}, function(c){
    result = c;
});

runtime.sync();
testing.check("result.length == 2");

result = null;
DB['objects::StlContainers'].search.c({contains: 'x'}, function(c){
    result = c;
});

runtime.sync();
testing.check("result.length == 2");

result = null;
DB['objects::StlContainers'].search.c({contains: 'y'}, function(c){
    result = c;
});

runtime.sync();
testing.check("result.length == 1");

DB['objects::StlContainers'].remove(response);
runtime.sync();
