testing.check("DB['objects::A'].fields[0][1] == DB.UUID");
testing.check("DB['objects::IntegerValue'].search.name()[2]");
testing.check("!DB['objects::IntegerValue'].search.whatever()[2]");

testing.check("DB['objects::A'].fields[0][1].name == 'UUID'");
testing.check("DB['objects::Lists'].fields[1][1].name == 'Array[Integer]'");
testing.check("DB['objects::Lists'].fields[2][1].name == 'Array[String]'");
