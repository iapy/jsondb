response = null;
DB['objects::Lists'].create({
}).then(function(c){
    response = c;
});

runtime.sync();
testing.check("response.ints.length == 0");
testing.check("response.strings.length == 0");

response.ints = [1, '2', 3];
response.strings = ['foo', 'bar', 'baz'];

DB['objects::Lists'].update(response, function(c){
    response = c;
});

runtime.sync();
testing.check("response.ints.length == 3");
testing.check("response.strings.length == 3");

error = null;
try {
    response.ints = [1, 'foo', 3];
    DB['objects::Lists'].update(response);
} catch(e) {
    error = e.errors;
}

testing.check("error != null");
testing.check("error.length == 1");
testing.check("error[0][0] == 'ints'");

DB['objects::Lists'].all(function(c){
    response = c;
});

runtime.sync();
testing.check("response.length == 1");

DB['objects::Lists'].remove(response);
runtime.sync();
