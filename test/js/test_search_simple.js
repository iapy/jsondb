testing.check("DB['objects::Author'].search.name()[0] == DB.String");
testing.check("DB['objects::Author'].search.country()[0] == DB.UUID");

response = null;
var countries = null;
DB['objects::Country'].create([
    {
        name: 'France'
    },
    {
        name: 'UK'
    }
]).then(function(c){
    countries = c;
});

runtime.sync();

DB['objects::Author'].create([
    {
        name: 'Albert Camus',
        country: countries[0]
    },
    {
        name: 'Bertrand Russel',
        country: countries[1]
    },
    {
        name: 'Carl Spitteler'
    },
    {
        name: 'Franz Kafka'
    },
    {
        name: 'George Bernard Shaw',
        country: countries[1]
    },
    {
        name: 'Halldor Laxness'
    }
]).then(function(c){
    response = c;
});

runtime.sync();

function getid(o) {
    return o['_id'];
}

var result = null;
DB['objects::Author'].search.name({gt: 'Bertrand'}, function(c){
    result = c;
});
runtime.sync();
testing.check("result.length == 5");
testing.check("result.map(getid).join(' ') == response.slice(1).map(getid).join(' ')");

result = null;
DB['objects::Author'].search.name({gt: 'Bertrand Russel'}, function(c){
    result = c;
});
runtime.sync();
testing.check("result.length == 4");
testing.check("result.map(getid).join(' ') == response.slice(2).map(getid).join(' ')");

result = null;
DB['objects::Author'].search.name({lt: 'Bertrand Russel'}, function(c){
    result = c;
});
runtime.sync();
testing.check("result.length == 1");
testing.check("result.map(getid).join(' ') == response.slice(0, 1).map(getid).join(' ')");

result = null;
DB['objects::Author'].search.name({eq: 'Bertrand Russel'}, function(c){
    result = c;
});
runtime.sync();
testing.check("result.length == 1");
testing.check("result.map(getid).join(' ') == response.slice(1, 2).map(getid).join(' ')");

result = null;
DB['objects::Author'].search.name({gt: 'Bertrand Russel', lt: 'Franz Kafka'}, function(c){
    result = c;
});
runtime.sync();
testing.check("result.length == 1");
testing.check("result.map(getid).join(' ') == response.slice(2, 3).map(getid).join(' ')");

result = null;
DB['objects::Author'].search.country({eq: countries[0]._id}, function(c){
    result = c;
});
runtime.sync();
testing.check("result.length == 1");

result = null;
DB['objects::Author'].search.country({eq: countries[1]}, function(c){
    result = c;
});
runtime.sync();
testing.check("result.length == 2");

result = null;
DB['objects::Author'].search._id({eq: countries[1]}, function(c){
    result = c;
});
runtime.sync();
testing.check("result.length == 0");

result = null;
DB['objects::Author'].search._id({eq: response[1]}, function(c){
    result = c;
});
runtime.sync();
testing.check("result.length == 1");
testing.check("result[0]._id == authors[1]._id");

error = null;
try {
    DB['objects::Author'].search.country({eq: 'xxx'});
} catch(e) {
    error = e.errors;
}

testing.check("error != null");
testing.check("error.length == 1");
testing.check("error[0][0] == 'eq'");

error = null;
try {
    DB['objects::Author'].search._id({});
} catch(e) {
    error = e.errors;
}

testing.check("error != null");
testing.check("error.length == 1");
testing.check("error[0][0] == 'eq'");

error = null;
try {
    DB['objects::Author'].search._id({ne: 'xxxx'});
} catch(e) {
    error = e.errors;
}

testing.check("error != null");
testing.check("error.length == 1");
testing.check("error[0][0] == 'eq'");

error = null;
try {
    DB['objects::Author'].search._id({eq: 'xxxx'});
} catch(e) {
    error = e.errors;
}

testing.check("error != null");
testing.check("error.length == 1");
testing.check("error[0][0] == 'eq'");

DB['objects::Author'].remove(response);
DB['objects::Country'].remove(response);
runtime.sync();
