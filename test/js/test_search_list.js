response = null;
DB['objects::Lists'].create([
    {
        ints: [1, 2, 3],
        strings: ['a', 'b', 'c']
    },
    {
        ints: [2, 3, 4],
        strings: ['c', 'd', 'e']
    },
    {
        ints: [3, 4, 5, 6],
        strings: ['d', 'e', 'f', 'g']
    },
    {
        ints: [4, 5, 6, 7],
        strings: ['e', 'f', 'g', 'h']
    }
]).then(function(c){
    response = c;
});
runtime.sync();

var result = null;
DB['objects::Lists'].search.ints({contains: 1}, function(c){
    result = c;
});

runtime.sync();
testing.check("result.length == 1");

result = null;
DB['objects::Lists'].search.ints({contains: 2}, function(c){
    result = c;
});

runtime.sync();
testing.check("result.length == 2");

result = null;
DB['objects::Lists'].search.ints({contains: 4}, function(c){
    result = c;
});

runtime.sync();
testing.check("result.length == 3");

result = null;
DB['objects::Lists'].search.strings({contains: 'a'}, function(c){
    result = c;
});

runtime.sync();
testing.check("result.length == 1");

result = null;
DB['objects::Lists'].search.strings({contains: 'c'}, function(c){
    result = c;
});

runtime.sync();
testing.check("result.length == 2");

result = null;
DB['objects::Lists'].search.strings({contains: 'e'}, function(c){
    result = c;
});

runtime.sync();
testing.check("result.length == 3");

error = null;
try {
    DB['objects::Lists'].search.ints({contains: 'xxxx'});
} catch(e) {
    error = e.errors;
}

testing.check("error != null");
testing.check("error.length == 1");
testing.check("error[0][0] == 'contains'");

DB['objects::Lists'].remove(response, function(){});
runtime.sync();
