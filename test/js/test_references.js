response = null;
DB['objects::Author'].create({
    name: 'Franz Kafka'
}).then(function(c){
    response = c;
});

runtime.sync();
testing.check("response.name == 'Franz Kafka'");
testing.check("response.country == null");

var fk = response;

DB['objects::Country'].create({
    name: 'Czech Republic'
}).then(function(c){
    response = c;
});

runtime.sync();
testing.check("response.name == 'Czech Republic'");

var cr = response;
fk.country = response;

DB['objects::Author'].update(fk, function(c){
    response = c;
});

runtime.sync();
testing.check("response.country == cr['_id']");

fk.country = null;

DB['objects::Author'].update(fk, function(c){
    response = c;
});

runtime.sync();
testing.check("response.country == null");

error = null;
fk.country = 'abracadabra';
try {
    DB['objects::Author'].update(fk);
} catch(e) {
    error = e.errors;
}

testing.check("error != null");
testing.check("error.length == 1");
testing.check("error[0][0] == 'country'");

DB['objects::Author'].all(function(a){
    DB['objects::Author'].remove(a);
});
DB['objects::Country'].all(function(a){
    DB['objects::Country'].remove(a);
});

runtime.sync();
testing.check("response.country == null");
