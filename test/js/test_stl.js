response = null;
DB['objects::StlContainers'].create({
    'a': {
        'a': 1,
        'b': '2'
    },
    'b': ['foo', 'bar', 'baz', 0],
    'c': {
        'x': [1, 2, 3],
        'y': 'string',
        'z': 24
    }
}).then(function(c){
    response = c;
});

runtime.sync();
testing.check("Object.keys(response['a']).length == 2");
testing.check("response['a']['a'] === 1");
testing.check("response['a']['b'] === 2");
testing.check("response['b'][0] === '0'");
testing.check("response['b'][1] === 'bar'");
testing.check("response['b'][2] === 'baz'");
testing.check("response['b'][3] === 'foo'");
testing.check("Object.keys(response['c']).length == 3");
testing.check("response['c']['x'][0] == 1");
testing.check("response['c']['x'][1] == 2");
testing.check("response['c']['x'][2] == 3");
testing.check("response['c']['y'] === 'string'");
testing.check("response['c']['z'] === 24");

response.c = {'a': [3, 2, 1]};

DB['objects::StlContainers'].update(response).then(function(c){
    response = c;
});

runtime.sync();
testing.check("response['c']['a'][0] === 3");
testing.check("response['c']['a'][1] === 2");
testing.check("response['c']['a'][2] === 1");

DB['objects::StlContainers'].remove(response);
runtime.sync();
