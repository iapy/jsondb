response = null;

var d = new Date();
var ds = [1, 2, 3, 4, 5].map(function(i){
    return new Date(new Date(d).setDate(d.getDate() + i));
});

DB['objects::Times'].create({
    time: d,
    times: ds
}).then(function(c){
    response = c;
});

runtime.sync();
testing.check("response.time.toString() == d.toString()");
testing.check("response.times.length == 5");
for(var i = 0; i < 5; ++i)
{
    testing.check("(response.times[i].toString() == ds.toString()");
}

error = null;
try {
    DB['objects::Times'].create({
        time: 'foobar'
    });
} catch(e) {
    error = e.errors;
}

testing.check("error != null");
testing.check("error.length == 1");
testing.check("error[0][0] == 'time'");

error = null;
try {
    DB['objects::Times'].create({
        times: ['foobar']
    });
} catch(e) {
    error = e.errors;
}

testing.check("error != null");
testing.check("error.length == 1");
testing.check("error[0][0] == 'times'");

DB['objects::Times'].remove(response);
runtime.sync();
