response = null;
DB['objects::Enum'].create([
    {
        name: 'A1',
        option: 'a'
    },
    {
        name: 'A2',
        option: 'a'
    },
    {
        name: 'A3',
        option: 'a'
    },
    {
        name: 'A4',
        option: 'a'
    },
    {
        name: 'B1',
        option: 'b'
    },
    {
        name: 'B2',
        option: 'b'
    }
]).then(function(c){
    response = c;
});

runtime.sync();
testing.check("response.length == 6");

var result = null;
DB['objects::Enum'].search.option({eq: 'a'}, function(c){
    result = c;
});

runtime.sync();
testing.check("result.length == 4");

result = null;
DB['objects::Enum'].search.option({eq: 'b'}, function(c){
    result = c;
});

runtime.sync();
testing.check("result.length == 2");

error = null;
try {
    DB['objects::Enum'].search._id({eq: 'xxxx'});
} catch(e) {
    error = e.errors;
}

testing.check("error != null");
testing.check("error.length == 1");
testing.check("error[0][0] == 'eq'");

DB['objects::Enum'].remove(response, function(){});
runtime.sync();
