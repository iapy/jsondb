#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(ClientSearchComplex, jsondb::tester::JsondbClient)

BOOST_AUTO_TEST_CASE(Test)
{
    run_test([](auto sv, auto db){
        db.transaction(sv, [&](Session<Node> session) {
            Opaque<Node> tail{nullptr};
            for(int i = 50; i > 25; --i)
            {
                tail = session.make(Node{
                    .value = i,
                    .next = tail
                });
            }
        });

        db.transaction(sv, [](Session<Node> session){
            Opaque<Node> tail{nullptr};
            for(int i = 25; i > 0; --i)
            {
                tail = session.make(Node{
                    .value = i,
                    .next = tail
                });
            }
        });

        db.transaction(sv, [&](Session<Node> session){
            session.each(Search<&Node::value>{.eq = 25}, [&](Node *n25){
                session.each(Search<&Node::value>{.eq = 26}, [&](Node const *n26){
                    BOOST_TEST(n25->next == nullptr);
                    n25->next = n26;
                });
            });
        });

        db.transaction(sv, [&](Session<Node> session) {
            int sum = 0;
            std::size_t count = 0;

            session.each(Search<&Node::value>{.gt = 45, .lt = 5}, [&](Node const *node) {
                sum += node->value;
            });
            BOOST_TEST(sum == 0);

            session.each(Search<&Node::value>{.lt = 5}, [&](Node const *node) {
                sum += node->value;
            });
            BOOST_TEST(sum == 10);

            sum = 0;
            session.each(Search<&Node::value>{.eq = 10, .lt = 45}, [&](Node const *node) {
                sum += node->value;
            });
            BOOST_TEST(sum == 10);

            sum = 0;
            session.each(Search<&Node::value>{.gt = 45}, [&](Node const *node) {
                sum += node->value;
            });
            BOOST_TEST(sum == 240);

            sum = 0;
            session.each(Search<&Node::value>{.gt = 5, .lt = 45}, [&](Node const *node) {
                sum += node->value;
                BOOST_TEST(node->next);
                BOOST_TEST(node->next->value == node->value + 1);
            });
            BOOST_TEST(sum == 975);

            sum = 0;
            session.each(Search<&Node::value>{}, [&](Node const *node) {
                sum += node->value;
            });
            BOOST_TEST(sum == 1275);

            sum = 0;
            session.each(Search<&Node::next>{.eq = nullptr}, [&](Node const *node) {
                sum += node->value;
            });
            BOOST_TEST(sum == 50);
        });
    });
}

BOOST_AUTO_TEST_SUITE_END()
