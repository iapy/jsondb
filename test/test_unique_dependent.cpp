#include "lib/framework.hpp"

JSONDB_TEST_SUITE(UniqueDenendant, Config, WithConfig)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;
    run_test(
        [](auto db)
        {
            db.transaction([&](Lock<WithConfig> lock) {
                lock.unique("foo", [](WithConfig *w){
                    BOOST_TEST(w->name == "foo");
                    BOOST_TEST(w->config->name == "foo");
                    BOOST_TEST(w->config->value["a"] == 2);
                });
            });
        },
        [](auto db, auto &sync)
        {
            db.transaction([&](Lock<WithConfig, Config> lock) {
                auto c1 = lock.make(Config{
                    .name = "foo",
                    .value = "{\"a\": 1}"_json
                });
                auto w1 = lock.make(WithConfig{
                    .name = "foo",
                    .config = c1
                });
            });
            sync.fire(0);
            return 0;
        },
        [](auto db, auto &sync)
        {
            sync.wait(0);
            db.transaction([&](Lock<WithConfig, Config> lock) {
                auto c1 = lock.make(Config{
                    .name = "foo",
                    .value = "{\"a\": 2}"_json
                });
            });
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
