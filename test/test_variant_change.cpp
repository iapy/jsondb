#include "lib/framework.hpp"

JSONDB_TEST_SUITE(VariantChange, A, B, Variant)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    run_test(
        [&](auto db)
        {
            db.transaction([&](Lock<Variant const> lock){
                std::size_t count = 0;
                lock.each(Search<&Variant::object>{}, [&count](Variant const *v){
                    ++count;
                });
                BOOST_TEST(2 == count);

                count = 0;
                lock.each(Search<&Variant::object>{.eq = nullptr}, [&count](Variant const *v){
                    ++count;
                });
                BOOST_TEST(0 == count);

                count = 0;
                lock.each([&](A const *a) {
                    lock.each(Search<&Variant::object>{.eq = a}, [&](Variant const *v) {
                        ++count;
                        BOOST_TEST(std::holds_alternative<std::string>(v->primitive));
                    });
                    BOOST_TEST(1 == lock.count(Search<&Variant::object>{.eq = a}));
                });
                BOOST_TEST(1 == count);

                count = 0;
                lock.each([&](B const *b) {
                    lock.each(Search<&Variant::object>{.eq = b}, [&](Variant const *v) {
                        ++count;
                        BOOST_TEST(std::holds_alternative<int>(v->primitive));
                    });
                    BOOST_TEST(1 == lock.count(Search<&Variant::object>{.eq = b}));
                });
                BOOST_TEST(1 == count);
            });
        },
        [&](auto db, auto &sync)
        {
            db.transaction([&](Lock<A, B, Variant> lock){
                auto a = lock.make(A{
                    .name = "a"
                });

                lock.make(Variant{
                    .object = a,
                    .primitive = 42
                });
            });
            sync.fire(0);
            return 0;
        },
        [&](auto db, auto &sync)
        {
            db.transaction([&](Lock<A, B, Variant> lock){
                auto b = lock.make(B{
                    .value = 42
                });

                lock.make(Variant{
                    .object = b,
                    .primitive = "a"
                });
            });
            sync.fire(1);
            return 0;
        },
        [&](auto db, auto &sync)
        {
            sync.wait(0);
            sync.wait(1);

            db.transaction([&](Lock<Variant> lock){
                A const *aobj;
                lock.each([&](A const *a){ aobj = a; });

                B const *bobj;
                lock.each([&](B const *b){ bobj = b; });

                lock.each([&](Variant *v){
                    if(std::holds_alternative<Pointer<A>>(v->object))
                    {
                        v->object = bobj;
                    }
                    else
                    {
                        v->object = aobj;
                    }
                });
            });
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
