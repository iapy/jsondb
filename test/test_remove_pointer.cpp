#include "lib/framework.hpp"

JSONDB_TEST_SUITE(RemovePointer, Node)

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_literals;

    run_test(
        [](auto db)
        {
            int sum = 0;
            int nil = 0;
            db.transaction([&](Lock<Node const> lock)
            {
                lock.each([&](Node const *p)
                {
                    sum += p->value;
                    if(p->next == nullptr)
                    {
                        ++nil;
                    }
                    else
                    {
                        BOOST_TEST(p->value + 1 == p->next->value);
                    }
                });
            });
            BOOST_TEST(   2 == nil);
            BOOST_TEST(4900 == sum);
        },
        [](auto db, auto &sync)
        {
            auto tail = db.transaction([&sync](Lock<Node> lock)
            {
                Opaque<Node> tail{nullptr};
                for(int i = 99; i >= 0; --i)
                {
                    tail = lock.make(Node{
                        .value = i,
                        .next = tail
                    });
                    if(i == 50)
                    {
                        sync.fire(0);
                    }
                }
                return tail;
            });
            db.transaction([&tail](Lock<Node const> lock){
                BOOST_TEST(lock(tail)->value == 0);
            });
            return 0;
        },
        [](auto db, auto &sync)
        {
            sync.wait(0);
            db.transaction([](Lock<Node> lock){
                lock.each([&](Node *p){
                    if(p->value == 50)
                    {
                        lock.remove(p);
                        BOOST_TEST(nullptr == p);
                    }
                });
            });
            return 0;
        }
    );
}

BOOST_AUTO_TEST_SUITE_END()
